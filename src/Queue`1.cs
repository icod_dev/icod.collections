namespace Icod.Collections { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public class Queue<T> { 

		#region fields
		private Immutable.IQueue<T> myStore;
		#endregion fields


		#region .ctor
		public Queue() : base() { 
			myStore = Immutable.Queue<T>.GetEmpty();
		}
		#endregion .ctor


		#region properties
		public System.Int32 Count { 
			get { 
				return myStore.Count;
			}
		}
		#endregion properties


		#region methods
		public T Dequeue() { 
			T output;
			Immutable.IQueue<T> store;
			do { 
				store = myStore;
				output = store.Peek();
			} while ( !Icod.Threading.Interlocked.ExchangeCompare<Immutable.IQueue<T>>( 
				ref myStore, 
				store, 
				store.Dequeue() 
			) );
			return output;
		}
		public System.Boolean TryDequeue( out T item ) { 
			System.Exception e = null;
			return this.TryDequeue( out item, out e );
		}
		public System.Boolean TryDequeue( out T item, out System.Exception exception ) { 
			item = default( T );
			exception = null;
			System.Boolean success = false;
			try { 
				item = this.Dequeue();
				success = true;
			} catch ( System.Exception e ) { 
				exception = e;
			}
			return success;
		}

		public System.Int32 Enqueue( T item ) { 
			Immutable.IQueue<T> store;
			do { 
				store = myStore;
			} while ( !Icod.Threading.Interlocked.ExchangeCompare<Immutable.IQueue<T>>( 
				ref myStore, 
				store, 
				store.Enqueue( item ) 
			) );
			return store.Count + 1;
		}
		#endregion methods

	}

}