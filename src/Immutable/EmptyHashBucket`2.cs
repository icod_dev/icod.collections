namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[System.Diagnostics.DebuggerDisplay( "Empty" )]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class EmptyHashBucket<K, T> : IHashTable<K, T> { 

		#region fields
		private static readonly System.Int32 theHashCode;
		private static readonly System.Collections.Generic.ICollection<K> myKeys;
		private static readonly System.Collections.Generic.ICollection<T> myValues;
		private static System.Collections.Generic.IDictionary<IHashLogic<K, T>, IHashTable<K, T>> theEmpty;

		private readonly IHashLogic<K, T> myHashLogic;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static EmptyHashBucket() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked {
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
			theEmpty = new System.Collections.Generic.Dictionary<IHashLogic<K, T>, IHashTable<K, T>>();
			myKeys = new System.Collections.Generic.List<K>( 1 ).AsReadOnly();
			myValues = new System.Collections.Generic.List<T>( 1 ).AsReadOnly();
		}

		private EmptyHashBucket() : base() { 
			myHashCode = theHashCode;
		}
		private EmptyHashBucket( IHashLogic<K, T> hashLogic ) : this() { 
			if ( null == hashLogic ) { 
				throw new System.ArgumentNullException( "hashLogic" );
			}

			myHashLogic = hashLogic;
			unchecked {
				myHashCode += myHashLogic.GetHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public IHashLogic<K, T> HashLogic {
			get {
				return myHashLogic;
			}
		}
		public IHashTable<K, T> Empty {
			get {
				return this;
			}
		}
		public System.Int32 Count {
			get {
				return 0;
			}
		}
		public System.Int32 Length {
			get {
				return 0;
			}
		}
		public System.Int32 Occupancy {
			get {
				return 0;
			}
		}
		public System.Double LoadFactor {
			get {
				return 0;
			}
		}
		public System.Boolean IsEmpty {
			get {
				return true;
			}
		}
		T System.Collections.Generic.IDictionary<K, T>.this[ K key ] {
			get {
				return this[ key ];
			}
			set {
				throw new System.NotSupportedException();
			}
		}
		public T this[ K key ] {
			get {
				throw new System.InvalidOperationException();
			}
		}
		public System.Collections.Generic.ICollection<K> Keys {
			get {
				return myKeys;
			}
		}
		public System.Collections.Generic.ICollection<T> Values {
			get {
				return myValues;
			}
		}
		public System.Boolean IsReadOnly {
			get {
				return true;
			}
		}
		#endregion properties


		#region methods
		public System.Boolean ContainsKey( K key ) {
			return false;
		}
		public System.Boolean ContainsValue( T item ) {
			return false;
		}
		public HashPath<K, T> Probe( K key ) {
			return new HashPath<K, T>( Stack<IHashTable<K, T>>.GetEmpty().Push( this ), Stack<System.Int32>.GetEmpty() );
		}
		public IHashTable<K, T> GetBucket( System.Int32 index ) {
			throw new System.InvalidOperationException();
		}
		public IHashTable<K, T> SetBucket( System.Int32 index, IHashTable<K, T> bucket ) {
			throw new System.InvalidOperationException();
		}
		public IHashTable<K, T> Add( K key, T item ) {
			System.Collections.Generic.IEqualityComparer<K> comparer = myHashLogic.KeyComparer;
			return new LeafHashBucket<K, T>( myHashLogic, HashHelper.GetHash<K>( comparer, key ), key, item );
		}
		public IHashTable<K, T> Remove( K key ) {
			throw new System.Collections.Generic.KeyNotFoundException();
		}

		void System.Collections.Generic.IDictionary<K, T>.Add( K key, T value ) {
			throw new System.NotImplementedException();
		}
		System.Boolean System.Collections.Generic.IDictionary<K, T>.Remove( K key ) {
			throw new System.NotSupportedException();
		}
		public System.Boolean TryGetValue( K key, out T value ) {
			value = default( T );
			System.Boolean success = false;
			IHashTable<K, T> probe = this.Probe( key ).Bucket.Peek();
			if ( probe.ContainsKey( key ) ) {
				value = probe[ key ];
				success = true;
			}
			return success;
		}
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Add( System.Collections.Generic.KeyValuePair<K, T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Clear() {
			throw new System.NotSupportedException();
		}
		public System.Boolean Contains( System.Collections.Generic.KeyValuePair<K, T> item ) {
			return this.Probe( item.Key ).Bucket.Peek().ContainsValue( item.Value );
		}
		public void CopyTo( System.Collections.Generic.KeyValuePair<K, T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( System.Collections.Generic.KeyValuePair<K, T> kvp in this ) {
				array[ arrayIndex++ ] = kvp;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Remove( System.Collections.Generic.KeyValuePair<K, T> item ) {
			throw new System.NotSupportedException();
		}

		public sealed override System.Int32 GetHashCode() {
			return myHashCode;
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
		public System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<K, T>> GetEnumerator() {
			yield break;
		}

		public sealed override System.Boolean Equals( System.Object obj ) {
			IHashTable<K, T> other = ( obj as IHashTable<K, T> );
			if ( null == other ) {
				return false;
			} else {
				return this.Equals( other );
			}
		}
		public System.Boolean Equals( IHashTable<K, T> other ) {
			return HashHelper.Equals<K, T>( this, other );
		}
		#endregion methods


		#region static methods
		public static IHashTable<K, T> Get() {
			return Get( HashLogic<K, T>.Get() );
		}
		public static IHashTable<K, T> Get( System.Collections.Generic.IEqualityComparer<K> keyComparer ) {
			if ( null == keyComparer ) {
				throw new System.ArgumentNullException( "keyComparer" );
			}

			return Get( HashLogic<K, T>.Get( keyComparer, Comparer<T>.Default ) );
		}
		public static IHashTable<K, T> Get( System.Collections.Generic.IEqualityComparer<K> keyComparer, System.Collections.Generic.IEqualityComparer<T> itemComparer ) {
			if ( null == itemComparer ) {
				throw new System.ArgumentNullException( "itemComparer" );
			} else if ( null == keyComparer ) {
				throw new System.ArgumentNullException( "keyComparer" );
			}

			return Get( HashLogic<K, T>.Get( keyComparer, itemComparer ) );
		}
		public static IHashTable<K, T> Get( IHashLogic<K, T> hashLogic ) {
			if ( null == hashLogic ) {
				throw new System.ArgumentNullException( "hashLogic" );
			}

			lock ( theEmpty ) {
				IHashTable<K, T> empty;
				if ( theEmpty.ContainsKey( hashLogic ) ) {
					empty = theEmpty[ hashLogic ];
				} else {
					empty = new EmptyHashBucket<K, T>( hashLogic );
					theEmpty.Add( hashLogic, empty );
				}
				return empty;
			}
		}

		public static System.Boolean operator ==( EmptyHashBucket<K, T> a, EmptyHashBucket<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator ==( EmptyHashBucket<K, T> a, IHashTable<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator ==( IHashTable<K, T> a, EmptyHashBucket<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator !=( EmptyHashBucket<K, T> a, EmptyHashBucket<K, T> b ) {
			return !( a == b );
		}
		public static System.Boolean operator !=( EmptyHashBucket<K, T> a, IHashTable<K, T> b ) {
			return !( a == b );
		}
		public static System.Boolean operator !=( IHashTable<K, T> a, EmptyHashBucket<K, T> b ) {
			return !( a == b );
		}
		#endregion static methods

	}

}