namespace Icod.Collections.Immutable {

	[System.Serializable]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class ComparableHashBucket<K, T> : LeafHashBucketBase<K, T> { 

		#region fields
		private static readonly System.Int32 theHashCode;
		private readonly System.Int32 myHashCode;
		private readonly IBinarySearchTree<K, T> myStore;
		#endregion fields


		#region .ctor
		static ComparableHashBucket() {
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked {
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
		}
		private ComparableHashBucket( IHashLogic<K, T> hashLogic, System.Int32 keyHash ) : base( hashLogic, keyHash ) {
			if ( null == hashLogic ) {
				throw new System.ArgumentNullException( "hashLogic" );
			}
			System.Collections.Generic.IComparer<K> comparer = ( hashLogic.KeyComparer as System.Collections.Generic.IComparer<K> );
			if ( null == comparer ) {
				throw new System.ArgumentException( null, "hashLogic" );
			}

			myStore = AvlTree<K, T>.GetEmpty( KeyLogic<K, T>.Get( comparer ) );
			myHashCode = theHashCode;
			unchecked {
				myHashCode += keyHash;
			}
		}
		internal ComparableHashBucket( IHashLogic<K, T> hashLogic, System.Int32 keyHash, System.Collections.Generic.IEnumerable<Pair<K, T>> collection ) : this( hashLogic, keyHash ) {
			if ( null == collection ) {
				throw new System.ArgumentNullException( "collection" );
			} else if ( null == hashLogic ) {
				throw new System.ArgumentNullException( "hashLogic" );
			}

			foreach ( Pair<K, T> pair in collection ) {
				myStore = myStore.Add( pair.First, pair.Second );
			}
			if ( 0 == myStore.Count ) {
				throw new System.ArgumentNullException( "collection" );
			}

			unchecked {
				myHashCode += myStore.GetHashCode();
			}
		}
		internal ComparableHashBucket( IHashLogic<K, T> hashLogic, System.Int32 keyHash, IBinarySearchTree<K, T> store ) : this( hashLogic, keyHash ) {
			if ( ( null == store ) || store.IsEmpty ) {
				throw new System.ArgumentNullException( "store" );
			} else if ( null == hashLogic ) {
				throw new System.ArgumentNullException( "hashLogic" );
			}

			myStore = store;
			unchecked {
				myHashCode += myStore.GetHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public sealed override System.Int32 Count {
			get {
				return myStore.Count;
			}
		}
		public sealed override T this[ K key ] {
			get {
				return myStore[ key ].Value;
			}
		}
		public sealed override System.Collections.Generic.ICollection<K> Keys {
			get {
				return myStore.Keys;
			}
		}
		public sealed override System.Collections.Generic.ICollection<T> Values {
			get {
				return myStore.Values;
			}
		}
		#endregion properties


		#region methods
		public sealed override System.Boolean ContainsKey( K key ) {
			IBinarySearchTree<K, T> find = myStore.Find( key );
			return this.HashLogic.KeyComparer.Equals( find.Key, key );
		}
		public sealed override IHashTable<K, T> Add( K key, T item ) {
			IHashLogic<K, T> logic = this.HashLogic;
			System.Collections.Generic.IEqualityComparer<K> comparer = logic.KeyComparer;
			System.Int32 tCode = this.KeyHash;
			System.Int32 oCode = HashHelper.GetHash<K>( comparer, key );

			if ( tCode != oCode ) {
				return HashHelper.HashUp<K, T>( logic, PrimeHelper.MinPrime, this, new LeafHashBucket<K, T>( logic, oCode, key, item ) );
			} else if ( this.ContainsKey( key ) ) {
				throw new DuplicateKeyException( "key" );
			} else {
				ComparableHashBucket<K, T> output = null;
				try {
					output = new ComparableHashBucket<K, T>( logic, tCode, myStore.Add( key, item ) );
				} catch ( DuplicateKeyException ) {
					throw new DuplicateKeyException( "key" );
				}
				return output;
			}
		}
		public sealed override IHashTable<K, T> Remove( K key ) {
			System.Int32 code = HashHelper.GetHash<K>( this.HashLogic.KeyComparer, key );
			if ( code != this.KeyHash ) {
				throw new System.Collections.Generic.KeyNotFoundException();
			}
			IBinarySearchTree<K, T> store = myStore.Remove( key );
			IHashTable<K, T> output;
			if ( 0 == store.Count ) {
				output = this.Empty;
			} else {
				output = new ComparableHashBucket<K, T>( this.HashLogic, this.KeyHash, store );
			}
			return output;
		}

		public sealed override System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<K, T>> GetEnumerator() {
			foreach ( IBinarySearchTree<K, T> tree in BinarySearchTreeBase<K, T>.InOrder( myStore ) ) {
				yield return new System.Collections.Generic.KeyValuePair<K, T>( tree.Key, tree.Value );
			}
		}

		public sealed override System.Int32 GetHashCode() {
			return myHashCode;
		}
		#endregion methods

	}

}