namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public abstract class AvlTreeBase<K, V> : BinarySearchTreeBase<K, V> { 

		#region internal classes
		new protected abstract class EmptyTree : BinarySearchTreeBase<K, V>.EmptyTree { 
			protected EmptyTree( IKeyLogic<K, V> keyLogic ) : base( keyLogic ) { 
			}

			public sealed override System.Boolean IsInBalance( IBinarySearchTree<K, V> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				System.Int32 f = other.BalanceFactor;
				return ( ( -1 <= f ) && ( f <= 1 ) );
			}
			public override IBinarySearchTree<K, V> BalanceTree( IBinarySearchTree<K, V> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( 
					other.IsEmpty 
					|| other.IsLeaf 
					|| other.IsPerfect 
				) { 
					return other;
				}

				IBinarySearchTree<K, V> probe;
				do { 
					while ( !this.IsInBalance( other ) ) { 
						other = this.BalanceVertex( other );
					}
					probe = other.Left;
					if ( !this.IsInBalance( probe ) ) { 
						probe = this.BalanceTree( probe );
						other = other.SetLeft( probe );
					}
					probe = other.Right;
					if ( !this.IsInBalance( probe ) ) { 
						probe = this.BalanceTree( probe );
						other = other.SetRight( probe );
					}
				} while ( !this.IsInBalance( other ) );

				return other;
			}
			protected virtual IBinarySearchTree<K, V> BalanceVertex( IBinarySearchTree<K, V> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( 
					other.IsEmpty 
					|| other.IsLeaf 
					|| other.IsPerfect 
					|| ( other.Count <= 2 ) 
				) { 
					return other;
				}

				IBinarySearchTree<K, V> probe;
				if ( other.IsRightHeavy ) { 
					probe = other.Right;
					if ( probe.IsLeftHeavy ) { 
						other = other.RotateLeftRight();
					} else { 
						other = other.RotateLeft();
					}
				} else if ( other.IsLeftHeavy ) { 
					probe = other.Left;
					if ( probe.IsRightHeavy ) { 
						other = other.RotateRightLeft();
					} else { 
						other = other.RotateRight();
					}
				}

				return other;
			}
		}
		#endregion internal classes


		#region fields
		private static System.Int32 theToggle;
		#endregion fields


		#region .ctor
		static AvlTreeBase() { 
			theToggle = new System.Random().Next();
		}

		protected AvlTreeBase( IBinarySearchTree<K, V> left, IBinarySearchTree<K, V> root, IBinarySearchTree<K, V> right ) : base( left, root, right ) { 
		}
		protected AvlTreeBase( IBinarySearchTree<K, V> left, K key, V value, IKeyLogic<K, V> keyLogic, IBinarySearchTree<K, V> right ) : base( left, key, value, keyLogic, right ) { 
		}
		#endregion .ctor


		#region methods
		public override IBinarySearchTree<K, V> Find( K key ) { 
			IBinarySearchTree<K, V> empty = this.Empty;
			IBinarySearchTree<K, V> current = base.Find( key );
			return current.SetLeft( 
				empty.BalanceTree( current.Left ) 
			).SetRight( 
				empty.BalanceTree( current.Right ) 
			);
		}

		public override System.Boolean IsInBalance( IBinarySearchTree<K, V> other ) { 
			return this.Empty.IsInBalance( other );
		}

		public sealed override IBinarySearchTree<K, V> Merge( IBinarySearchTree<K, V> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, V> empty = this.Empty;
				return base.Merge( empty.BalanceTree( other ), empty.BalanceTree, empty.BalanceTree );
			}
		}

		public override IBinarySearchTree<K, V> Remove( K key ) { 
			IStack<IBinarySearchTree<K, V>> branch = Stack<IBinarySearchTree<K, V>>.GetEmpty().Push( this );
			IStack<System.Boolean> isLeft = Stack<System.Boolean>.GetEmpty();

			System.Collections.Generic.IComparer<K> comparer;
			System.Int32 c;

			IBinarySearchTree<K, V> current = this;
			do { 
				comparer = current.KeyLogic.Comparer;
				c = comparer.Compare( current.Key, key );
				if ( c < 0 ) { 
					current = current.Right;
					isLeft = isLeft.Push( false );
				} else if ( 0 < c ) { 
					current = current.Left;
					isLeft = isLeft.Push( true );
				} else { 
					break;
				}
				branch = branch.Push( current );
			} while ( !current.IsEmpty && ( 0 != c ) );
			if ( current.IsEmpty ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			}

			return this.ReconstituteTree( 
				branch.Pop().Push( this.RemoveRoot( current ) ), 
				isLeft 
			);
		}
		protected virtual IBinarySearchTree<K, V> RemoveRoot( IBinarySearchTree<K, V> current ) { 
			if ( ( null == current ) || ( current.IsEmpty ) ) { 
				throw new System.ArgumentNullException( "current" );
			} else if ( current.IsLeaf ) { 
				return current.Empty;
			}

			IBinarySearchTree<K, V> left = current.Left;
			IBinarySearchTree<K, V> right = current.Right;
			if ( left.IsEmpty ) { 
				current = right;
			} else if ( right.IsEmpty ) { 
				current = left;
			} else { 
				if ( left.IsLeaf ) { 
					current = left.SetRight( right );
				} else if ( right.IsLeaf ) { 
					current = right.SetLeft( left );
				} else { 
					IBinarySearchTree<K, V> probe;
					if ( 
						( left.Height < right.Height ) 
						|| ( left.Count < right.Count ) 
					) { 
						probe = right.Min;
						current = probe.SetLeft( 
							left 
						).SetRight( 
							right.Remove( probe.Key )
						);
					} else if ( 
						( right.Height < left.Height ) 
						|| ( right.Count < left.Count ) 
					) { 
						probe = left.Max;
						current = probe.SetRight( 
							right 
						).SetLeft( 
							left.Remove( probe.Key ) 
						);
					} else { 
						if ( 0 == ( 1 & this.IncToggle() ) ) { 
							probe = right.Min;
							current = probe.SetLeft( 
								left 
							).SetRight( 
								right.Remove( probe.Key ) 
							);
						} else { 
							probe = left.Max;
							current = probe.SetRight( 
								right 
							).SetLeft( 
								left.Remove( probe.Key ) 
							);
						}
					}
				}
			}

			return this.BalanceTree( current );
		}
		protected System.Int32 IncToggle() { 
			return System.Threading.Interlocked.Increment( ref theToggle );
		}
		#endregion methods


		#region static methods
		#endregion static methods

	}

}