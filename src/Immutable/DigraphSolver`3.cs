namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public abstract class DigraphSolver<V, W, R> : GraphSolverBase<IDigraph<V, W>, R> { 

		private readonly IDigraph<V, W> myGraph;

		protected DigraphSolver( IDigraph<V, W> graph ) : base() { 
			if ( null == graph ) { 
				throw new System.ArgumentNullException( "graph" );
			}
			myGraph = graph;
		}

		public IDigraph<V, W> Graph { 
			get { 
				return myGraph;
			}
		}

	}

}