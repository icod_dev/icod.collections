namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class HashPath<K, T> : Pair<IStack<IHashTable<K, T>>, IStack<System.Int32>> { 

		internal HashPath( IStack<IHashTable<K, T>> bucket, IStack<System.Int32> index ) : base( bucket, index ) { 
			if ( null == index ) { 
				throw new System.ArgumentNullException( "index" );
			} else if ( ( null == bucket ) || ( bucket.IsEmpty ) ) { 
				throw new System.ArgumentNullException( "bucket" );
			} else if ( bucket.Count != ( index.Count + 1 ) ) { 
				throw new System.ArgumentException( null, "index" );
			}
		}

		public IStack<IHashTable<K, T>> Bucket { 
			get { 
				return this.First;
			}
		}
		public IStack<System.Int32> Index { 
			get { 
				return this.Second;
			}
		}

	}

}