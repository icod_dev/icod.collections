namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class Edge<V, W> : IEdge<V, W> { 

		#region fields
		private static readonly System.Int32 theHashCode;

		private readonly V myA;
		private readonly V myB;
		private readonly W myWeight;
		private readonly System.Collections.Generic.IEqualityComparer<V> myComparer;
		private ISet<V> myVertexes;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static Edge() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( V ).AssemblyQualifiedName.GetHashCode();
			}
		}

		private Edge() : base() { 
			myHashCode = theHashCode;
			myVertexes = null;
		}
		private Edge( V a, V b, W weight ) : this( a, b, weight, Comparer<V>.Default ) { 
		}
		internal Edge( V a, V b, W weight, System.Collections.Generic.IEqualityComparer<V> comparer ) : this() { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			myComparer = comparer;
			myWeight = weight;
			myB = b;
			myA = a;
			unchecked { 
				myHashCode += myComparer.GetHashCode( myA );
				myHashCode += myComparer.GetHashCode( myB );
				if ( null != (System.Object)myWeight ) { 
					myHashCode += myWeight.GetHashCode();
				}
			}
		}
		#endregion .ctor


		#region properties
		public V A { 
			get { 
				return myA;
			}
		}
		public V B { 
			get { 
				return myB;
			}
		}
		public W Weight { 
			get { 
				return myWeight;
			}
		}
		public V this[ V vertex ] { 
			get { 
				if ( myComparer.Equals( myA, vertex ) ) { 
					return myB;
				} else if ( myComparer.Equals( myB, vertex ) ) { 
					return myA;
				} else { 
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}
		}
		public System.Collections.Generic.IEqualityComparer<V> Comparer { 
			get { 
				return myComparer;
			}
		}
		public System.Boolean IsReflexive { 
			get { 
				return myComparer.Equals( myA, myB );
			}
		}
		public ISet<V> Vertexes { 
			get { 
				if ( null == myVertexes ) { 
					ISet<V> verts = Set<V>.GetEmpty( myComparer ).Add( myA );
					if ( !verts.Contains( myB ) ) { 
						verts = verts.Add( myB );
					}
					System.Threading.Interlocked.CompareExchange<ISet<V>>( 
						ref myVertexes, 
						verts, 
						null 
					);
				}
				return myVertexes;
			}
		}
		public System.Int32 Count { 
			get { 
				return this.Vertexes.Count;
			}
		}
		public System.Boolean IsReadOnly { 
			get { 
				return true;
			}
		}
		#endregion properties


		#region methods
		public System.Boolean Contains( V item ) { 
			return this.Vertexes.Contains( item );
		}

		public void CopyTo( V[] array, System.Int32 arrayIndex ) { 
			if ( arrayIndex < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) { 
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) { 
				throw new System.ArgumentException( null, "array" );
			}
			this.Vertexes.CopyTo( array, arrayIndex );
		}
		public System.Boolean Equals( V x, V y ) { 
			return myComparer.Equals( x, y );
		}
		public System.Int32 GetHashCode( V obj ) { 
			return myComparer.GetHashCode( obj );
		}
		void System.Collections.Generic.ICollection<V>.Add( V item ) { 
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<V>.Clear() { 
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<V>.Remove( V item ) { 
			throw new System.NotSupportedException();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { 
			return this.GetEnumerator();
		}
		public System.Collections.Generic.IEnumerator<V> GetEnumerator() { 
			return this.Vertexes.GetEnumerator();
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		#endregion static methods

	}

}