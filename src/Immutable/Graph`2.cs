namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class Graph<V, W> : GraphBase<V, W>, IGraph<V, W> { 

		#region internal classes
		[System.Serializable]
		new private sealed class EmptyGraph : GraphBase<V, W>.EmptyGraph, IGraph<V, W> { 
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary< 
				IComparer<W>, 
				System.Collections.Generic.IDictionary< 
					System.Collections.Generic.IEqualityComparer<V>, 
					IGraph<V, W> 
				> 
			> theEmpty;
			private static readonly ISet<IGraph<V, W>> theForest;
			private readonly ISet<IEdge<V, W>> myEdges;
			private readonly IHashTable<V, ISet<IEdge<V, W>>> myOutBound;
			private readonly System.Int32 myHashCode;

			static EmptyGraph() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( V ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( W ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary< 
					IComparer<W>, 
					System.Collections.Generic.IDictionary< 
						System.Collections.Generic.IEqualityComparer<V>, 
						IGraph<V, W> 
					> 
				>();
				theForest = Set<IGraph<V, W>>.GetEmpty();
			}
			private EmptyGraph( IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) : base( weightComparer, vertexComparer ) { 
				myHashCode = theHashCode;
				unchecked { 
					myHashCode += weightComparer.GetHashCode();
					myHashCode += vertexComparer.GetHashCode();
				}
				myOutBound = HashTable<V, ISet<IEdge<V, W>>>.GetEmpty( this.VertexComparer );
				myEdges = Set<IEdge<V, W>>.GetEmpty( this );
			}

			public IGraph<V, W> Empty { 
				get { 
					return this;
				}
			}
			public sealed override ISet<IEdge<V, W>> Edges { 
				get { 
					return myEdges;
				}
			}
			public IGraph<V, W> Add( V vertex ) { 
				return new Graph<V, W>( 
					this.Vertexes.Add( vertex ), 
					this.Edges, 
					myOutBound, 
					this.WeightComparer, 
					this.VertexComparer, 
					GraphConnectivity.Not 
				);
			}
			public IGraph<V, W> Add( IEdge<V, W> edge ) { 
				if ( null == edge ) { 
					throw new System.ArgumentNullException( "edge" );
				}
				V a = edge.A;
				V b = edge.B;
				System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
				if ( comparer.Equals( a, b ) ) { 
					throw new System.NotSupportedException();
				}
				ISet<IEdge<V, W>> edges = this.Edges.Add( edge );
				return new Graph<V, W>( 
					this.Vertexes.Add( a ).Add( b ), 
					edges, 
					myOutBound.Add( a, edges ).Add( b, edges ), 
					this.WeightComparer, 
					comparer, 
					GraphConnectivity.Weak 
				);
			}
			public IGraph<V, W> Connect( V a, V b, W weight ) { 
				System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
				if ( comparer.Equals( a, b ) ) { 
					throw new System.NotSupportedException();
				}
				IEdge<V, W> e = new Edge<V, W>( a, b, weight, comparer );
				IHashTable<V, ISet<IEdge<V, W>>> outBound = myOutBound.Add( 
					a, 
					Set<IEdge<V, W>>.GetEmpty( this ).Add( e ) 
				).Add( 
					b, 
					Set<IEdge<V, W>>.GetEmpty( this ).Add( e ) 
				);
				return new Graph<V, W>( 
					this.Vertexes.Add( a ).Add( b ), 
					this.Edges.Add( e ), 
					outBound, 
					this.WeightComparer, 
					comparer, 
					GraphConnectivity.Weak 
				);
			}
			public IGraph<V, W> Remove( V vertex ) { 
				return this;
			}
			public IGraph<V, W> Remove( IEdge<V, W> edge ) { 
				if ( null == edge ) { 
					throw new System.ArgumentNullException( "edge" );
				}
				return this;
			}
			public IGraph<V, W> Update( IEdge<V, W> x, IEdge<V, W> y ) { 
				if ( null == y ) { 
					throw new System.ArgumentNullException( "y" );
				} else if ( null == x ) { 
					throw new System.ArgumentNullException( "x" );
				} else if ( x.Vertexes != y.Vertexes ) { 
					throw new System.ArgumentOutOfRangeException( "y" );
				}
				throw new System.Collections.Generic.KeyNotFoundException();
			}
			public IGraph<V, W> Update( IEdge<V, W> edge, W weight ) { 
				if ( null == edge ) { 
					throw new System.ArgumentNullException( "edge" );
				}
				throw new System.Collections.Generic.KeyNotFoundException();
			}
			public ISet<IGraph<V, W>> AsForest() { 
				return theForest;
			}

			public static IGraph<V, W> Get() { 
				return Get( Comparer<W>.Default, Comparer<V>.Default );
			}
			public static IGraph<V, W> Get( IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) { 
				if ( null == vertexComparer ) { 
					throw new System.ArgumentNullException( "vertexComparer" );
				} else if ( null == weightComparer ) { 
					throw new System.ArgumentNullException( "weightComparer" );
				}
				IGraph<V, W> output = null;
				System.Collections.Generic.IDictionary<System.Collections.Generic.IEqualityComparer<V>, IGraph<V, W>> branch = null;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( weightComparer ) ) { 
						branch = theEmpty[ weightComparer ];
					} else { 
						branch = new System.Collections.Generic.Dictionary<System.Collections.Generic.IEqualityComparer<V>, IGraph<V, W>>();
						theEmpty.Add( weightComparer, branch );
					}
				}
				lock ( branch ) { 
					if ( branch.ContainsKey( vertexComparer ) ) { 
						output = branch[ vertexComparer ];
					} else { 
						output = new EmptyGraph( weightComparer, vertexComparer );
						branch.Add( vertexComparer, output );
					}
				}
				return output;
			}

			public sealed override System.Int32 GetHashCode() { 
				return myHashCode;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;

		private IGraph<V, W> myEmpty;
		private System.Int32 myConnectivity;
		private ISet<IGraph<V, W>> myForest;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static Graph() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( V ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( W ).AssemblyQualifiedName.GetHashCode();
			}
		}

		private Graph( ISet<V> vertexes, ISet<IEdge<V, W>> edges, IHashTable<V, ISet<IEdge<V, W>>> outBound, IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) : this( vertexes, edges, outBound, weightComparer, vertexComparer, GraphConnectivity.Untested ) { 
		}
		private Graph( ISet<V> vertexes, ISet<IEdge<V, W>> edges, IHashTable<V, ISet<IEdge<V, W>>> outBound, IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer, GraphConnectivity connectivity ) : base( vertexes, edges, outBound, weightComparer, vertexComparer ) { 
			if ( !System.Enum.IsDefined( typeof( GraphConnectivity ), connectivity ) ) { 
				throw new System.ArgumentOutOfRangeException( "connectivity" );
			}
			myHashCode = theHashCode;
			unchecked { 
				myHashCode += vertexes.GetHashCode();
				myHashCode += vertexes.Count;
				myHashCode += edges.GetHashCode();
				myHashCode += edges.Count;
			}
			myEmpty = null;
			myConnectivity = (System.Int32)connectivity;
		}
		#endregion .ctor


		#region properties
		public IGraph<V, W> Empty { 
			get { 
				if ( null == myEmpty ) { 
					System.Threading.Interlocked.CompareExchange<IGraph<V, W>>( 
						ref myEmpty, 
						GetEmpty( this.WeightComparer, this.VertexComparer ), 
						null 
					);
				}
				return myEmpty;
			}
		}
		public IEdge<V, W> this[ V a, V b ] { 
			get { 
				IEdge<V, W> output;
				if ( this.TryGetEdge( a, b, out output ) ) { 
					return output;
				} else { 
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}
		}
		public sealed override System.Boolean IsConnected { 
			get { 
				if ( (System.Int32)GraphConnectivity.Untested == myConnectivity ) { 
					System.Threading.Interlocked.CompareExchange( 
						ref myConnectivity, 
						(System.Int32)( ( 1 == this.AsForest().Count ) ? GraphConnectivity.Weak : GraphConnectivity.Not ), 
						(System.Int32)GraphConnectivity.Untested 
					);
				}
				return ( (System.Int32)GraphConnectivity.Weak <= myConnectivity );
			}
		}
		#endregion properties


		#region methods
		public sealed override System.Boolean ContainsEdge( V a, V b ) { 
			if ( this.VertexComparer.Equals( a, b ) ) { 
				return false;
			}
			return base.ContainsEdge( a, b );
		}
		public sealed override System.Boolean TryGetEdge( V a, V b, out IEdge<V, W> edge ) { 
			edge = null;
			System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
			if ( comparer.Equals( a, b ) ) { 
				return false;
			}
			foreach ( IEdge<V, W> e in this.GetAdjacentTo( a ).Intersection( this.GetAdjacentTo( b ) ) ) { 
				if ( 
					( comparer.Equals( e.A, a ) && comparer.Equals( e.B, b ) ) 
					|| ( comparer.Equals( e.A, b ) && comparer.Equals( e.B, a ) ) 
				) { 
					edge = e;
					return true;
				}
			}
			return false;
		}
		public IGraph<V, W> Add( V vertex ) { 
			if ( this.Vertexes.Contains( vertex ) ) { 
				return this;
			}
			return new Graph<V, W>( this.Vertexes.Add( vertex ), this.Edges, this.OutBound, this.WeightComparer, this.VertexComparer, GraphConnectivity.Not );
		}
		public IGraph<V, W> Add( IEdge<V, W> edge ) { 
			if ( null == edge ) { 
				throw new System.ArgumentNullException( "edge" );
			}
			ISet<IEdge<V, W>> edges = this.Edges;
			if ( edges.Contains( edge ) ) { 
				return this;
			}
			V a = edge.A;
			V b = edge.B;
			System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
			if ( comparer.Equals( a, b ) ) { 
				throw new System.NotSupportedException();
			} else if ( this.ContainsEdge( a, b ) ) { 
				throw new DuplicateKeyException();
			}
			edges = edges.Add( edge );
			IHashTable<V, ISet<IEdge<V, W>>> outBound = this.OutBound;
			ISet<IEdge<V, W>> probe;
			if ( outBound.TryGetValue( a, out probe ) ) { 
				outBound = outBound.Remove( a );
			} else { 
				probe = Set<IEdge<V, W>>.GetEmpty( this.Empty );
			}
			outBound = outBound.Add( a, probe.Add( edge ) );
			if ( outBound.TryGetValue( b, out probe ) ) { 
				outBound = outBound.Remove( b );
			} else { 
				probe = Set<IEdge<V, W>>.GetEmpty( this.Empty );
			}
			outBound = outBound.Add( b, probe.Add( edge ) );
			return new Graph<V, W>( 
				this.Vertexes.Add( a ).Add( b ), 
				edges, 
				outBound, 
				this.WeightComparer, 
				comparer, 
				GraphConnectivity.Untested  
			);
		}
		public IGraph<V, W> Connect( V a, V b, W weight ) { 
			System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
			if ( comparer.Equals( a, b ) ) { 
				throw new System.NotSupportedException();
			}
			IEdge<V, W> edge;
			if ( this.TryGetEdge( a, b, out edge ) ) { 
				throw new DuplicateKeyException();
			}
			return this.Add( new Edge<V, W>( a, b, weight, this.VertexComparer ) );
		}
		public IGraph<V, W> Remove( V vertex ) { 
			ISet<V> verts = this.Vertexes;
			if ( !verts.Contains( vertex ) ) { 
				return this;
			}
			verts = verts.Remove( vertex );
			if ( verts.IsEmpty ) { 
				return this.Empty;
			}
			ISet<IEdge<V, W>> edges = this.Edges;
			IHashTable<V, ISet<IEdge<V, W>>> outBound = this.OutBound;
			if ( outBound.ContainsKey( vertex ) ) { 
				foreach ( IEdge<V, W> e in outBound[ vertex ] ) { 
					edges = edges.Remove( e );
				}
				outBound = outBound.Remove( vertex );
			}
			return new Graph<V, W>( verts, edges, outBound, this.WeightComparer, this.VertexComparer, GraphConnectivity.Untested );
		}
		public IGraph<V, W> Remove( IEdge<V, W> edge ) { 
			ISet<IEdge<V, W>> edges = this.Edges;
			if ( !edges.Contains( edge ) ) { 
				return this;
			}
			edges = edges.Remove( edge );
			IHashTable<V, ISet<IEdge<V, W>>> outBound = this.OutBound;
			ISet<IEdge<V, W>> probe;
			if ( outBound.ContainsKey( edge.A ) ) { 
				probe = outBound[ edge.A ];
				outBound = outBound.Remove( edge.A ).Add( edge.A, probe.Remove( edge ) );
			}
			if ( outBound.ContainsKey( edge.B ) ) { 
				probe = outBound[ edge.B ];
				outBound = outBound.Remove( edge.B ).Add( edge.B, probe.Remove( edge ) );
			}
			return new Graph<V, W>( this.Vertexes, edges, outBound, this.WeightComparer, this.VertexComparer, GraphConnectivity.Untested );
		}
		public IGraph<V, W> Update( IEdge<V, W> x, IEdge<V, W> y ) { 
			if ( null == y ) { 
				throw new System.ArgumentNullException( "y" );
			} else if ( null == x ) { 
				throw new System.ArgumentNullException( "x" );
			} else if ( x.Vertexes != y.Vertexes ) {
				throw new System.ArgumentOutOfRangeException( "y" );
			}
			ISet<IEdge<V, W>> edges = this.Edges;
			if ( !edges.Contains( x ) ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			}
			IComparer<W> wComparer = this.WeightComparer;
			if ( wComparer.Equals( x.Weight, y.Weight ) ) { 
				return this;
			}

			edges = edges.Remove( x ).Add( y );
			IHashTable<V, ISet<IEdge<V, W>>> outBound = this.OutBound;
			ISet<IEdge<V, W>> probe;
			V a = x.A;
			if ( outBound.TryGetValue( a, out probe ) ) { 
				outBound = outBound.Remove( a ).Add( a, probe.Remove( x ).Add( y ) );
			}
			V b = x.B;
			if ( outBound.TryGetValue( b, out probe ) ) { 
				outBound = outBound.Remove( b ).Add( b, probe.Remove( x ).Add( y ) );
			}
			return new Graph<V, W>( this.Vertexes, edges, outBound, wComparer, this.VertexComparer, (GraphConnectivity)myConnectivity );
		}
		public IGraph<V, W> Update( IEdge<V, W> edge, W weight ) { 
			if ( null == edge ) { 
				throw new System.ArgumentNullException( "edge" );
			}
			return this.Update( edge, new Edge<V, W>( edge.A, edge.B, weight, this.VertexComparer ) );
		}
		public ISet<IEdge<V, W>> GetAdjacentTo( V vertex ) { 
			ISet<IEdge<V, W>> edges;
			if ( this.OutBound.TryGetValue( vertex, out edges ) ) { 
				return edges;
			} else { 
				return Set<IEdge<V, W>>.GetEmpty( this.Empty );
			}
		}
		public ISet<IGraph<V, W>> AsForest() { 
			if ( null == myForest ) { 
				System.Threading.Interlocked.CompareExchange<ISet<IGraph<V, W>>>( 
					ref myForest, 
					this.Empty.AsForest().Add( new GraphSolvers.ForestPartition<V, W>( this ).Solve() ), 
					null 
				);
			}
			return myForest;
		}

		public System.Int32 GetHashCode( IEdge<V, W> obj ) { 
			return this.Empty.GetHashCode( obj );
		}
		public System.Boolean Equals( IEdge<V, W> x, IEdge<V, W> y ) { 
			return this.Empty.Equals( x, y );
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static IGraph<V, W> GetEmpty() { 
			return EmptyGraph.Get();
		}
		public static IGraph<V, W> GetEmpty( IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) { 
			if ( null == vertexComparer ) { 
				throw new System.ArgumentNullException( "vertexComparer" );
			} else if ( null == weightComparer ) { 
				throw new System.ArgumentNullException( "weightComparer" );
			}

			return EmptyGraph.Get( weightComparer, vertexComparer );
		}
		#endregion static methods

	}

}