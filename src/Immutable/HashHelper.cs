namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	internal static class HashHelper { 

		#region methods
		internal static System.Collections.Generic.IEnumerator<IHashTable<K, T>> GetBucketEnumerator<K, T>( IHashTable<K, T> hashTable ) { 
			if ( null == hashTable ) { 
				throw new System.ArgumentNullException( "hashTable" );
			} else if ( hashTable.IsEmpty ) { 
				yield break;
			}

			using ( System.Collections.Generic.IEnumerator<LeafHashBucketBase<K, T>> e = GetBuckets<K, T>( hashTable ) ) { 
				while ( e.MoveNext() ) { 
					yield return (IHashTable<K, T>)e.Current;
				}
			}
		}
		internal static System.Collections.Generic.IEnumerator<LeafHashBucketBase<K, T>> GetBuckets<K, T>( IHashTable<K, T> table ) { 
			if ( null == table ) { 
				throw new System.ArgumentNullException( "table" );
			} else if ( table.IsEmpty ) { 
				yield break;
			}

			IQueue<IHashTable<K, T>> bucket = Queue<IHashTable<K, T>>.GetEmpty().Enqueue( table );
			IHashTable<K, T> probe;
			IHashTable<K, T> test;
			LeafHashBucketBase<K, T> one;
			while ( !bucket.IsEmpty ) { 
				probe = bucket.Peek();
				bucket = bucket.Dequeue();
				if ( 0 < probe.Count ) { 
					if ( 1 < probe.Length ) { 
						for ( System.Int32 i = probe.Length - 1; 0 <= i; i-- ) { 
							test = probe.GetBucket( i );
							if ( 0 < test.Count ) { 
								bucket = bucket.Enqueue( test );
							}
						}
					} else { 
						one = ( probe as LeafHashBucketBase<K, T> );
						if ( null != one ) { 
							yield return one;
						}
					}
				}
			}
		}
		public static IHashTable<K, T> ToSize<K, T>( IHashTable<K, T> table ) { 
			if ( null == table ) { 
				throw new System.ArgumentNullException( "table" );
			} else if ( table.IsEmpty ) { 
				return EmptyHashBucket<K, T>.Get( table.HashLogic );
			}

			IStack<LeafHashBucketBase<K, T>> bucket = Stack<LeafHashBucketBase<K, T>>.GetEmpty();
			using ( System.Collections.Generic.IEnumerator<LeafHashBucketBase<K, T>> e = GetBuckets( table ) ) { 
				while ( e.MoveNext() ) { 
					bucket = bucket.Push( e.Current );
				}
			}
			if ( 1 == bucket.Count ) { 
				return bucket.Peek();
			} else { 
				return Resize<K, T>( bucket, PrimeHelper.GetNextPrime( bucket.Count ) );
			}
		}
		public static IHashTable<K, T> Rehash<K, T>( IHashTable<K, T> table ) { 
			if ( null == table ) { 
				throw new System.ArgumentNullException( "table" );
			} else if ( table.IsEmpty ) { 
				return table.Empty;
			}
			IHashTable<K, T> output = table.Empty;
			foreach ( System.Collections.Generic.KeyValuePair<K, T> kvp in table ) { 
				output = output.Add( kvp.Key, kvp.Value );
			}
			return output;
		}
		public static IHashTable<K, T> Resize<K, T>( IHashTable<K, T> table, System.Int32 size ) {
			if ( ( size < PrimeHelper.MinPrime ) || ( PrimeHelper.MaxPrime < size ) ) {
				throw new System.ArgumentOutOfRangeException( "size" );
			} else if ( null == table ) {
				throw new System.ArgumentNullException( "table" );
			}

			using ( System.Collections.Generic.IEnumerator<LeafHashBucketBase<K, T>> e = GetBuckets( table ) ) {
				return Resize<K, T>( e, size );
			}
		}
		private static IHashTable<K, T> Resize<K, T>( System.Collections.Generic.IEnumerable<LeafHashBucketBase<K, T>> bucket, System.Int32 size ) {
			if ( ( size < PrimeHelper.MinPrime ) || ( PrimeHelper.MaxPrime < size ) ) {
				throw new System.ArgumentOutOfRangeException( "size" );
			} else if ( null == bucket ) {
				throw new System.ArgumentNullException( "bucket" );
			}

			using ( System.Collections.Generic.IEnumerator<LeafHashBucketBase<K, T>> e = bucket.GetEnumerator() ) {
				return Resize<K, T>( e, size );
			}
		}
		private static IHashTable<K, T> Resize<K, T>( System.Collections.Generic.IEnumerator<LeafHashBucketBase<K, T>> bucket, System.Int32 size ) {
			if ( size < PrimeHelper.MinPrime ) {
				throw new System.ArgumentOutOfRangeException( "size" );
			} else if ( null == bucket ) {
				throw new System.ArgumentNullException( "bucket" );
			} else if ( !bucket.MoveNext() ) {
				throw new System.ArgumentNullException( "bucket" );
			}

			IHashTable<K, T> output = new HashTable<K, T>( bucket.Current.HashLogic, size );
			do {
				output = AddBucket<K, T>( output, bucket.Current );
			} while ( bucket.MoveNext() );
			return output;
		}
		private static IHashTable<K, T> AddBucket<K, T>( IHashTable<K, T> table, LeafHashBucketBase<K, T> bucket ) {
			if ( null == bucket ) {
				throw new System.ArgumentNullException( "bucket" );
			} else if ( null == table ) {
				throw new System.ArgumentNullException( "table" );
			}

			IStack<IHashTable<K, T>> list = Stack<IHashTable<K, T>>.GetEmpty().Push( table );
			IStack<System.Int32> index = Stack<System.Int32>.GetEmpty();
			IHashTable<K, T> current;
			System.Int32 i;
			System.Int32 code = bucket.KeyHash;
			LeafHashBucketBase<K, T> probe;
			do {
				current = list.Peek();
				i = code % current.Length;
				index = index.Push( i );
				current = current.GetBucket( i );
				list = list.Push( current );
			} while ( 1 < current.Length );
			if ( current.IsEmpty ) {
				list = list.Pop().Push( bucket );
			} else {
				probe = ( current as LeafHashBucketBase<K, T> );
				if ( null == probe ) {
					throw new System.NotImplementedException();
				} else {
					current = HashHelper.HashUp<K, T>(
						current.HashLogic,
						PrimeHelper.GetNextPrime( current.Length ),
						probe,
						bucket
					);
				}
				list = list.Pop().Push( current );
			}

			return Unwind<K, T>( list, index );
		}

		internal static IHashTable<K, T> Unwind<K, T>( HashPath<K, T> path ) { 
			if ( null == path ) { 
				throw new System.ArgumentNullException( "path" );
			} else if ( ( null == path.Bucket ) || path.Bucket.IsEmpty || ( null == path.Index ) ) { 
				throw new System.ArgumentException( null, "path");
			} else if ( path.Bucket.Count != ( path.Index.Count + 1 ) ) { 
				throw new System.ArgumentException( null, "path" );
			}

			return Unwind<K, T>( path.Bucket, path.Index );
		}
		internal static IHashTable<K, T> Unwind<K, T>( IStack<IHashTable<K, T>> bucket, IStack<System.Int32> index ) { 
			if ( ( null == bucket ) || bucket.IsEmpty ) { 
				throw new System.ArgumentNullException( "bucket" );
			} else if ( null == index ) { 
				throw new System.ArgumentNullException( "index" );
			} else if ( bucket.Count != ( index.Count + 1 ) ) { 
				throw new System.ArgumentException( null, "index" );
			}

			IHashTable<K, T> current;
			IHashTable<K, T> parent;
			IHashTable<K, T> empty = bucket.Peek().Empty;
			while ( 1 < bucket.Count ) { 
				current = bucket.Peek();
				bucket = bucket.Pop();
				if ( 0 == current.Count ) { 
					current = empty;
				}
				parent = bucket.Peek();
				if ( parent.Length <= current.Length ) { 
					parent = Promote<K, T>( parent, index.Peek(), current );
				} else { 
					parent = parent.SetBucket( index.Peek(), current );
				}
				index = index.Pop();
				bucket = bucket.Pop().Push( parent );
			}

			return bucket.Peek();
		}
		private static IHashTable<K, T> Promote<K, T>( IHashTable<K, T> parent, System.Int32 index, IHashTable<K, T> child ) { 
			if ( null == child ) { 
				throw new System.ArgumentNullException( "child" );
			} else if ( null == parent ) { 
				throw new System.ArgumentNullException( "parent" );
			} else if ( ( index < 0 ) || ( parent.Length <= index ) ) { 
				throw new System.IndexOutOfRangeException( "index" );
			}

			parent = parent.SetBucket( index, parent.Empty );
			using ( System.Collections.Generic.IEnumerator<LeafHashBucketBase<K, T>> e = GetBuckets( parent ) ) { 
				while ( e.MoveNext() ) { 
					child = AddBucket( child, e.Current );
				}
			}
			return child;
		}

		internal static System.Int32 GetHash<K>( System.Collections.Generic.IEqualityComparer<K> comparer, K key ) {
			if ( null == comparer ) {
				throw new System.ArgumentNullException( "comparer" );
			}

			return ( comparer.GetHashCode( key ) & System.Int32.MaxValue );
		}

		internal static IHashTable<K, T> HashFrom<K, T>( IHashLogic<K, T> logic, System.Int32 len, LeafHashBucketBase<K, T> first, LeafHashBucketBase<K, T> second, NextHashTableLength next ) {
			if ( null == next ) { 
				throw new System.ArgumentNullException( "next" );
			} else if ( null == second ) { 
				throw new System.ArgumentNullException( "second" );
			} else if ( null == first ) { 
				throw new System.ArgumentNullException( "first" );
			} else if ( null == logic ) { 
				throw new System.ArgumentNullException( "logic" );
			} else if ( ( len < PrimeHelper.MinPrime ) || ( PrimeHelper.MaxPrime < len ) ) {
				throw new System.ArgumentOutOfRangeException( "len" );
			} else if ( first.KeyHash == second.KeyHash ) {
				IHashTable<K, T> from = ( first.Count < second.Count ) ? second : first;
				IHashTable<K, T> to = ( from == second ) ? first : second;
				foreach ( System.Collections.Generic.KeyValuePair<K, T> pair in from ) {
					to = to.Add( pair.Key, pair.Value );
					if ( 1 < to.Length ) {
						throw new System.NotImplementedException();
					}
				}
				return new HashTable<K, T>( logic, len ).SetBucket( first.KeyHash % len, to );
			}

			System.Int32 fcode = first.KeyHash;
			System.Int32 scode = second.KeyHash;
			System.Int32 f;
			System.Int32 s;
			System.Boolean isMatch;
			do {
				f = fcode % len;
				s = scode % len;
				isMatch = ( f == s );
				if ( isMatch ) {
					len = next( len );
				}
			} while ( isMatch );

			return new HashTable<K, T>( logic, len ).SetBucket( f, first ).SetBucket( s, second );
		}
		internal static IHashTable<K, T> HashDown<K, T>( IHashLogic<K, T> logic, System.Int32 len, LeafHashBucketBase<K, T> first, LeafHashBucketBase<K, T> second ) {
			if ( null == second ) { 
				throw new System.ArgumentNullException( "second" );
			} else if ( null == first ) { 
				throw new System.ArgumentNullException( "first" );
			} else if ( null == logic ) { 
				throw new System.ArgumentNullException( "logic" );
			} else if ( len < PrimeHelper.MinPrime ) { 
				throw new System.ArgumentOutOfRangeException( "len" );
			}

			return HashFrom( logic, len, first, second, PrimeHelper.GetPreviousPrime );
		}
		internal static IHashTable<K, T> HashUp<K, T>( IHashLogic<K, T> logic, System.Int32 len, LeafHashBucketBase<K, T> first, LeafHashBucketBase<K, T> second ) {
			if ( null == second ) {
				throw new System.ArgumentNullException( "second" );
			} else if ( null == first ) {
				throw new System.ArgumentNullException( "first" );
			} else if ( null == logic ) {
				throw new System.ArgumentNullException( "logic" );
			} else if ( PrimeHelper.MaxPrime < len ) {
				throw new System.ArgumentOutOfRangeException( "len" );
			}

			return HashFrom( logic, len, first, second, PrimeHelper.GetNextPrime );
		}

		public static System.Boolean Equals<K, T>( IHashTable<K, T> a, IHashTable<K, T> b ) { 
			if ( ( null == (System.Object)a ) && ( null == (System.Object)b ) ) { 
				return true;
			} else if ( ( null == (System.Object)a ) || ( null == (System.Object)b ) ) { 
				return false;
			} else if ( System.Object.ReferenceEquals( a, b ) ) { 
				return true;
			} else if ( a.Count != b.Count ) { 
				return false;
			} else { 
				IHashLogic<K, T> logic = a.HashLogic;
				if ( !logic.Equals( b.HashLogic ) ) { 
					return false;
				} else { 
					System.Collections.Generic.IEqualityComparer<T> comparer = logic.ItemComparer;
					T t;
					foreach ( K k in a.Keys ) { 
						if ( !b.TryGetValue( k, out t ) ) { 
							return false;
						} else if ( !comparer.Equals( a[ k ], t ) ) { 
							return false;
						}
					}
					return true;
				}
			}
		}
		#endregion methods

	}

}