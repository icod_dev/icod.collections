namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public abstract class DirectedVertexTraverser<V, W> : DigraphSolver<V, W, System.Collections.Generic.IEnumerable<V>> {

		#region fields
		private readonly System.Boolean myIsVertexSet;
		private readonly V myVertex;
		#endregion fields


		#region .ctor
		protected DirectedVertexTraverser( IDigraph<V, W> graph ) : base( graph ) { 
			myIsVertexSet = false;
			myVertex = default( V );
		}
		protected DirectedVertexTraverser( IDigraph<V, W> graph, V vertex ) : base( graph ) { 
			if ( !graph.Vertexes.Contains( vertex ) ) { 
				throw new System.ArgumentOutOfRangeException( "vertex" );
			}
			myIsVertexSet = true;
			myVertex = vertex;
		}
		#endregion .ctor


		#region properties
		public System.Boolean IsVertexSet {
			get {
				return myIsVertexSet;
			}
		}
		public V Vertex {
			get {
				return myVertex;
			}
		}
		#endregion properties

	}

}