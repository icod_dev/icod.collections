namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class KeyLogic<K, T> : IKeyLogic<K, T>, System.IEquatable<IKeyLogic<K, T>> { 

		#region fields
		private static readonly System.Int32 theHashCode;
		private static IKeyLogic<K, T> theDefault;
		public static KeyGenerator<T, K> DefaultGenerator;
		private static System.Collections.Generic.IDictionary< 
			KeyGenerator<T, K>, 
			System.Collections.Generic.IDictionary< 
				System.Collections.Generic.IComparer<K>, 
				IKeyLogic<K, T> 
			> 
		> theLogic;

		private readonly System.Int32 myHashCode;
		private readonly KeyGenerator<T, K> myGenerator;
		private readonly System.Collections.Generic.IComparer<K> myComparer;
		#endregion fields


		#region .ctor
		static KeyLogic() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}

			DefaultGenerator = delegate( T value ) { 
				throw new System.NotImplementedException();
			};

			theDefault = null;

			theLogic = new System.Collections.Generic.Dictionary< 
				KeyGenerator<T, K>, 
				System.Collections.Generic.IDictionary< 
					System.Collections.Generic.IComparer<K>, 
					IKeyLogic<K, T> 
				> 
			>();
		}

		private KeyLogic() : base() { 
			myHashCode = theHashCode;
		}
		private KeyLogic( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> comparer ) : this() { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}
			myGenerator = generator;
			myComparer = comparer;
			unchecked { 
				myHashCode += myGenerator.GetHashCode();
				myHashCode += myComparer.GetHashCode();
			}
		}
		#endregion fields


		#region properties
		public static IKeyLogic<K, T> Default { 
			get { 
				if ( null == theDefault ) { 
					System.Threading.Interlocked.CompareExchange<IKeyLogic<K, T>>( 
						ref theDefault, 
						Get(), 
						null 
					);
				}
				return theDefault;
			}
		}

		public KeyGenerator<T, K> Generator { 
			get { 
				return myGenerator;
			}
		}
		public System.Collections.Generic.IComparer<K> Comparer { 
			get { 
				return myComparer;
			}
		}
		#endregion fields


		#region methods
		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		public sealed override System.Boolean Equals( System.Object other ) { 
			if ( true == KeyLogic<K, T>.ReferenceEquals( this, other ) ) { 
				return true;
			} else { 
				return this.Equals( (IKeyLogic<K, T>)other );
			}
		}
		public System.Boolean Equals( IKeyLogic<K, T> other ) { 
			if ( null == (System.Object)other ) { 
				return false;
			} else if ( System.Object.ReferenceEquals( this, other ) ) { 
				return true;
			} else { 
				return ( 
					System.Object.ReferenceEquals( this.Generator, other.Generator ) 
					&& System.Object.ReferenceEquals( this.Comparer, other.Comparer ) 
				);
			}
		}
		#endregion methods


		#region static methods
		public static IKeyLogic<K, T> Get() { 
			return Get( DefaultGenerator );
		}
		public static IKeyLogic<K, T> Get( KeyGenerator<T, K> generator ) { 
			if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}
			return Get( generator, Comparer<K>.Default );
		}
		public static IKeyLogic<K, T> Get( System.Collections.Generic.IComparer<K> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			return Get( DefaultGenerator, comparer );
		}
		public static IKeyLogic<K, T> Get( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}
			IKeyLogic<K, T> output = null;
			System.Collections.Generic.IDictionary<System.Collections.Generic.IComparer<K>, IKeyLogic<K, T>> leaf;
			lock ( theLogic ) { 
				if ( theLogic.ContainsKey( generator ) ) { 
					leaf = theLogic[ generator ];
				} else { 
					leaf = new System.Collections.Generic.Dictionary<System.Collections.Generic.IComparer<K>, IKeyLogic<K, T>>();
					theLogic.Add( generator, leaf );
				}
			}
			lock ( leaf ) { 
				if ( leaf.ContainsKey( comparer ) ) { 
					output = leaf[ comparer ];
				} else { 
					output = new KeyLogic<K,T>( generator, comparer );
					leaf.Add( comparer, output );
				}
			}
			return output;
		}

		public static System.Boolean operator ==( KeyLogic<K, T> a, KeyLogic<K, T> b ) { 
			if ( ( null == (System.Object)a ) && ( null == (System.Object)b ) ) { 
				return true;
			} else if ( ( null == (System.Object)a ) || ( null == (System.Object)b ) ) { 
				return false;
			} else { 
				return a.Equals( b );
			}
		}
		public static System.Boolean operator !=( KeyLogic<K, T> a, KeyLogic<K, T> b ) { 
			return !( a == b );
		}
		#endregion static methods

	}

}