namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IPriorityQueue<P, T> : System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<P, T>> { 

		IPriorityQueue<P, T> Empty { 
			get;
		}
		System.Int32 Count { 
			get;
		}
		System.Boolean IsEmpty { 
			get;
		}
		System.Collections.Generic.IComparer<P> Comparer { 
			get;
		}
		System.Collections.Generic.IEnumerable<P> Priorities { 
			get;
		}

		System.Collections.Generic.KeyValuePair<P, T> PeekMin();
		System.Collections.Generic.KeyValuePair<P, T> PeekMax();
		IPriorityQueue<P, T> Add( P priority, T item );
		IPriorityQueue<P, T> DequeueMin();
		IPriorityQueue<P, T> DequeueMax();
		System.Collections.Generic.KeyValuePair<P, T> Peek( P priority );
		IPriorityQueue<P, T> Dequeue( P priority );
		System.Boolean ContainsPriority( P priority );
		System.Boolean Contains( System.Collections.Generic.KeyValuePair<P, T> item );
		System.Boolean Contains( P priority, T item );
		System.Boolean ContainsItem( T item );
		System.Boolean ContainsItem( T item, System.Collections.Generic.IEqualityComparer<T> comparer );
		IPriorityQueue<P, T> DecreasePriority( T item );
		IPriorityQueue<P, T> DecreasePriority( T item, System.Collections.Generic.IEqualityComparer<T> comparer );
		IPriorityQueue<P, T> IncreasePriority( T item );
		IPriorityQueue<P, T> IncreasePriority( T item, System.Collections.Generic.IEqualityComparer<T> comparer );
		IPriorityQueue<P, T> UpdatePriority( T item, P priority );
		IPriorityQueue<P, T> UpdatePriority( T item, P priority, System.Collections.Generic.IEqualityComparer<T> comparer );

	}

}