namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IKeyLogic<K, V> : System.IEquatable<IKeyLogic<K, V>> { 

		System.Collections.Generic.IComparer<K> Comparer { 
			get;
		}
		KeyGenerator<V, K> Generator { 
			get;
		}

	}

}