namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IHashTable<K, T> : System.Collections.Generic.IDictionary<K, T>, System.IEquatable<IHashTable<K, T>> { 

		IHashLogic<K, T> HashLogic { 
			get;
		}
		IHashTable<K, T> Empty { 
			get;
		}
		System.Int32 Length { 
			get;
		}
		System.Int32 Occupancy { 
			get;
		}
		System.Double LoadFactor { 
			get;
		}
		System.Boolean IsEmpty { 
			get;
		}

		System.Boolean ContainsValue( T item );
		HashPath<K, T> Probe( K key );
		IHashTable<K, T> GetBucket( System.Int32 index );
		IHashTable<K, T> SetBucket( System.Int32 index, IHashTable<K, T> bucket );
		new IHashTable<K, T> Add( K key, T item );
		new IHashTable<K, T> Remove( K key );

	}

}