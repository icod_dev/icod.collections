namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IPath<V, W> : IQueue<System.Collections.Generic.KeyValuePair<V, W>>, System.Collections.Generic.IEnumerable<V> { 

		new IPath<V, W> Empty { 
			get;
		}
		W Weight { 
			get;
		}
		V Source { 
			get;
		}
		V Destination { 
			get;
		}

		IPath<V, W> Enqueue( V vertex, W weight );
		System.Boolean Contains( V vertex, W weight );
		System.Boolean Contains( V vertex );
		System.Boolean Contains( V vertex, System.Collections.Generic.IEqualityComparer<V> comparer );
		new IPath<V, W> Reverse();

	}

}