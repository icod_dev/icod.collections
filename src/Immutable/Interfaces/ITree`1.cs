namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface ITree<T> : System.Collections.Generic.ICollection<ITree<T>>, System.Collections.Generic.ICollection<T> { 

		ITree<T> Empty { 
			get;
		}
		new System.Int32 Count { 
			get;
		}
		new System.Boolean IsReadOnly { 
			get;
		}
		System.Int32 Height { 
			get;
		}
		System.Int32 Length { 
			get;
		}
		System.Boolean IsEmpty { 
			get;
		}
		System.Boolean IsLeaf { 
			get;
		}
		System.Boolean IsPerfect { 
			get;
		}
		System.Boolean IsFull { 
			get;
		}
		System.Int32 SlackFactor { 
			get;
		}

		T Value { 
			get;
		}
		System.Collections.Generic.ICollection<T> Values { 
			get;
		}

		System.Collections.Generic.IEnumerable<ITree<T>> Branches { 
			get;
		}

		new System.Collections.Generic.IEnumerator<ITree<T>> GetEnumerator();

	}

}