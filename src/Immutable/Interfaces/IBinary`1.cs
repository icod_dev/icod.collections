namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IBinary<T> { 

		System.Int32 BalanceFactor { 
			get;
		}
		System.Boolean IsLeftHeavy { 
			get;
		}
		System.Boolean IsRightHeavy { 
			get;
		}
		T Left { 
			get;
		}
		T Right { 
			get;
		}

		System.Boolean IsInBalance( T other );
		T SetLeft( T left );
		T SetRight( T right );
		T RotateLeft();
		T RotateRight();
		T RotateLeftRight();
		T RotateRightLeft();

	}

}