namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface ISearchTree<K, T> : ITree<T>, System.Collections.Generic.ICollection<ISearchTree<K, T>>, System.Collections.Generic.IDictionary<K, T> { 

		new ISearchTree<K, T> Empty { 
			get;
		}
		new System.Int32 Count {
			get;
		}
		new System.Boolean IsReadOnly {
			get;
		}
		new System.Collections.Generic.IEnumerable<ISearchTree<K, T>> Branches { 
			get;
		}
		new ISearchTree<K, T> this[ K key ] { 
			get;
		}
		ISearchTree<K, T> Min { 
			get;
		}
		ISearchTree<K, T> Max { 
			get;
		}
		K Key { 
			get;
		}
		IKeyLogic<K, T> KeyLogic { 
			get;
		}
		new System.Collections.Generic.ICollection<T> Values { 
			get;
		}

		ISearchTree<K, T> Find( K key );
		new ISearchTree<K, T> Add( T item );
		new ISearchTree<K, T> Add( K key, T item );
		new ISearchTree<K, T> Remove( K key );
		ISearchTree<K, T> RemoveItem( T item );
		ISearchTree<K, T> FindItem( T item );

		new System.Collections.Generic.IEnumerator<ISearchTree<K, T>> GetEnumerator();

	}

}