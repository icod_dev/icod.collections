namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IDigraph<V, W> : IGraph<V, W> { 

		new IDigraph<V, W> Empty { 
			get;
		}
		System.Boolean IsCyclic { 
			get;
		}
		System.Boolean IsStronglyConnected { 
			get;
		}

		new IDigraph<V, W> Add( V vertex );
		new IDigraph<V, W> Add( IEdge<V, W> edge );
		new IDigraph<V, W> Connect( V a, V b, W weight );
		new IDigraph<V, W> Remove( V vertex );
		new IDigraph<V, W> Remove( IEdge<V, W> edge );
		new IDigraph<V, W> Update( IEdge<V, W> x, IEdge<V, W> y );
		new IDigraph<V, W> Update( IEdge<V, W> edge, W weight );
		new ISet<IDigraph<V, W>> AsForest();
		IGraph<V, W> AsMinUndirected();
		IGraph<V, W> AsMaxUndirected();
		IGraph<V, System.Boolean> AsUndirected();
		ISet<IEdge<V, W>> GetOutBound( V vertex );
		ISet<IEdge<V, W>> GetInBound( V vertex );

	}

}