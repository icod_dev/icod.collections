namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IDeque<T> : System.Collections.Generic.ICollection<T>, IQueue<T>, IStack<T> { 

		new IDeque<T> Empty { 
			get;
		}
		new System.Int32 Count { 
			get;
		}
		new System.Boolean IsEmpty { 
			get;
		}

		T PeekLeft();
		T PeekRight();
		IDeque<T> EnqueueLeft( T item );
		IDeque<T> EnqueueRight( T item );
		IDeque<T> DequeueLeft();
		IDeque<T> DequeueRight();
		new IDeque<T> Reverse();
		new System.Boolean Contains( T item, System.Collections.Generic.IEqualityComparer<T> comparer );

		new System.Collections.Generic.IEnumerator<T> GetEnumerator();

	}

}