namespace Icod.Collections.Immutable {

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IStack<T> : System.Collections.Generic.ICollection<T> { 

		IStack<T> Empty { 
			get;
		}
		System.Boolean IsEmpty { 
			get;
		}

		IStack<T> Push( T item );
		IStack<T> Pop();
		T Peek();

		IStack<T> Duplicate();
		IStack<T> Copy( System.Int32 count );
		IStack<T> Exchange();
		IStack<T> Reverse();

		IStack<T> Rotate( System.Int32 shift );
		IStack<T> Rotate( System.Int32 count, System.Int32 shift );
		System.Boolean Contains( T item, System.Collections.Generic.IEqualityComparer<T> comparer );

		new System.Collections.Generic.IEnumerator<T> GetEnumerator();

	}

}