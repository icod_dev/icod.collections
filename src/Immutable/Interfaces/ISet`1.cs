namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface ISet<T> : System.Collections.Generic.ICollection<T>, System.IEquatable<ISet<T>> { 

		ISet<T> Empty { 
			get;
		}
		System.Boolean IsEmpty { 
			get;
		}
		System.Collections.Generic.IEqualityComparer<T> Comparer { 
			get;
		}

		new ISet<T> Add( T item );
		ISet<T> Add( System.Collections.Generic.IEnumerable<T> collection );
		new ISet<T> Remove( T item );
		ISet<T> Union( ISet<T> other );
		ISet<T> Intersection( ISet<T> other );
		ISet<T> SymmetricDifference( ISet<T> other );
		ISet<T> Add( ISet<T> other );
		ISet<T> Subtract( ISet<T> other );
		ISet<ISet<T>> Partition( System.Predicate<T> predicate );
		ISet<ISet<T>> Partition( Func<T, T, System.Boolean> predicate );

	}

}