namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IHashLogic<K, T> { 

		System.Collections.Generic.IEqualityComparer<K> KeyComparer { 
			get;
		}
		System.Collections.Generic.IEqualityComparer<T> ItemComparer { 
			get;
		}

	}

}