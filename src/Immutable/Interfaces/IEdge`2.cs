namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IEdge<V, W> : System.Collections.Generic.ICollection<V>, System.Collections.Generic.IEqualityComparer<V> { 

		V A { 
			get;
		}
		V B { 
			get;
		}
		W Weight { 
			get;
		}
		V this[ V vertex ] { 
			get;
		}
		ISet<V> Vertexes { 
			get;
		}
		System.Collections.Generic.IEqualityComparer<V> Comparer { 
			get;
		}
		System.Boolean IsReflexive { 
			get;
		}

	}

}