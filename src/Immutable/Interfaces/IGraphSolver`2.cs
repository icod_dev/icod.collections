namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IGraphSolver<G, R> { 

		System.IAsyncResult BeginSolve( System.AsyncCallback callback, G graph );
		R Solve();
		R EndSolve( System.IAsyncResult result );

	}

}