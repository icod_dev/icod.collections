namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IBinarySearchTree<K, T> : IBinary<IBinarySearchTree<K, T>>, ISearchTree<K, T>, System.Collections.Generic.ICollection<IBinarySearchTree<K, T>> { 

		new IBinarySearchTree<K, T> Empty { 
			get;
		}
		new System.Int32 Count {
			get;
		}
		new System.Boolean IsReadOnly {
			get;
		}
		new System.Collections.Generic.IEnumerable<IBinarySearchTree<K, T>> Branches { 
			get;
		}

		new IBinarySearchTree<K, T> this[ K key ] { 
			get;
		}
		new IBinarySearchTree<K, T> Min {
			get;
		}
		new IBinarySearchTree<K, T> Max { 
			get;
		}

		new IBinarySearchTree<K, T> Add( T item );
		new IBinarySearchTree<K, T> Add( K key, T item );
		new IBinarySearchTree<K, T> Remove( K key );
		new IBinarySearchTree<K, T> RemoveItem( T item );
		new IBinarySearchTree<K, T> Find( K key );
		new IBinarySearchTree<K, T> FindItem( T item );

		IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other );

		IBinarySearchTree<K, T> BalanceTree( IBinarySearchTree<K, T> other );

		new System.Collections.Generic.IEnumerator<IBinarySearchTree<K, T>> GetEnumerator();

	}

}