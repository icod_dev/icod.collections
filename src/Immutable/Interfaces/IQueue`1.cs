namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IQueue<T> : System.Collections.Generic.ICollection<T> { 

		IQueue<T> Empty { 
			get;
		}
		System.Boolean IsEmpty { 
			get;
		}

		IQueue<T> Enqueue( T item );
		IQueue<T> Dequeue();
		T Peek();

		IQueue<T> Duplicate();
		IQueue<T> Copy( System.Int32 count );
		IQueue<T> Exchange();
		IQueue<T> Reverse();

		IQueue<T> Rotate( System.Int32 shift );
		IQueue<T> Rotate( System.Int32 count, System.Int32 shift );
		System.Boolean Contains( T item, System.Collections.Generic.IEqualityComparer<T> comparer );

		new System.Collections.Generic.IEnumerator<T> GetEnumerator();

	}

}