namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IGraph<V, W> : System.Collections.Generic.IEqualityComparer<IEdge<V, W>> { 

		IGraph<V, W> Empty { 
			get;
		}
		System.Boolean IsEmpty { 
			get;
		}
		System.Boolean IsConnected { 
			get;
		}
		IComparer<W> WeightComparer { 
			get;
		}
		System.Collections.Generic.IEqualityComparer<V> VertexComparer { 
			get;
		}
		ISet<V> Vertexes { 
			get;
		}
		ISet<IEdge<V, W>> Edges { 
			get;
		}
		IEdge<V, W> this[ V a, V b ] { 
			get;
		}

		System.Boolean ContainsEdge( V a, V b );
		System.Boolean TryGetEdge( V a, V b, out IEdge<V, W> edge );
		System.Boolean IsSymmetric( V a, V b );
		System.Boolean IsSymmetric( IEdge<V, W> edge );
		IGraph<V, W> Add( V vertex );
		IGraph<V, W> Add( IEdge<V, W> edge );
		IGraph<V, W> Connect( V a, V b, W weight );
		IGraph<V, W> Remove( V vertex );
		IGraph<V, W> Remove( IEdge<V, W> edge );
		IGraph<V, W> Update( IEdge<V, W> x, IEdge<V, W> y );
		IGraph<V, W> Update( IEdge<V, W> edge, W weight );
		ISet<IEdge<V, W>> GetAdjacentTo( V vertex );
		ISet<IGraph<V, W>> AsForest();

	}

}