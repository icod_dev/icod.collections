namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IBinaryTree<T> : ITree<T>, IBinary<IBinaryTree<T>>, System.Collections.Generic.ICollection<IBinaryTree<T>> {

		new IBinaryTree<T> Empty { 
			get;
		}
		new System.Int32 Count { 
			get;
		}
		new System.Boolean IsReadOnly { 
			get;
		}
		new System.Collections.Generic.IEnumerable<IBinaryTree<T>> Branches { 
			get;
		}

		new System.Collections.Generic.IEnumerator<IBinaryTree<T>> GetEnumerator();

	}

}