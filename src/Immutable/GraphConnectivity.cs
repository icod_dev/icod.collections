namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	internal enum GraphConnectivity : System.Int32 { 

		Untested = 0, 
		Not = 1, 
		Weak = 2, 
		Strong = 3 

	}

}