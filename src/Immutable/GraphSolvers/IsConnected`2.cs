namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class IsConnected<V, W> : GraphSolver<V, W, System.Boolean> { 

		public IsConnected( IGraph<V, W> graph ) : base( graph ) { 
		}

		public sealed override System.Boolean Solve() { 
			IGraph<V, W> graph = this.Graph;

			if ( graph.Edges.IsEmpty || graph.Vertexes.IsEmpty || ( ( graph.Edges.Count + 1 ) < graph.Vertexes.Count ) ) { 
				return false;
			} else { 
				return ( 1 == graph.AsForest().Count );
			}
		}

	}

}