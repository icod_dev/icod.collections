namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class Dijkstra<V, W> : GraphSolver<V, W, IDigraph<V, W>> { 

		#region fields
		private readonly V mySource;
		private readonly Func<W, W, W> myAdd;
		#endregion fields


		#region .ctor
		public Dijkstra( IGraph<V, W> graph, V source, Func<W, W, W> add ) : base( graph ) { 
			if ( null == add ) { 
				throw new System.ArgumentNullException( "add" );
			} else if ( !graph.Vertexes.Contains( source ) ) { 
				throw new System.ArgumentException( "The source vertex must be contained within the specified graph.", "source" );
			}
			mySource = source;
			myAdd = add;
		}
		#endregion .ctor


		#region properties
		public V Source { 
			get { 
				return mySource;
			}
		}
		public Func<W, W, W> Add { 
			get { 
				return myAdd;
			}
		}
		#endregion properties


		#region methods
		public sealed override IDigraph<V, W> Solve() { 
			IGraph<V, W> graph = this.Graph;
			if ( graph is IDigraph<V, W> ) { 
				return new DirectedDijkstra<V, W>( graph as IDigraph<V, W>, mySource, myAdd ).Solve();
			}

			System.Collections.Generic.IEqualityComparer<V> vComparer = graph.VertexComparer;
			IComparer<W> wComparer = graph.WeightComparer;

			W def = default( W );
			IHashTable<V, W> dist = HashTable<V, W>.GetEmpty( vComparer, wComparer ).Add( mySource, def );
			IHashTable<V, V> previous = HashTable<V, V>.GetEmpty( vComparer, vComparer );
			IPriorityQueue<W, V> next = PriorityQueue<W, V>.GetEmpty( wComparer ).Add( def, mySource );
			V current;
			ISet<V> empty = Set<V>.GetEmpty( vComparer );
			ISet<V> potential;
			W alt;
			do { 
				current = next.PeekMin().Value;
				next = next.DequeueMin();
				if ( !graph.Vertexes.Contains( current ) ) { 
					continue;
				}
				potential = empty;
				foreach ( IEdge<V, W> e in graph.GetAdjacentTo( current ) ) { 
					potential = potential.Union( e.Vertexes );
				}
				foreach ( V v in potential.Remove( current ) ) { 
					alt = myAdd( dist[ current ], graph[ current, v ].Weight );
					if ( dist.ContainsKey( v ) ) { 
						if ( 0 < wComparer.Compare( dist[ v ], alt ) ) { 
							dist = dist.Remove( v ).Add( v, alt );
							previous = previous.Remove( v ).Add( v, current );
						}
					} else { 
						dist = dist.Add( v, alt );
						previous = previous.Add( v, current );
					}
					next = next.UpdatePriority( v, dist[ v ], vComparer );
				}
				graph = graph.Remove( current );
			} while ( !next.IsEmpty );

			graph = this.Graph;
			IDigraph<V, W> output = Digraph<V, W>.GetEmpty( wComparer, vComparer ).Add( mySource );
			V u;
			foreach ( V v in dist.Keys ) { 
				if ( previous.TryGetValue( v, out u ) ) { 
					output = output.Add( graph[ u, v ] );
				}
			}

			return output;
		}
		#endregion methods

	}

}