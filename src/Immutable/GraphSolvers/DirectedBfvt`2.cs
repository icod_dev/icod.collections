namespace Icod.Collections.Immutable.GraphSolvers {

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class DirectedBfvt<V, W> : DirectedVertexTraverser<V, W> {

		public DirectedBfvt( IDigraph<V, W> graph ) : base( graph ) { 
		}
		public DirectedBfvt( IDigraph<V, W> graph, V vertex ) : base( graph, vertex ) { 
		}


		public sealed override System.Collections.Generic.IEnumerable<V> Solve() { 
			IDigraph<V, W> graph = this.Graph;

			if ( graph.Vertexes.IsEmpty ) {
				yield break;
			} else if ( graph.Edges.IsEmpty ) {
				if ( this.IsVertexSet ) {
					yield return this.Vertex;
				} else {
					foreach ( V v in graph.Vertexes ) {
						yield return v;
						break;
					}
				}
				yield break;
			}

			ISet<V> verts = graph.Vertexes;
			IQueue<V> pump = Queue<V>.GetEmpty();
			if ( this.IsVertexSet ) {
				pump = pump.Enqueue( this.Vertex );
			} else {
				foreach ( V v in graph.Vertexes ) {
					pump = pump.Enqueue( v );
					break;
				}
			}
			V vertex;
			while ( !pump.IsEmpty ) { 
				vertex = pump.Peek();
				pump = pump.Dequeue();
				if ( !verts.Contains( vertex ) ) { 
					continue;
				}
				verts = verts.Remove( vertex );
				foreach ( IEdge<V, W> e in graph.GetOutBound( vertex ) ) { 
					if ( verts.Contains( e.B ) ) { 
						pump = pump.Enqueue( e.B );
					}
				}
				yield return vertex;
			};
		}

	}

}