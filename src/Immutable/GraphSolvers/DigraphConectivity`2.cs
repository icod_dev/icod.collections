namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	internal sealed class DigraphConnectivity<V, W> : DigraphSolver<V, W, GraphConnectivity> { 

		public DigraphConnectivity( IDigraph<V, W> graph ) : base( graph ) { 
		}

		public sealed override GraphConnectivity Solve() { 
			IDigraph<V, W> graph = this.Graph;

			if ( graph.Edges.IsEmpty || graph.Vertexes.IsEmpty || ( ( graph.Edges.Count + 1 ) < graph.Vertexes.Count ) ) { 
				return GraphConnectivity.Not;
			}

			GraphConnectivity isStrong = GraphConnectivity.Untested;
			System.Int32 vCount = graph.Vertexes.Count;
			System.Int32 probe;
			foreach ( V v in graph.Vertexes ) { 
				probe = 0;
				foreach ( V p in new DirectedDfvt<V, W>( graph, v ).Solve() ) { 
					probe += 1;
				}
				if ( probe != vCount ) { 
					isStrong = GraphConnectivity.Not;
					break;
				}
			}
			if ( GraphConnectivity.Not != isStrong ) { 
				isStrong = GraphConnectivity.Strong;
			}
			if ( GraphConnectivity.Strong == isStrong ) { 
				return isStrong;
			} else { 
				return ( new IsConnected<V, W>( graph.AsMinUndirected() ).Solve() ) ? GraphConnectivity.Weak : GraphConnectivity.Not;
			}
		}

	}

}