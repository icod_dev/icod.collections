namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class TopologicalVertexTraverser<V, W> : DigraphSolver<V, W, System.Collections.Generic.IEnumerable<ISet<V>>> { 

		#region .ctor
		public TopologicalVertexTraverser( IDigraph<V, W> graph ) : base( graph ) { 
		}
		#endregion .ctor


		#region methods
		public sealed override System.Collections.Generic.IEnumerable<ISet<V>> Solve() { 
			IDigraph<V, W> graph = this.Graph;

			IQueue<V> pump = Queue<V>.GetEmpty();
			foreach ( V q in new RootVertexSolver<V, W>( graph ).Solve() ) { 
				pump = pump.Enqueue( q );
			}

			V v;
			ISet<IEdge<V, W>> outBound;
			ISet<V> empty = Set<V>.GetEmpty( graph.VertexComparer );
			ISet<V> tier;
			while ( !pump.IsEmpty ) { 
				tier = empty;
				while ( !pump.IsEmpty ) { 
					v = pump.Peek();
					pump = pump.Dequeue();
					outBound = graph.GetOutBound( v );
					graph = graph.Remove( v );
					foreach ( IEdge<V, W> e in outBound ) { 
						if ( graph.GetInBound( e.B ).IsEmpty ) { 
							pump = pump.Enqueue( e.B );
						}
					}
					tier = tier.Add( v );
				}
				yield return tier;
				foreach ( V q in new RootVertexSolver<V, W>( graph ).Solve() ) { 
					pump = pump.Enqueue( q );
				}
			}
		}
		#endregion methods

	}

}