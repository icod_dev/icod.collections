namespace Icod.Collections.Immutable.GraphSolvers {

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class DirectedDijkstra<V, W> : DigraphSolver<V, W, IDigraph<V, W>> {

		#region fields
		private readonly V mySource;
		private readonly Func<W, W, W> myAdd;
		#endregion fields


		#region .ctor
		public DirectedDijkstra( IDigraph<V, W> graph, V source, Func<W, W, W> add ) : base( graph ) { 
			if ( null == add ) { 
				throw new System.ArgumentNullException( "add" );
			} else if ( !graph.Vertexes.Contains( source ) ) { 
				throw new System.ArgumentException( "The source vertex must be contained within the specified graph.", "source" );
			}
			mySource = source;
			myAdd = add;
		}
		#endregion .ctor


		#region properties
		public V Source {
			get {
				return mySource;
			}
		}
		public Func<W, W, W> Add {
			get {
				return myAdd;
			}
		}
		#endregion properties


		#region methods
		public sealed override IDigraph<V, W> Solve() {
			IDigraph<V, W> graph = this.Graph;
			System.Collections.Generic.IEqualityComparer<V> vComparer = graph.VertexComparer;
			IComparer<W> wComparer = graph.WeightComparer;

			W def = default( W );
			IHashTable<V, W> dist = HashTable<V, W>.GetEmpty( vComparer, wComparer ).Add( mySource, def );
			IHashTable<V, V> previous = HashTable<V, V>.GetEmpty( vComparer, vComparer );
			IPriorityQueue<W, V> next = PriorityQueue<W, V>.GetEmpty( wComparer ).Add( def, mySource );
			V current;
			ISet<V> empty = Set<V>.GetEmpty( vComparer );
			ISet<V> potential;
			W alt;
			do { 
				current = next.PeekMin().Value;
				next = next.DequeueMin();
				if ( !graph.Vertexes.Contains( current ) ) { 
					continue;
				}
				potential = empty;
				foreach ( IEdge<V, W> e in graph.GetOutBound( current ) ) { 
					potential = potential.Add( e.B );
				}
				foreach ( V v in potential ) { 
					alt = myAdd( dist[ current ], graph[ current, v ].Weight );
					if ( dist.ContainsKey( v ) ) { 
						if ( 0 < wComparer.Compare( dist[ v ], alt ) ) { 
							dist = dist.Remove( v ).Add( v, alt );
							previous = previous.Remove( v ).Add( v, current );
						}
					} else { 
						dist = dist.Add( v, alt );
						previous = previous.Add( v, current );
					}
					next = next.UpdatePriority( v, dist[ v ], vComparer );
					graph = graph.Remove( graph[ current, v ] );
				}
				if ( graph.GetAdjacentTo( current ).IsEmpty ) { 
					graph = graph.Remove( current );
				}
			} while ( !next.IsEmpty );

			graph = this.Graph;
			IDigraph<V, W> output = graph.Empty.Add( mySource );
			V u;
			foreach ( V v in dist.Keys ) {
				if ( previous.TryGetValue( v, out u ) ) {
					output = output.Add( graph[ u, v ] );
				}
			}

			return output;
		}
		#endregion methods

	}

}