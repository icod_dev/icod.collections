namespace Icod.Collections.Immutable.GraphSolvers {

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class DirectedFloyd<V, W> : DigraphSolver<V, W, System.Collections.Generic.IEnumerable<IPath<V, W>>> { 

		#region fields
		private readonly Func<W, W, W> myAdd;
		#endregion fields


		#region .ctor
		public DirectedFloyd( IDigraph<V, W> graph, Func<W, W, W> add ) : base( graph ) { 
			if ( null == add ) { 
				throw new System.ArgumentNullException( "add" );
			}
			myAdd = add;
		}
		#endregion .ctor


		#region methods
		public sealed override System.Collections.Generic.IEnumerable<IPath<V, W>> Solve() {
			IGraph<V, W> graph = this.Graph;

			System.Collections.Generic.IEqualityComparer<V> vComparer = graph.VertexComparer;
			IComparer<W> wComparer = graph.WeightComparer;
			if ( graph.Edges.IsEmpty || graph.Vertexes.IsEmpty ) {
				yield break;
			}

			IHashTable<V, V> emptyVv = HashTable<V, V>.GetEmpty( vComparer, vComparer );
			IHashTable<V, IHashTable<V, V>> next = HashTable<V, IHashTable<V, V>>.GetEmpty( vComparer );
			IHashTable<V, W> emptyVw = HashTable<V, W>.GetEmpty( vComparer, wComparer );
			IHashTable<V, IHashTable<V, W>> dist = HashTable<V, IHashTable<V, W>>.GetEmpty( vComparer );
			V ea;
			V eb;
			W ew;
			W def = default( W );
			foreach ( IEdge<V, W> e in graph.Edges ) { 
				ea = e.A;
				eb = e.B;
				next = next.Add( 
					ea, emptyVv.Add( eb, eb ) 
				);
				ew = e.Weight;
				dist = dist.Add( 
					ea, emptyVw.Add( eb, ew ) 
				);
			}
			IHashTable<V, V> vv;
			IHashTable<V, W> vw;
			ISet<V> verts = Set<V>.GetEmpty( vComparer ).Add( dist.Keys );
			W abc;
			W ab;
			W bc;
			W ac;
			foreach ( V b in verts ) { 
				foreach ( V a in verts ) { 
					if ( !dist[ a ].ContainsKey( b ) ) { 
						continue;
					}
					foreach ( V c in verts ) { 
						if ( !dist[ b ].ContainsKey( c ) ) { 
							continue;
						}
						if ( !dist[ a ].ContainsKey( c ) ) { 
							ab = dist[ a ][ b ];
							bc = dist[ b ][ c ];
							abc = myAdd( ab, bc );
							vw = dist[ a ].Add( c, abc );
							dist = dist.Remove( a ).Add( a, vw );
							vv = next[ a ].Add( c, b );
							next = next.Remove( a ).Add( a, vv );
						} else { 
							ac = dist[ a ][ c ];
							ab = dist[ a ][ b ];
							bc = dist[ b ][ c ];
							abc = myAdd( ab, bc );
							if ( wComparer.Compare( abc, ac ) < 0 ) { 
								vw = dist[ a ].Remove( c ).Add( c, abc );
								dist = dist.Remove( a ).Add( a, vw );
								vv = next[ a ].Remove( c ).Add( c, b );
								next = next.Remove( a ).Add( a, vv );
							}
						}
					}
				}
			}

			IPath<V, W> emptyPath = Path<V, W>.GetEmpty( myAdd, vComparer );
			IPath<V, W> path;
			IHashTable<V, V> destinations;
			V current;
			V top;
			foreach ( V s in next.Keys ) { 
				destinations = next[ s ];
				foreach ( V d in destinations.Keys ) { 
					path = emptyPath.Enqueue( s, def );
					top = s;
					while ( !vComparer.Equals( top, d ) ) { 
						current = next[ top ][ d ];
						path = path.Enqueue( current, graph[ top, current ].Weight );
						top = path.Destination;
					}
					yield return path;
				}
			}
		}
		#endregion methods

	}

}