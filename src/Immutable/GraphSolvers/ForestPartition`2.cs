namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class ForestPartition<V, W> : GraphSolver<V, W, System.Collections.Generic.IEnumerable<IGraph<V, W>>> { 

		public ForestPartition( IGraph<V, W> graph ) : base( graph ) { 
		}

		public sealed override System.Collections.Generic.IEnumerable<IGraph<V, W>> Solve() { 
			IGraph<V, W> graph = this.Graph;

			if ( graph.Vertexes.IsEmpty ) { 
				yield break;
			}
			IGraph<V, W> empty = graph.Empty;
			if ( graph.Edges.IsEmpty ) { 
				foreach ( V v in graph.Vertexes ) { 
					yield return empty.Add( v );
				}
				yield break;
			}

			IGraph<V, W> output;
			ISet<IEdge<V, W>> edges;
			while ( !graph.Vertexes.IsEmpty ) { 
				output = empty;
				foreach ( V v in new Dfvt<V, W>( graph ).Solve() ) { 
					edges = graph.GetAdjacentTo( v );
					foreach ( IEdge<V, W> e in edges ) { 
						if ( !output.ContainsEdge( e.A, e.B ) ) { 
							output = output.Add( e );
						}
					}
					output = output.Add( v );
					graph = graph.Remove( v );
				}
				yield return output;
			}
		}

	}

}