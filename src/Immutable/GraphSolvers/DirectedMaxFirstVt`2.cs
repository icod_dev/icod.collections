namespace Icod.Collections.Immutable.GraphSolvers {

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class DirectedMaxFirstVt<V, W> : DirectedVertexTraverser<V, W> {

		public DirectedMaxFirstVt( IDigraph<V, W> graph ) : base( graph ) { 
		}
		public DirectedMaxFirstVt( IDigraph<V, W> graph, V vertex ) : base( graph, vertex ) { 
		}


		public sealed override System.Collections.Generic.IEnumerable<V> Solve() { 
			IDigraph<V, W> graph = this.Graph;

			if ( graph.Vertexes.IsEmpty ) { 
				yield break;
			} else if ( graph.Edges.IsEmpty ) { 
				if ( this.IsVertexSet ) { 
					yield return this.Vertex;
				} else { 
					foreach ( V v in graph.Vertexes ) { 
						yield return v;
						break;
					}
				}
				yield break;
			}

			System.Collections.Generic.IEqualityComparer<V> vComparer = graph.VertexComparer;
			IComparer<W> wComparer = graph.WeightComparer;
			IPriorityQueue<W, V> next = PriorityQueue<W, V>.GetEmpty( wComparer );
			W def = default( W );
			if ( this.IsVertexSet ) { 
				next = next.Add( def, this.Vertex );
			} else { 
				foreach ( V v in graph.Vertexes ) { 
					next = next.Add( def, v );
					break;
				}
			}
			System.Collections.Generic.KeyValuePair<W, V> current;
			V u;
			W w;
			while ( !next.IsEmpty ) { 
				current = next.PeekMax();
				next = next.DequeueMax();
				u = current.Value;
				if ( !graph.Vertexes.Contains( u ) ) { 
					continue;
				}
				w = current.Key;
				foreach ( IEdge<V, W> e in graph.GetOutBound( u ) ) { 
					next = next.Add( e.Weight, e.B );
					graph = graph.Remove( e );
				}
				graph = graph.Remove( u );
				yield return u;
			}
		}

	}

}