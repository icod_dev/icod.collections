namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class DijkstraSearch<V, W> : GraphSolver<V, W, IPath<V, W>> { 

		#region fields
		private readonly V mySource;
		private readonly Func<W, W, W> myAdd;
		private readonly V myDestination;
		#endregion fields


		#region .ctor
		public DijkstraSearch( IGraph<V, W> graph, V source, Func<W, W, W> add, V destination ) : base( graph ) { 
			if ( null == add ) { 
				throw new System.ArgumentNullException( "add" );
			} else if ( !graph.Vertexes.Contains( source ) ) { 
				throw new System.ArgumentException( "The source vertex must be contained within the specified graph.", "source" );
			} else if ( !graph.Vertexes.Contains( destination ) ) { 
				throw new System.ArgumentException( "The destination vertex must be contained within the specified graph.", "source" );
			}
			mySource = source;
			myAdd = add;
			myDestination = destination;
		}
		#endregion .ctor


		#region properties
		public V Source { 
			get { 
				return mySource;
			}
		}
		public Func<W, W, W> Add { 
			get { 
				return myAdd;
			}
		}
		public V Destination { 
			get { 
				return myDestination;
			}
		}
		#endregion properties


		#region methods
		public sealed override IPath<V, W> Solve() { 
			IGraph<V, W> graph = this.Graph;
			if ( graph is IDigraph<V, W> ) { 
				return new DirectedDijkstraSearch<V, W>( graph as IDigraph<V, W>, mySource, myAdd, myDestination ).Solve();
			}

			System.Collections.Generic.IEqualityComparer<V> vComparer = graph.VertexComparer;
			if ( vComparer.Equals( mySource, myDestination ) ) { 
				return Path<V, W>.Create( myAdd, vComparer, mySource );
			}

			IComparer<W> wComparer = graph.WeightComparer;
			W def = default( W );
			IHashTable<V, W> dist = HashTable<V, W>.GetEmpty( vComparer, wComparer ).Add( mySource, def );
			IHashTable<V, V> previous = HashTable<V, V>.GetEmpty( vComparer, vComparer );
			IPriorityQueue<W, V> next = PriorityQueue<W, V>.GetEmpty( wComparer ).Add( def, mySource );
			V current;
			ISet<V> empty = Set<V>.GetEmpty( vComparer );
			ISet<V> potential;
			W alt;
			do { 
				current = next.PeekMin().Value;
				next = next.DequeueMin();
				if ( !graph.Vertexes.Contains( current ) ) { 
					continue;
				}
				potential = empty;
				foreach ( IEdge<V, W> e in graph.GetAdjacentTo( current ) ) { 
					potential = potential.Union( e.Vertexes );
				}
				foreach ( V v in potential.Remove( current ) ) { 
					alt = myAdd( dist[ current ], graph[ current, v ].Weight );
					if ( dist.ContainsKey( v ) ) { 
						if ( 0 < wComparer.Compare( dist[ v ], alt ) ) { 
							dist = dist.Remove( v ).Add( v, alt );
							previous = previous.Remove( v ).Add( v, current );
						}
					} else { 
						dist = dist.Add( v, alt );
						previous = previous.Add( v, current );
					}
					next = next.UpdatePriority( v, dist[ v ], vComparer );
				}
				graph = graph.Remove( current );
			} while ( !next.IsEmpty && !vComparer.Equals( current, myDestination ) );

			if ( !previous.ContainsKey( myDestination ) ) { 
				return Path<V, W>.GetEmpty( myAdd, vComparer );
			}
			graph = this.Graph;
			current = myDestination;
			IStack<IEdge<V, W>> rev = Stack<IEdge<V, W>>.GetEmpty();
			V u;
			do { 
				u = previous[ current ];
				rev = rev.Push( graph[ u, current ] );
				current = u;
			} while ( !vComparer.Equals( mySource, current ) );
			IPath<V, W> path = Path<V, W>.Create( myAdd, vComparer, mySource );
			IEdge<V, W> step;
			while ( rev.IsEmpty ) { 
				step = rev.Peek();
				rev = rev.Pop();
				path = path.Enqueue( step.B, step.Weight );
			}
			return path;
		}
		#endregion methods

	}

}