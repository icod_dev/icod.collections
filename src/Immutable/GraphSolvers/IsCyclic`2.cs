namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class IsCyclic<V, W> : DigraphSolver<V, W, System.Boolean> { 

		public IsCyclic( IDigraph<V, W> graph ) : base( graph ) { 
		}

		public sealed override System.Boolean Solve() { 
			IDigraph<V, W> graph = this.Graph;

			System.Int32 vCount = graph.Vertexes.Count;
			System.Int32 probe = 0;
			foreach ( ISet<V> set in new TopologicalVertexTraverser<V, W>( graph ).Solve() ) { 
				probe += set.Count;
			}
			return ( vCount != probe );
		}

	}

}