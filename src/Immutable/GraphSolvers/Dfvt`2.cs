namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class Dfvt<V, W> : VertexTraverser<V, W> { 

		public Dfvt( IGraph<V, W> graph ) : base( graph ) { 
		}
		public Dfvt( IGraph<V, W> graph, V vertex ) : base( graph, vertex ) { 
		}


		public sealed override System.Collections.Generic.IEnumerable<V> Solve() { 
			IGraph<V, W> graph = this.Graph;

			if ( graph.Vertexes.IsEmpty ) { 
				yield break;
			} else if ( graph.Edges.IsEmpty ) { 
				if ( this.IsVertexSet ) { 
					yield return this.Vertex;
				} else { 
					foreach ( V v in graph.Vertexes ) { 
						yield return v;
						break;
					}
				}
				yield break;
			}

			ISet<V> verts = graph.Vertexes;
			IStack<V> pump = Stack<V>.GetEmpty();
			if ( this.IsVertexSet ) { 
				pump = pump.Push( this.Vertex );
			} else { 
				foreach ( V v in graph.Vertexes ) {
					pump = pump.Push( v );
					break;
				}
			}
			V vertex;
			while ( !pump.IsEmpty ) { 
				vertex = pump.Peek();
				pump = pump.Pop();
				if ( !verts.Contains( vertex ) ) { 
					continue;
				}
				verts = verts.Remove( vertex );
				foreach ( IEdge<V, W> e in graph.GetAdjacentTo( vertex ) ) { 
					if ( verts.Contains( e.A ) ) { 
						pump = pump.Push( e.A );
					}
					if ( verts.Contains( e.B ) ) { 
						pump = pump.Push( e.B );
					}
				}
				yield return vertex;
			}
		}

	}

}