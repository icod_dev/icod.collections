namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class RootVertexSolver<V, W> : DirectedVertexTraverser<V, W> { 

		public RootVertexSolver( IDigraph<V, W> graph ) : base( graph ) { 
		}

		public sealed override System.Collections.Generic.IEnumerable<V> Solve() { 
			IDigraph<V, W> graph = this.Graph;

			foreach ( V v in graph.Vertexes ) { 
				if ( !graph.GetInBound( v ).IsEmpty ) { 
					continue;
				}
				yield return v;
			}

		}

	}

}