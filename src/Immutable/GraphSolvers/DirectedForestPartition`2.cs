namespace Icod.Collections.Immutable.GraphSolvers { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class DirectedForestPartition<V, W> : DigraphSolver<V, W, System.Collections.Generic.IEnumerable<IDigraph<V, W>>> { 

		public DirectedForestPartition( IDigraph<V, W> graph ) : base( graph ) { 
		}

		public sealed override System.Collections.Generic.IEnumerable<IDigraph<V, W>> Solve() { 
			IDigraph<V, W> graph = this.Graph;

			if ( graph.Vertexes.IsEmpty ) { 
				yield break;
			}
			IDigraph<V, W> empty = graph.Empty;
			if ( graph.Edges.IsEmpty ) { 
				foreach ( V v in graph.Vertexes ) { 
					yield return empty.Add( v );
				}
				yield break;
			}

			IDigraph<V, W> output;
			ISet<IEdge<V, W>> edges;
			while ( !graph.Vertexes.IsEmpty ) { 
				output = empty;
				foreach ( V v in new Dfvt<V, W>( graph ).Solve() ) {
					edges = graph.GetAdjacentTo( v );
					foreach ( IEdge<V, W> e in edges ) { 
						if ( !output.Edges.Contains( e ) ) { 
							output = output.Add( e );
						}
					}
					output = output.Add( v );
					graph = graph.Remove( v );
				}
				yield return output;
			}
		}

	}

}