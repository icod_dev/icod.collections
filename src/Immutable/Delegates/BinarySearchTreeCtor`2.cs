namespace Icod.Collections.Immutable { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public delegate IBinarySearchTree<K, V> BinarySearchTreeCtor<K, V>( IBinarySearchTree<K, V> left, IBinarySearchTree<K, V> root, IBinarySearchTree<K, V> right );

}