namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class Digraph<V, W> : GraphBase<V, W>, IDigraph<V, W> { 

		#region internal classes
		[System.Serializable]
		private sealed class EmptyDigraph : GraphBase<V, W>.EmptyGraph, IDigraph<V, W> {
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary<
				IComparer<W>,
				System.Collections.Generic.IDictionary<
					System.Collections.Generic.IEqualityComparer<V>,
					IDigraph<V, W>
				>
			> theEmpty;
			private static readonly ISet<IDigraph<V, W>> theForest;
			private readonly IGraph<V, W> myUndirected;
			private readonly ISet<IEdge<V, W>> myEdges;
			private readonly IHashTable<V, ISet<IEdge<V, W>>> myOutBound;
			private readonly IHashTable<V, ISet<IEdge<V, W>>> myInBound;
			private readonly System.Int32 myHashCode;

			static EmptyDigraph() {
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked {
					theHashCode += typeof( V ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( W ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary<
					IComparer<W>,
					System.Collections.Generic.IDictionary<
						System.Collections.Generic.IEqualityComparer<V>,
						IDigraph<V, W>
					>
				>();
				theForest = Set<IDigraph<V, W>>.GetEmpty();
			}
			private EmptyDigraph( IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) : base( weightComparer, vertexComparer ) {
				myHashCode = theHashCode;
				unchecked {
					myHashCode += weightComparer.GetHashCode();
					myHashCode += vertexComparer.GetHashCode();
				}
				myEdges = Set<IEdge<V, W>>.GetEmpty( this );
				myOutBound = HashTable<V, ISet<IEdge<V, W>>>.GetEmpty( this.VertexComparer );
				myInBound = myOutBound;
				myUndirected = Graph<V, W>.GetEmpty( this.WeightComparer, this.VertexComparer );
			}

			IGraph<V, W> IGraph<V, W>.Empty { 
				get { 
					return this;
				}
			}
			public IDigraph<V, W> Empty { 
				get { 
					return this;
				}
			}
			public System.Boolean IsCyclic { 
				get { 
					return false;
				}
			}
			public System.Boolean IsStronglyConnected { 
				get { 
					return false;
				}
			}
			public sealed override ISet<IEdge<V, W>> Edges { 
				get { 
					return myEdges;
				}
			}

			IGraph<V, W> IGraph<V, W>.Add( V vertex ) { 
				return this.Add( vertex );
			}
			public IDigraph<V, W> Add( V vertex ) { 
				return new Digraph<V, W>( 
					this.Vertexes.Add( vertex ), 
					myEdges, 
					myOutBound, 
					myInBound, 
					this.WeightComparer, 
					this.VertexComparer, 
					GraphConnectivity.Not, 
					GraphCyclicity.False 
				);
			}
			IGraph<V, W> IGraph<V, W>.Add( IEdge<V, W> edge ) { 
				return this.Add( edge );
			}
			public IDigraph<V, W> Add( IEdge<V, W> edge ) { 
				if ( null == edge ) { 
					throw new System.ArgumentNullException( "edge" );
				}
				V a = edge.A;
				V b = edge.B;
				System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
				System.Boolean isCyclic = comparer.Equals( a, b );
				GraphConnectivity connectivity = ( isCyclic ? GraphConnectivity.Strong : GraphConnectivity.Weak );
				GraphCyclicity cyclicity = ( isCyclic ? GraphCyclicity.True : GraphCyclicity.False );
				ISet<IEdge<V, W>> edges = this.Edges.Add( edge );
				return new Digraph<V,W>( 
					this.Vertexes.Add( a ).Add( b ), 
					edges, 
					myOutBound.Add( a, edges ), 
					myInBound.Add( b, edges ), 
					this.WeightComparer, 
					comparer, 
					connectivity, 
					cyclicity 
				);
			}
			IGraph<V, W> IGraph<V, W>.Connect( V a, V b, W weight ) { 
				return this.Connect( a, b, weight );
			}
			public IDigraph<V, W> Connect( V a, V b, W weight ) { 
				System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
				IEdge<V, W> edge = new Edge<V, W>( a, b, weight, comparer );
				System.Boolean isReflexive = comparer.Equals( a, b );
				ISet<IEdge<V, W>> edges = myEdges;
				return new Digraph<V, W>( 
					this.Vertexes.Add( a ).Add( b ), 
					edges, 
					myOutBound.Add( a, edges ), 
					myInBound.Add( b, edges ), 
					this.WeightComparer, 
					comparer, 
					isReflexive ? GraphConnectivity.Strong : GraphConnectivity.Weak, 
					isReflexive ? GraphCyclicity.True : GraphCyclicity.False 
				);
			}
			IGraph<V, W> IGraph<V, W>.Remove( V vertex ) { 
				return this.Remove( vertex );
			}
			public IDigraph<V, W> Remove( V vertex ) { 
				return this;
			}
			IGraph<V, W> IGraph<V, W>.Remove( IEdge<V, W> edge ) { 
				return this.Remove( edge );
			}
			public IDigraph<V, W> Remove( IEdge<V, W> edge ) { 
				if ( null == edge ) { 
					throw new System.ArgumentNullException( "edge" );
				}
				return this;
			}
			IGraph<V, W> IGraph<V, W>.Update( IEdge<V, W> x, IEdge<V, W> y ) { 
				return this.Update( x, y );
			}
			public IDigraph<V, W> Update( IEdge<V, W> x, IEdge<V, W> y ) { 
				if ( null == y ) { 
					throw new System.ArgumentNullException( "y" );
				} else if ( null == x ) { 
					throw new System.ArgumentNullException( "x" );
				}
				System.Collections.Generic.IEqualityComparer<V> vComparer = this.VertexComparer;
				if ( !vComparer.Equals( x.A, y.A ) || !vComparer.Equals( x.B, y.B ) ) { 
					throw new System.ArgumentOutOfRangeException( "y" );
				}
				throw new System.Collections.Generic.KeyNotFoundException();
			}
			IGraph<V, W> IGraph<V, W>.Update( IEdge<V, W> edge, W weight ) { 
				return this.Update( edge, weight );
			}
			public IDigraph<V, W> Update( IEdge<V, W> edge, W weight ) { 
				if ( null == edge ) { 
					throw new System.ArgumentNullException( "edge" );
				}
				throw new System.Collections.Generic.KeyNotFoundException();
			}
			public IGraph<V, W> AsMinUndirected() { 
				return myUndirected;
			}
			public IGraph<V, W> AsMaxUndirected() { 
				return this.AsMinUndirected();
			}
			public IGraph<V, System.Boolean> AsUndirected() { 
				return Graph<V, System.Boolean>.GetEmpty( Comparer<System.Boolean>.Default, this.VertexComparer );
			}
			ISet<IGraph<V, W>> IGraph<V, W>.AsForest() { 
				return (ISet<IGraph<V, W>>)this.AsForest();
			}
			public ISet<IDigraph<V, W>> AsForest() { 
				return theForest;
			}
			public ISet<IEdge<V, W>> GetOutBound( V vertex ) { 
				return this.Edges;
			}
			public ISet<IEdge<V, W>> GetInBound( V vertex ) { 
				return this.Edges;
			}

			public sealed override System.Boolean Equals( IEdge<V, W> x, IEdge<V, W> y ) { 
				if ( ( null == (System.Object)x ) && ( null == (System.Object)y ) ) { 
					return true;
				} else if ( ( null == (System.Object)x ) || ( null == (System.Object)y ) ) { 
					return false;
				} else if ( System.Object.ReferenceEquals( x, y ) ) { 
					return true;
				} else { 
					System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
					return ( 
						comparer.Equals( x.A, y.A ) 
						&& comparer.Equals( x.B, y.B ) 
						&& this.WeightComparer.Equals( x.Weight, y.Weight ) 
					);
				}
			}

			public static IDigraph<V, W> Get() { 
				return Get( Comparer<W>.Default, Comparer<V>.Default );
			}
			public static IDigraph<V, W> Get( IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) { 
				if ( null == vertexComparer ) { 
					throw new System.ArgumentNullException( "vertexComparer" );
				} else if ( null == weightComparer ) { 
					throw new System.ArgumentNullException( "weightComparer" );
				}
				IDigraph<V, W> output = null;
				System.Collections.Generic.IDictionary<System.Collections.Generic.IEqualityComparer<V>, IDigraph<V, W>> branch = null;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( weightComparer ) ) { 
						branch = theEmpty[ weightComparer ];
					} else {
						branch = new System.Collections.Generic.Dictionary<System.Collections.Generic.IEqualityComparer<V>, IDigraph<V, W>>();
						theEmpty.Add( weightComparer, branch );
					}
				}
				lock ( branch ) { 
					if ( branch.ContainsKey( vertexComparer ) ) { 
						output = branch[ vertexComparer ];
					} else { 
						output = new EmptyDigraph( weightComparer, vertexComparer );
						branch.Add( vertexComparer, output );
					}
				}
				return output;
			}

			public sealed override System.Int32 GetHashCode() { 
				return myHashCode;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;

		private IDigraph<V, W> myEmpty;
		private System.Int32 myConnectivity;
		private System.Int32 myCyclicity;
		private readonly IHashTable<V, ISet<IEdge<V, W>>> myInBound;
		private ISet<IDigraph<V, W>> myForest;
		private IGraph<V, W> myMinUndirected;
		private IGraph<V, W> myMaxUndirected;
		private IGraph<V, System.Boolean> myUndirected;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static Digraph() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( V ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( W ).AssemblyQualifiedName.GetHashCode();
			}
		}

		private Digraph( ISet<V> vertexes, ISet<IEdge<V, W>> edges, IHashTable<V, ISet<IEdge<V, W>>> outBound, IHashTable<V, ISet<IEdge<V, W>>> inBound, IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) : this( vertexes, edges, outBound, inBound, weightComparer, vertexComparer, GraphConnectivity.Untested, GraphCyclicity.Untested ) { 
		}
		private Digraph( ISet<V> vertexes, ISet<IEdge<V, W>> edges, IHashTable<V, ISet<IEdge<V, W>>> outBound, IHashTable<V, ISet<IEdge<V, W>>> inBound, IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer, GraphConnectivity connectivity, GraphCyclicity cyclicity ) : base( vertexes, edges, outBound, weightComparer, vertexComparer ) { 
			if ( !System.Enum.IsDefined( typeof( GraphCyclicity ), cyclicity ) ) { 
				throw new System.ArgumentOutOfRangeException( "cyclicity" );
			} else if ( !System.Enum.IsDefined( typeof( GraphConnectivity ), connectivity ) ) { 
				throw new System.ArgumentOutOfRangeException( "connectivity" );
			} else if ( null == inBound ) { 
				throw new System.ArgumentNullException( "inBound" );
			}
			myInBound = inBound;

			myHashCode = theHashCode;
			unchecked { 
				myHashCode += vertexes.GetHashCode();
				myHashCode += vertexes.Count;
				myHashCode += edges.GetHashCode();
				myHashCode += edges.Count;
			}
			myConnectivity = (System.Int32)connectivity;
			myCyclicity = (System.Int32)cyclicity;
			myEmpty = null;
			myForest = null;
			myMinUndirected = null;
			myMaxUndirected = null;
			myUndirected = null;
		}
		#endregion .ctor


		#region properties
		IGraph<V, W> IGraph<V, W>.Empty { 
			get { 
				return this.Empty;
			}
		}
		public IDigraph<V, W> Empty { 
			get { 
				if ( null == myEmpty ) { 
					System.Threading.Interlocked.CompareExchange<IDigraph<V, W>>( 
						ref myEmpty, 
						GetEmpty( this.WeightComparer, this.VertexComparer ), 
						null 
					);
				}
				return myEmpty;
			}
		}
		public IEdge<V, W> this[ V a, V b ] { 
			get { 
				IEdge<V, W> edge;
				if ( this.TryGetEdge( a, b, out edge ) ) { 
					return edge;
				} else { 
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}
		}
		public sealed override System.Boolean IsConnected { 
			get { 
				if ( (System.Int32)GraphConnectivity.Untested == myConnectivity ) { 
					System.Threading.Interlocked.CompareExchange( 
						ref myConnectivity, 
						(System.Int32)( new GraphSolvers.DigraphConnectivity<V, W>( this ).Solve() ), 
						(System.Int32)GraphConnectivity.Untested 
					);
				}
				return ( (System.Int32)GraphConnectivity.Weak <= myConnectivity );
			}
		}
		public System.Boolean IsStronglyConnected { 
			get { 
				if ( (System.Int32)GraphConnectivity.Untested == myConnectivity ) { 
					System.Threading.Interlocked.CompareExchange( 
						ref myConnectivity, 
						(System.Int32)( new GraphSolvers.DigraphConnectivity<V, W>( this ).Solve() ), 
						(System.Int32)GraphConnectivity.Untested 
					);
				}
				return ( (System.Int32)GraphConnectivity.Strong == myConnectivity );
			}
		}
		public System.Boolean IsCyclic { 
			get { 
				if ( (System.Int32)GraphCyclicity.Untested == myCyclicity ) { 
					System.Int32 cyclic = (System.Int32)( new GraphSolvers.IsCyclic<V, W>( this ).Solve() ? GraphCyclicity.True : GraphCyclicity.False );
					System.Threading.Interlocked.CompareExchange( 
						ref myCyclicity, 
						cyclic, 
						(System.Int32)GraphCyclicity.Untested 
					);
				}
				return ( (System.Int32)GraphCyclicity.True == myCyclicity );
			}
		}
		private IHashTable<V, ISet<IEdge<V, W>>> InBound { 
			get { 
				return myInBound;
			}
		}
		#endregion properties


		#region methods
		public sealed override System.Boolean TryGetEdge( V a, V b, out IEdge<V, W> edge ) { 
			edge = null;
			ISet<V> verts = this.Vertexes;
			if ( !verts.Contains( a ) || !verts.Contains( b ) ) { 
				return false;
			} else if ( !this.OutBound.ContainsKey( a ) || !myInBound.ContainsKey( b ) ) { 
				return false;
			}

			System.Collections.Generic.IEqualityComparer<V> comparer = this.VertexComparer;
			foreach ( IEdge<V, W> e in this.OutBound[ a ].Intersection( myInBound[ b ] ) ) { 
				if ( comparer.Equals( e.A, a ) && comparer.Equals( e.B, b ) ) { 
					edge = e;
					return true;
				}
			}
			return false;
		}
		public sealed override System.Boolean ContainsEdge( V a, V b ) { 
			IEdge<V, W> edge;
			return this.TryGetEdge( a, b, out edge );
		}
		public sealed override System.Boolean IsSymmetric( V a, V b ) { 
			if ( this.VertexComparer.Equals( a, b ) ) { 
				return this.ContainsEdge( a, b );
			} else { 
				return ( this.ContainsEdge( a, b ) && this.ContainsEdge( b, a ) );
			}
		}
		public sealed override System.Boolean IsSymmetric( IEdge<V, W> edge ) { 
			if ( null == edge ) { 
				throw new System.ArgumentNullException( "edge" );
			}
			return this.IsSymmetric( edge.A, edge.B );
		}

		IGraph<V, W> IGraph<V, W>.Add( V vertex ) { 
			return this.Add( vertex );
		}
		public IDigraph<V, W> Add( V vertex ) { 
			if ( this.Vertexes.Contains( vertex ) ) { 
				return this;
			}
			return new Digraph<V, W>( 
				this.Vertexes.Add( vertex ), 
				this.Edges, 
				this.OutBound, 
				myInBound, 
				this.WeightComparer, 
				this.VertexComparer, 
				GraphConnectivity.Not, 
				(GraphCyclicity)myCyclicity 
			);
		}
		IGraph<V, W> IGraph<V, W>.Add( IEdge<V, W> edge ) { 
			return this.Add( edge );
		}
		public IDigraph<V, W> Add( IEdge<V, W> edge ) { 
			if ( null == edge ) { 
				throw new System.ArgumentNullException( "edge" );
			}
			ISet<IEdge<V, W>> edges = this.Edges;
			if ( edges.Contains( edge ) ) { 
				return this;
			}
			V a = edge.A;
			V b = edge.B;
			if ( this.ContainsEdge( a, b ) ) { 
				throw new DuplicateKeyException();
			}
			edges = edges.Add( edge );
			ISet<IEdge<V, W>> probe;
			IHashTable<V, ISet<IEdge<V, W>>> outBound = this.OutBound;
			if ( outBound.TryGetValue( a, out probe ) ) { 
				outBound = outBound.Remove( a );
			} else { 
				probe = Set<IEdge<V, W>>.GetEmpty( this.Empty );
			}
			outBound = outBound.Add( a, probe.Add( edge ) );
			IHashTable<V, ISet<IEdge<V, W>>> inBound = myInBound;
			if ( inBound.TryGetValue( b, out probe ) ) { 
				inBound = outBound.Remove( b );
			} else { 
				probe = Set<IEdge<V, W>>.GetEmpty( this.Empty );
			}
			inBound = inBound.Add( b, probe.Add( edge ) );
			return new Digraph<V, W>( 
				this.Vertexes.Add( a ).Add( b ), 
				edges, 
				outBound, 
				inBound, 
				this.WeightComparer, 
				this.VertexComparer, 
				GraphConnectivity.Untested, 
				GraphCyclicity.Untested 
			);
		}
		IGraph<V, W> IGraph<V, W>.Connect( V a, V b, W weight ) { 
			return this.Connect( a, b, weight );
		}
		public IDigraph<V, W> Connect( V a, V b, W weight ) { 
			IEdge<V, W> edge;
			if ( this.TryGetEdge( a, b, out edge ) ) { 
				throw new DuplicateKeyException();
			}
			return this.Add( new Edge<V, W>( a, b, weight, this.VertexComparer ) );
		}
		IGraph<V, W> IGraph<V, W>.Remove( V vertex ) { 
			return this.Remove( vertex );
		}
		public IDigraph<V, W> Remove( V vertex ) { 
			ISet<V> verts = this.Vertexes;
			if ( !verts.Contains( vertex ) ) { 
				return this;
			}
			verts = verts.Remove( vertex );
			if ( verts.IsEmpty ) { 
				return this.Empty;
			}
			ISet<IEdge<V, W>> edges = this.Edges;
			ISet<IEdge<V, W>> probe;
			IHashTable<V, ISet<IEdge<V, W>>> outBound = this.OutBound;
			IHashTable<V, ISet<IEdge<V, W>>> inBound = myInBound;
			if ( outBound.TryGetValue( vertex, out probe ) ) { 
				foreach ( IEdge<V, W> e in probe ) { 
					edges = edges.Remove( e );
				}
				outBound = outBound.Remove( vertex );
			}
			if ( inBound.TryGetValue( vertex, out probe ) ) { 
				foreach ( IEdge<V, W> e in probe ) { 
					edges = edges.Remove( e );
				}
				inBound = inBound.Remove( vertex );
			}
			return new Digraph<V, W>( verts, edges, outBound, inBound, this.WeightComparer, this.VertexComparer, GraphConnectivity.Untested, GraphCyclicity.Untested );
		}
		IGraph<V, W> IGraph<V, W>.Remove( IEdge<V, W> edge ) { 
			return this.Remove( edge );
		}
		public IDigraph<V, W> Remove( IEdge<V, W> edge ) { 
			if ( null == edge ) { 
				throw new System.ArgumentNullException( "edge" );
			}
			ISet<IEdge<V, W>> edges = this.Edges;
			if ( !edges.Contains( edge ) ) { 
				return this;
			}
			edges = edges.Remove( edge );
			ISet<IEdge<V, W>> probe;
			IHashTable<V, ISet<IEdge<V, W>>> outBound = this.OutBound;
			if ( outBound.TryGetValue( edge.A, out probe ) ) { 
				if ( probe.Contains( edge ) ) { 
					probe = probe.Remove( edge );
					outBound = outBound.Remove( edge.A );
					if ( !probe.IsEmpty ) { 
						outBound = outBound.Add( edge.A, probe );
					}
				}
			}
			IHashTable<V, ISet<IEdge<V, W>>> inBound = myInBound;
			if ( inBound.TryGetValue( edge.B, out probe ) ) { 
				if ( probe.Contains( edge ) ) { 
					inBound = inBound.Remove( edge.B );
					probe = probe.Remove( edge );
					if ( !probe.IsEmpty ) { 
						inBound = inBound.Add( edge.B, probe );
					}
				}
			}
			return new Digraph<V, W>( 
				this.Vertexes, 
				edges, 
				outBound, 
				inBound, 
				this.WeightComparer, 
				this.VertexComparer, 
				GraphConnectivity.Untested, 
				GraphCyclicity.Untested 
			);
		}
		IGraph<V, W> IGraph<V, W>.Update( IEdge<V, W> x, IEdge<V, W> y ) { 
			return this.Update( x, y );
		}
		public IDigraph<V, W> Update( IEdge<V, W> x, IEdge<V, W> y ) { 
			if ( null == y ) { 
				throw new System.ArgumentNullException( "y" );
			} else if ( null == x ) { 
				throw new System.ArgumentNullException( "x" );
			}
			System.Collections.Generic.IEqualityComparer<V> vComparer = this.VertexComparer;
			V a = x.A;
			V b = x.B;
			if ( !vComparer.Equals( a, y.A ) || !vComparer.Equals( b, y.B ) ) { 
				throw new System.ArgumentOutOfRangeException( "y" );
			}
			ISet<IEdge<V, W>> edges = this.Edges;
			if ( !edges.Contains( x ) ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			}
			IComparer<W> wComparer = this.WeightComparer;
			if ( wComparer.Equals( x.Weight, y.Weight ) ) { 
				return this;
			}

			edges = edges.Remove( x ).Add( y );
			ISet<IEdge<V, W>> probe;
			IHashTable<V, ISet<IEdge<V, W>>> outBound = this.OutBound;
			if ( outBound.TryGetValue( a, out probe ) ) { 
				outBound = outBound.Remove( a ).Add( a, probe.Remove( x ).Add( y ) );
			}
			IHashTable<V, ISet<IEdge<V, W>>> inBound = myInBound;
			if ( inBound.TryGetValue( b, out probe ) ) { 
				inBound = inBound.Remove( b ).Add( b, probe.Remove( x ).Add( y ) );
			}

			return new Digraph<V, W>( 
				this.Vertexes, 
				edges, 
				outBound, 
				inBound, 
				wComparer, 
				vComparer, 
				(GraphConnectivity)myConnectivity, 
				(GraphCyclicity)myCyclicity 
			);
		}
		IGraph<V, W> IGraph<V, W>.Update( IEdge<V, W> edge, W weight ) { 
			return this.Update( edge, weight );
		}
		public IDigraph<V, W> Update( IEdge<V, W> x, W weight ) { 
			if ( null == x ) { 
				throw new System.ArgumentNullException( "edge" );
			}
			return this.Update( x, new Edge<V, W>( x.A, x.B, weight, this.VertexComparer ) );
		}

		public ISet<IEdge<V, W>> GetAdjacentTo( V vertex ) { 
			return this.GetOutBound( vertex ).Union( this.GetInBound( vertex ) );
		}
		ISet<IGraph<V, W>> IGraph<V, W>.AsForest() { 
			return (ISet<IGraph<V, W>>)this.AsForest();
		}
		public ISet<IDigraph<V, W>> AsForest() { 
			if ( null == myForest ) { 
				System.Threading.Interlocked.CompareExchange<ISet<IDigraph<V, W>>>( 
					ref myForest, 
					this.GetAsForest(), 
					null 
				);
			}
			return myForest;
		}
		public IGraph<V, W> AsMinUndirected() { 
			if ( null == myMinUndirected ) { 
				System.Threading.Interlocked.CompareExchange<IGraph<V, W>>( 
					ref myMinUndirected, 
					this.GetMinUndirected(), 
					null 
				);
			}
			return myMinUndirected;
		}
		public IGraph<V, W> AsMaxUndirected() { 
			if ( null == myMaxUndirected ) { 
				System.Threading.Interlocked.CompareExchange<IGraph<V, W>>( 
					ref myMaxUndirected, 
					this.GetMaxUndirected(), 
					null 
				);
			}
			return myMinUndirected;
		}
		public IGraph<V, System.Boolean> AsUndirected() { 
			if ( null == myUndirected ) { 
				System.Threading.Interlocked.CompareExchange<IGraph<V, System.Boolean>>( 
					ref myUndirected, 
					this.GetAsUndirected(), 
					null 
				);
			}
			return myUndirected;
		}
		public ISet<IEdge<V, W>> GetInBound( V vertex ) {
			ISet<IEdge<V, W>> output;
			if ( myInBound.TryGetValue( vertex, out output ) ) {
				return output;
			} else {
				return Set<IEdge<V, W>>.GetEmpty( this.Empty );
			}
		}
		public ISet<IEdge<V, W>> GetOutBound( V vertex ) {
			ISet<IEdge<V, W>> output;
			if ( this.OutBound.TryGetValue( vertex, out output ) ) {
				return output;
			} else {
				return Set<IEdge<V, W>>.GetEmpty( this.Empty );
			}
		}

		public System.Int32 GetHashCode( IEdge<V, W> obj ) { 
			return this.Empty.GetHashCode( obj );
		}
		public System.Boolean Equals( IEdge<V, W> x, IEdge<V, W> y ) { 
			return this.Empty.Equals( x, y );
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}


		private IGraph<V, W> GetMinUndirected() { 
			System.Collections.Generic.IEqualityComparer<V> vComparer = this.VertexComparer;
			IComparer<W> wComparer = this.WeightComparer;
			IGraph<V, W> output = Graph<V, W>.GetEmpty( wComparer, vComparer );
			V a;
			V b;
			IEdge<V, W> probe;
			foreach ( IEdge<V, W> e in this.Edges ) {
				a = e.A;
				b = e.B;
				if ( !vComparer.Equals( a, b ) ) {
					if ( !output.ContainsEdge( a, b ) ) {
						output = output.Add( e );
					} else {
						probe = output[ a, b ];
						if ( 0 < wComparer.Compare( probe.Weight, e.Weight ) ) {
							output = output.Update( probe, e );
						}
					}
				}
			}
			return output;
		}
		private IGraph<V, System.Boolean> GetAsUndirected() { 
			System.Collections.Generic.IEqualityComparer<V> vComparer = this.VertexComparer;
			IGraph<V, System.Boolean> output = Graph<V, System.Boolean>.GetEmpty( Comparer<System.Boolean>.Default, vComparer );
			V a;
			V b;
			foreach ( IEdge<V, W> e in this.Edges ) { 
				a = e.A;
				b = e.B;
				if ( !vComparer.Equals( a, b ) ) { 
					if ( !output.ContainsEdge( a, b ) ) { 
						output = output.Connect( a, b, true );
					}
				}
			}
			return output;
		}
		private IGraph<V, W> GetMaxUndirected() {
			System.Collections.Generic.IEqualityComparer<V> vComparer = this.VertexComparer;
			IComparer<W> wComparer = this.WeightComparer;
			IGraph<V, W> output = Graph<V, W>.GetEmpty( wComparer, vComparer );
			V a;
			V b;
			IEdge<V, W> probe;
			foreach ( IEdge<V, W> e in this.Edges ) {
				a = e.A;
				b = e.B;
				if ( !vComparer.Equals( a, b ) ) {
					if ( !output.ContainsEdge( a, b ) ) {
						output = output.Add( e );
					} else {
						probe = output[ a, b ];
						if ( wComparer.Compare( probe.Weight, e.Weight ) < 0 ) {
							output = output.Update( probe, e );
						}
					}
				}
			}
			return output;
		}
		private ISet<IDigraph<V, W>> GetAsForest() { 
			return Set<IDigraph<V, W>>.GetEmpty().Add( new GraphSolvers.DirectedForestPartition<V, W>( this ).Solve() );
		}
		#endregion methods


		#region static methods
		public static IDigraph<V, W> GetEmpty() { 
			return EmptyDigraph.Get();
		}
		public static IDigraph<V, W> GetEmpty( IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) { 
			if ( null == vertexComparer ) { 
				throw new System.ArgumentNullException( "vertexComparer" );
			} else if ( null == weightComparer ) { 
				throw new System.ArgumentNullException( "weightComparer" );
			}

			return EmptyDigraph.Get( weightComparer, vertexComparer );
		}
		#endregion static methods

	}

}