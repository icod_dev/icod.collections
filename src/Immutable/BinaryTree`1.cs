namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class BinaryTree<T> : IBinaryTree<T> { 

		#region internal classes
		[System.Serializable]
		[System.Diagnostics.DebuggerDisplay( "Empty" )]
		private sealed class EmptyBinaryTree : IBinaryTree<T> { 
			private static readonly System.Int32 theHashCode;
			private static readonly System.Collections.Generic.ICollection<T> theValues;

			static EmptyBinaryTree() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
				}
				theValues = new System.Collections.Generic.List<T>( 1 ).AsReadOnly();
			}
			public EmptyBinaryTree() : base() { 
			}

			ITree<T> ITree<T>.Empty { 
				get { 
					return this.Empty;
				}
			}
			public IBinaryTree<T> Empty { 
				get { 
					return this;
				}
			}
			public System.Int32 Count { 
				get { 
					return 0;
				}
			}
			public System.Int32 BalanceFactor { 
				get { 
					return 0;
				}
			}
			public System.Int32 SlackFactor { 
				get { 
					return 0;
				}
			}
			public System.Boolean IsLeftHeavy { 
				get { 
					return false;
				}
			}
			public System.Boolean IsRightHeavy { 
				get { 
					return false;
				}
			}

			public System.Int32 Height { 
				get { 
					return 0;
				}
			}
			public System.Int32 Length { 
				get { 
					return 0;
				}
			}

			public System.Boolean IsEmpty { 
				get { 
					return true;
				}
			}
			public System.Boolean IsLeaf { 
				get { 
					return false;
				}
			}
			public System.Boolean IsFull { 
				get { 
					return true;
				}
			}

			public System.Boolean IsPerfect { 
				get { 
					return true;
				}
			}

			public T Value { 
				get { 
					throw new System.InvalidOperationException();
				}
			}
			System.Collections.Generic.IEnumerable<ITree<T>> ITree<T>.Branches { 
				get {
					return ((ITree<T>)this).Branches;
				}
			}
			public System.Collections.Generic.IEnumerable<IBinaryTree<T>> Branches { 
				get { 
					yield break;
				}
			}

			public System.Collections.Generic.ICollection<T> Values { 
				get { 
					return theValues;
				}
			}

			public IBinaryTree<T> Left { 
				get { 
					return this;
				}
			}
			public IBinaryTree<T> Right { 
				get { 
					return this;
				}
			}

			public System.Boolean IsInBalance( IBinaryTree<T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other " );
				}
				System.Int32 f = other.BalanceFactor;
				return ( ( -1 <= f ) && ( f <= 1 ) );
			}
			public IBinaryTree<T> SetLeft( IBinaryTree<T> left ) { 
				throw new System.InvalidOperationException();
			}
			public IBinaryTree<T> SetRight( IBinaryTree<T> right ) { 
				throw new System.InvalidOperationException();
			}
			public IBinaryTree<T> RotateLeft() { 
				return this;
			}
			public IBinaryTree<T> RotateRight() { 
				return this;
			}
			public IBinaryTree<T> RotateLeftRight() { 
				return this;
			}
			public IBinaryTree<T> RotateRightLeft() { 
				return this;
			}

			public System.Boolean IsReadOnly { 
				get { 
					return true;
				}
			}
			void System.Collections.Generic.ICollection<ITree<T>>.Add( ITree<T> item ) { 
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<ITree<T>>.Clear() { 
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.ICollection<ITree<T>>.Contains( ITree<T> item ) { 
				foreach ( ITree<T> tree in InOrder( this ) ) { 
					if ( tree.Equals( item ) ) { 
						return true;
					}
				}
				return false;
			}
			public void CopyTo( ITree<T>[] array, System.Int32 arrayIndex ) {
				if ( arrayIndex < 0 ) {
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) {
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) {
					throw new System.ArgumentException( null, "array" );
				}
				foreach ( ITree<T> tree in InOrder( this ) ) { 
					array[ arrayIndex++ ] = tree;
				}
			}
			System.Boolean System.Collections.Generic.ICollection<ITree<T>>.Remove( ITree<T> item ) { 
				throw new System.NotSupportedException();
			}

			void System.Collections.Generic.ICollection<IBinaryTree<T>>.Add( IBinaryTree<T> item ) {
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<IBinaryTree<T>>.Clear() {
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.ICollection<IBinaryTree<T>>.Contains( IBinaryTree<T> item ) {
				foreach ( ITree<T> tree in InOrder( this ) ) {
					if ( tree.Equals( item ) ) {
						return true;
					}
				}
				return false;
			}
			public void CopyTo( IBinaryTree<T>[] array, System.Int32 arrayIndex ) {
				if ( arrayIndex < 0 ) {
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) {
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) {
					throw new System.ArgumentException( null, "array" );
				}
				foreach ( IBinaryTree<T> tree in InOrder( this ) ) {
					array[ arrayIndex++ ] = tree;
				}
			}
			System.Boolean System.Collections.Generic.ICollection<IBinaryTree<T>>.Remove( IBinaryTree<T> item ) {
				throw new System.NotSupportedException();
			}

			void System.Collections.Generic.ICollection<T>.Add( T item ) { 
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<T>.Clear() { 
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.ICollection<T>.Contains( T item ) { 
				return this.Values.Contains( item );
			}
			public void CopyTo( T[] array, System.Int32 arrayIndex ) {
				if ( arrayIndex < 0 ) {
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) {
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) {
					throw new System.ArgumentException( null, "array" );
				}
				this.Values.CopyTo( array, arrayIndex );
			}
			System.Boolean System.Collections.Generic.ICollection<T>.Remove( T item ) { 
				throw new System.NotSupportedException();
			}

			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
				yield break;
			}
			System.Collections.Generic.IEnumerator<ITree<T>> ITree<T>.GetEnumerator() {
				yield break;
			}
			System.Collections.Generic.IEnumerator<ITree<T>> System.Collections.Generic.IEnumerable<ITree<T>>.GetEnumerator() { 
				yield break;
			}
			System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator() {
				yield break;
			}
			public System.Collections.Generic.IEnumerator<IBinaryTree<T>> GetEnumerator() { 
				yield break;
			}

			public override System.Int32 GetHashCode() { 
				return theHashCode;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;
		private static readonly IBinaryTree<T> theEmpty;

		private readonly T myValue;
		private readonly IBinaryTree<T> myLeft;
		private readonly IBinaryTree<T> myRight;
		private readonly System.Int32 myBalanceFactor;
		private readonly System.Int32 mySlackFactor;
		private readonly System.Int32 myHeight;
		private readonly System.Int32 myCount;
		private readonly System.Int32 myLength;
		private readonly System.Boolean myIsPerfect;
		private readonly System.Boolean myIsFull;
		private readonly System.Int32 myHashCode;
		private System.Collections.Generic.ICollection<T> myValues;
		#endregion fields


		#region .ctor
		static BinaryTree() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
			theEmpty = new EmptyBinaryTree();
		}

		private BinaryTree() : base() { 
			myHashCode = theHashCode;
		}
		private BinaryTree( IBinaryTree<T> left, T value, IBinaryTree<T> right ) : this() { 
			if ( null == left ) { 
				throw new System.ArgumentNullException( "left" );
			} else if ( null == right ) { 
				throw new System.ArgumentNullException( "right" );
			}

			myValue = value;

			myLeft = left;
			myRight = right;
			System.Int32 lh = left.Height;
			System.Int32 rh = right.Height;
			myBalanceFactor = rh - lh;
			myHeight = 1 + System.Math.Max( lh, rh );
			myLength = 1 + System.Math.Min( left.Length, right.Length );
			mySlackFactor = myHeight - myLength;
			myCount = 1 + left.Count + right.Count;
			myIsPerfect = ( myCount == ( (System.Int32)System.Math.Pow( 2.0, myHeight ) - 1 ) );

			if ( myLeft.IsEmpty ^ myRight.IsEmpty ) { 
				myIsFull = false;
			} else { 
				myIsFull = myLeft.IsFull && myRight.IsFull;
			}

			unchecked { 
				myHashCode += myLeft.GetHashCode();
				myHashCode += myRight.GetHashCode();
				if ( null != value ) { 
					myHashCode += value.GetHashCode();
				}
			}
		}
		#endregion .ctor


		#region properties
		ITree<T> ITree<T>.Empty { 
			get { 
				return this.Empty;
			}
		}
		public IBinaryTree<T> Empty { 
			get { 
				return GetEmpty();
			}
		}
		public System.Int32 Count { 
			get { 
				return myCount;
			}
		}
		public System.Int32 Length { 
			get { 
				return myLength;
			}
		}

		public System.Int32 BalanceFactor {
			get {
				return this.Right.Height - this.Left.Height;
			}
		}
		public System.Int32 SlackFactor { 
			get { 
				return mySlackFactor;
			}
		}
		public System.Boolean IsRightHeavy { 
			get { 
				return ( 2 <= this.BalanceFactor );
			}
		}
		public System.Boolean IsLeftHeavy { 
			get { 
				return ( this.BalanceFactor <= -2 );
			}
		}

		public System.Int32 Height { 
			get {
				return myHeight;
			}
		}

		public System.Boolean IsEmpty { 
			get { 
				return false;
			}
		}
		public System.Boolean IsFull { 
			get { 
				return myIsFull;
			}
		}
		public System.Boolean IsReadOnly {
			get {
				return true;
			}
		}

		public System.Boolean IsPerfect { 
			get { 
				return myIsPerfect;
			}
		}
		public System.Boolean IsLeaf { 
			get {
				return ( myLeft.IsEmpty && myRight.IsEmpty );
			}
		}

		public IBinaryTree<T> Left { 
			get { 
				return myLeft;
			}
		}

		public T Value { 
			get { 
				return myValue;
			}
		}

		public IBinaryTree<T> Right { 
			get { 
				return myRight;
			}
		}

		public System.Collections.Generic.ICollection<T> Values { 
			get { 
				if ( null == myValues ) { 
					System.Collections.Generic.List<T> values = new System.Collections.Generic.List<T>( this.Count );
					foreach ( IBinaryTree<T> tree in BinaryTree<T>.InOrder( this ) ) { 
						values.Add( tree.Value );
					}
					if ( null == myValues ) { 
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<T>>( 
							ref myValues, 
							values.AsReadOnly(), 
							null 
						);
					}
				}
				return myValues;
			}
		}

		System.Collections.Generic.IEnumerable<ITree<T>> ITree<T>.Branches { 
			get { 
				foreach ( IBinaryTree<T> tree in this.Branches ) { 
					yield return tree;
				}
			}
		}
		public System.Collections.Generic.IEnumerable<IBinaryTree<T>> Branches { 
			get { 
				yield return this.Left;
				yield return this.Right;
			}
		}
		#endregion properties


		#region methods
		public System.Boolean IsInBalance( IBinaryTree<T> other ) { 
			return this.Empty.IsInBalance( other );
		}
		public IBinaryTree<T> SetLeft( IBinaryTree<T> left ) { 
			if ( null == left ) { 
				throw new System.ArgumentNullException( "left" );
			}
			return new BinaryTree<T>( left, this.Value, this.Right );
		}
		public IBinaryTree<T> SetRight( IBinaryTree<T> right ) { 
			if ( null == right ) { 
				throw new System.ArgumentNullException( "right" );
			}
			return new BinaryTree<T>( this.Left, this.Value, right );
		}
		public IBinaryTree<T> RotateLeft() { 
			if ( this.Right.IsEmpty ) { 
				return this;
			} else { 
				IBinaryTree<T> root = this.Right;
				IBinaryTree<T> left = this.SetRight( root.Left );
				root = root.SetLeft( left );
				return root;
			}
		}
		public IBinaryTree<T> RotateRight() { 
			if ( this.Left.IsEmpty ) { 
				return this;
			} else { 
				IBinaryTree<T> root = this.Left;
				IBinaryTree<T> right = this.SetLeft( root.Right );
				root = root.SetRight( right );
				return root;
			}
		}
		public IBinaryTree<T> RotateLeftRight() { 
			if ( this.Right.IsEmpty ) { 
				return this;
			} else { 
				IBinaryTree<T> right = this.Right;
				return this.SetRight( right.RotateLeft() ).RotateLeft();
			}
		}
		public IBinaryTree<T> RotateRightLeft() { 
			if ( this.Left.IsEmpty ) { 
				return this;
			} else { 
				IBinaryTree<T> left = this.Left;
				return this.SetLeft( left.RotateLeft() ).RotateRight();
			}
		}

		void System.Collections.Generic.ICollection<ITree<T>>.Add( ITree<T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<ITree<T>>.Clear() {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<ITree<T>>.Contains( ITree<T> item ) {
			foreach ( ITree<T> tree in InOrder( this ) ) {
				if ( tree.Equals( item ) ) {
					return true;
				}
			}
			return false;
		}
		public void CopyTo( ITree<T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( IBinaryTree<T> tree in InOrder( this ) ) {
				array[ arrayIndex++ ] = tree;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<ITree<T>>.Remove( ITree<T> item ) {
			throw new System.NotSupportedException();
		}

		void System.Collections.Generic.ICollection<IBinaryTree<T>>.Add( IBinaryTree<T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<IBinaryTree<T>>.Clear() {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<IBinaryTree<T>>.Contains( IBinaryTree<T> item ) {
			foreach ( ITree<T> tree in InOrder( this ) ) {
				if ( tree.Equals( item ) ) {
					return true;
				}
			}
			return false;
		}
		public void CopyTo( IBinaryTree<T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( IBinaryTree<T> tree in InOrder( this ) ) {
				array[ arrayIndex++ ] = tree;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<IBinaryTree<T>>.Remove( IBinaryTree<T> item ) {
			throw new System.NotSupportedException();
		}

		void System.Collections.Generic.ICollection<T>.Add( T item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<T>.Clear() {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<T>.Contains( T item ) {
			return this.Values.Contains( item );
		}
		public void CopyTo( T[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			this.Values.CopyTo( array, arrayIndex );
		}
		System.Boolean System.Collections.Generic.ICollection<T>.Remove( T item ) {
			throw new System.NotSupportedException();
		}

		System.Collections.Generic.IEnumerator<ITree<T>> ITree<T>.GetEnumerator() { 
			foreach ( ITree<T> tree in this ) { 
				yield return tree;
			}
		}
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
		System.Collections.Generic.IEnumerator<ITree<T>> System.Collections.Generic.IEnumerable<ITree<T>>.GetEnumerator() {
			foreach ( IBinaryTree<T> tree in this ) { 
				yield return tree;
			}
		}
		System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator() {
			return this.Values.GetEnumerator();
		}
		public System.Collections.Generic.IEnumerator<IBinaryTree<T>> GetEnumerator() { 
			return InOrder( this ).GetEnumerator();
		}

		public override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static IBinaryTree<T> GetEmpty() { 
			return theEmpty;
		}

		public static System.Collections.Generic.IEnumerable<IBinaryTree<T>> PreOrder( IBinaryTree<T> tree ) { 
			if ( ( null == tree ) || ( tree.IsEmpty ) ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IStack<IBinaryTree<T>> stack = Stack<IBinaryTree<T>>.GetEmpty().Push( tree );
			IBinaryTree<T> current;
			IBinaryTree<T> left;
			IBinaryTree<T> right;
			while ( !stack.IsEmpty ) { 
				current = stack.Peek();
				stack = stack.Pop();
				right = current.Right;
				if ( !right.IsEmpty ) { 
					stack = stack.Push( right );
				}
				left = current.Left;
				if ( !left.IsEmpty ) { 
					stack = stack.Push( left );
				}
				yield return current;
			}
		}
		public static System.Collections.Generic.IEnumerable<IBinaryTree<T>> InOrder( IBinaryTree<T> tree ) { 
			if ( ( null == tree ) || ( tree.IsEmpty ) ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IStack<IBinaryTree<T>> stack = Stack<IBinaryTree<T>>.GetEmpty();
			for ( IBinaryTree<T> current = tree; ( false == current.IsEmpty ) || ( false == stack.IsEmpty ); current = current.Right ) { 
				while ( false == current.IsEmpty ) { 
					stack = stack.Push( current );
					current = current.Left;
				}
				current = stack.Peek();
				stack = stack.Pop();
				yield return current;
			}
		}
		public static System.Collections.Generic.IEnumerable<IBinaryTree<T>> PostOrder( IBinaryTree<T> tree ) { 
			if ( ( null == tree ) || ( tree.IsEmpty ) ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IStack<IBinaryTree<T>> stack = Stack<IBinaryTree<T>>.GetEmpty().Push( tree );
			IStack<IBinaryTree<T>> visited = Stack<IBinaryTree<T>>.GetEmpty();
			IBinaryTree<T> current;
			IBinaryTree<T> left;
			IBinaryTree<T> right;
			while ( !stack.IsEmpty ) { 
				current = stack.Peek();
				if ( ( !visited.IsEmpty ) && ( visited.Peek() == current ) ) { 
					stack = stack.Pop();
					visited = visited.Pop();
					yield return current;
				} else if ( !current.IsLeaf ) { 
					visited = visited.Push( current );
					right = current.Right;
					if ( !right.IsEmpty ) { 
						stack = stack.Push( right );
					}
					left = current.Left;
					if ( !left.IsEmpty ) { 
						stack = stack.Push( left );
					}
				} else { 
					stack = stack.Pop();
					yield return current;
				}
			}
		}
		public static System.Collections.Generic.IEnumerable<IBinaryTree<T>> LevelOrder( IBinaryTree<T> tree ) { 
			if ( ( null == tree ) || ( tree.IsEmpty ) ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IQueue<IBinaryTree<T>> hold = Queue<IBinaryTree<T>>.GetEmpty().Enqueue( tree );
			IBinaryTree<T> current;
			IBinaryTree<T> left;
			IBinaryTree<T> right;
			while ( !hold.IsEmpty ) { 
				current = hold.Peek();
				hold = hold.Dequeue();
				left = current.Left;
				if ( !left.IsEmpty ) { 
					hold = hold.Enqueue( left );
				}
				right = current.Right;
				if ( !right.IsEmpty ) { 
					hold = hold.Enqueue( right );
				}
				yield return current;
			}
		}
		#endregion static methods

	}

}