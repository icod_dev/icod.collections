namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class PriorityQueue<P, T> : IPriorityQueue<P, T> { 

		#region internal classes
		[System.Serializable]
		[System.Diagnostics.DebuggerDisplay( "Empty" )]
		private sealed class EmptyPriorityQueue : IPriorityQueue<P, T> { 
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary<System.Collections.Generic.IComparer<P>, IPriorityQueue<P, T>> theEmpty;
			private static readonly KeyGenerator<IDeque<T>, P> theKeyGenerator;
			private readonly IKeyLogic<P, IDeque<T>> myKeyLogic;
			private readonly System.Int32 myHashCode;
			private readonly System.Collections.Generic.IComparer<P> myComparer;

			static EmptyPriorityQueue() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( P ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary<System.Collections.Generic.IComparer<P>, IPriorityQueue<P, T>>();
				theKeyGenerator = KeyLogic<P, IDeque<T>>.DefaultGenerator;
			}
			private EmptyPriorityQueue() : base() { 
				myHashCode = theHashCode;
			}
			private EmptyPriorityQueue( System.Collections.Generic.IComparer<P> comparer ) : this() { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				myComparer = comparer;
				unchecked { 
					myHashCode += myComparer.GetHashCode();
				}
				myKeyLogic = KeyLogic<P, IDeque<T>>.Get( theKeyGenerator, myComparer );
			}

			public IPriorityQueue<P, T> Empty { 
				get { 
					return this;
				}
			}
			public System.Int32 Count { 
				get { 
					return 0;
				}
			}
			public System.Boolean IsEmpty { 
				get { 
					return true;
				}
			}
			public System.Collections.Generic.IComparer<P> Comparer { 
				get { 
					return myComparer;
				}
			}
			public System.Collections.Generic.IEnumerable<P> Priorities { 
				get { 
					yield break;
				}
			}

			public System.Collections.Generic.KeyValuePair<P, T> PeekMin() { 
				throw new System.InvalidOperationException();
			}
			public System.Collections.Generic.KeyValuePair<P, T> PeekMax() { 
				throw new System.InvalidOperationException();
			}
			public IPriorityQueue<P, T> Add( P priority, T item ) { 
				return new PriorityQueue<P, T>( MinMaxTree<P, IDeque<T>>.GetEmpty( myKeyLogic ).Add( Deque<T>.GetEmpty().EnqueueRight( item ) ), 1 );
			}
			public IPriorityQueue<P, T> DequeueMin() { 
				throw new System.InvalidOperationException();
			}
			public IPriorityQueue<P, T> DequeueMax() { 
				throw new System.InvalidOperationException();
			}
			public System.Collections.Generic.KeyValuePair<P, T> Peek( P priority ) { 
				throw new System.InvalidOperationException();
			}
			public IPriorityQueue<P, T> Dequeue( P prioroty ) { 
				throw new System.InvalidOperationException();
			}
			public System.Boolean ContainsPriority( P priority ) { 
				return false;
			}
			public System.Boolean Contains( System.Collections.Generic.KeyValuePair<P, T> item ) { 
				return false;
			}
			public System.Boolean Contains( P priority, T item ) { 
				return false;
			}
			public System.Boolean ContainsItem( T item ) { 
				return false;
			}
			public System.Boolean ContainsItem( T item, System.Collections.Generic.IEqualityComparer<T> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				return false;
			}
			public IPriorityQueue<P, T> DecreasePriority( T item ) { 
				throw new System.InvalidOperationException();
			}
			public IPriorityQueue<P, T> DecreasePriority( T item, System.Collections.Generic.IEqualityComparer<T> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				throw new System.InvalidOperationException();
			}
			public IPriorityQueue<P, T> IncreasePriority( T item ) { 
				throw new System.InvalidOperationException();
			}
			public IPriorityQueue<P, T> IncreasePriority( T item, System.Collections.Generic.IEqualityComparer<T> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				throw new System.InvalidOperationException();
			}
			public IPriorityQueue<P, T> UpdatePriority( T item, P priority ) { 
				throw new System.InvalidOperationException();
			}
			public IPriorityQueue<P, T> UpdatePriority( T item, P priority, System.Collections.Generic.IEqualityComparer<T> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				throw new System.InvalidOperationException();
			}

			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { 
				return this.GetEnumerator();
			}
			public System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<P, T>> GetEnumerator() { 
				yield break;
			}

			public sealed override System.Int32 GetHashCode() { 
				return myHashCode;
			}
			public sealed override System.Boolean Equals( System.Object obj ) { 
				return this.Equals( obj as EmptyPriorityQueue );
			}
			public System.Boolean Equals( EmptyPriorityQueue other ) { 
				if ( null == other ) { 
					return false;
				} else {
					return System.Object.ReferenceEquals( this, other );
				}
			}

			public static IPriorityQueue<P, T> Get() { 
				return Get( Comparer<P>.Default );
			}
			public static IPriorityQueue<P, T> Get( System.Collections.Generic.IComparer<P> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				IPriorityQueue<P, T> empty;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( comparer ) ) { 
						empty = theEmpty[ comparer ];
					} else { 
						empty = new EmptyPriorityQueue( comparer );
						theEmpty.Add( comparer, empty );
					}
				}
				return empty;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;

		private readonly IBinarySearchTree<P, IDeque<T>> myStore;
		private readonly System.Int32 myCount;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static PriorityQueue() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked {
				theHashCode += typeof( P ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
		}

		private PriorityQueue() : base() { 
			myHashCode = theHashCode;
		}
		private PriorityQueue( IBinarySearchTree<P, IDeque<T>> store, System.Int32 count ) : this() { 
			if ( ( null == store ) || store.IsEmpty ) { 
				throw new System.ArgumentNullException( "store" );
			} else if ( count < store.Count ) { 
				throw new System.ArgumentOutOfRangeException( "count" );
			}

			myStore = store;
			myCount = count;
			unchecked { 
				myHashCode += myStore.GetHashCode();
				myHashCode += myCount;
			}
		}
		#endregion .ctor


		#region properties
		public IPriorityQueue<P, T> Empty { 
			get { 
				return GetEmpty( this.Comparer );
			}
		}
		public System.Int32 Count { 
			get { 
				return myCount;
			}
		}
		public System.Boolean IsEmpty { 
			get { 
				return ( 0 == myCount );
			}
		}
		public System.Collections.Generic.IComparer<P> Comparer { 
			get { 
				return myStore.KeyLogic.Comparer;
			}
		}
		public System.Collections.Generic.IEnumerable<P> Priorities { 
			get { 
				foreach ( IBinarySearchTree<P, IDeque<T>> node in BinarySearchTreeBase<P, IDeque<T>>.LevelOrder( myStore ) ) { 
					yield return node.Key;
				}
			}
		}
		#endregion properties


		#region methods
		public System.Collections.Generic.KeyValuePair<P, T> PeekMin() { 
			IBinarySearchTree<P, IDeque<T>> store = myStore.Min;
			return new System.Collections.Generic.KeyValuePair<P, T>( store.Key, store.Value.PeekLeft() );
		}
		public System.Collections.Generic.KeyValuePair<P, T> PeekMax() { 
			IBinarySearchTree<P, IDeque<T>> store = myStore.Max;
			return new System.Collections.Generic.KeyValuePair<P, T>( store.Key, store.Value.PeekLeft() );
		}
		public IPriorityQueue<P, T> Add( P priority, T item ) { 
			IBinarySearchTree<P, IDeque<T>> store = myStore.Find( priority );
			if ( 0 != this.Comparer.Compare( store.Key, priority ) ) { 
				store = store.Add( priority, Deque<T>.GetEmpty().EnqueueRight( item ) );
			} else { 
				IDeque<T> list = store.Value.EnqueueRight( item );
				store = store.Empty.Add( priority, list ).SetLeft( store.Left ).SetRight( store.Right );
			}
			return new PriorityQueue<P, T>( store, myCount + 1 );
		}
		public IPriorityQueue<P, T> DequeueMin() { 
			IBinarySearchTree<P, IDeque<T>> store = myStore;
			IBinarySearchTree<P, IDeque<T>> left = store.Left;
			IBinarySearchTree<P, IDeque<T>> min;
			if ( left.IsEmpty ) { 
				min = store;
			} else { 
				min = left;
			}
			IDeque<T> value = min.Value.DequeueLeft();
			if ( value.IsEmpty ) { 
				min = min.Remove( min.Key );
			} else { 
				min = min.Empty.Add( min.Key, value ).SetRight( min.Right );
			}
			if ( left.IsEmpty ) { 
				store = min;
			} else { 
				store = store.SetLeft( min );
			}
			if ( store.IsEmpty ) { 
				return this.Empty;
			} else { 
				return new PriorityQueue<P, T>( store, myCount - 1 );
			}
		}
		public IPriorityQueue<P, T> DequeueMax() { 
			IBinarySearchTree<P, IDeque<T>> store = myStore;
			IBinarySearchTree<P, IDeque<T>> right = store.Right;
			IBinarySearchTree<P, IDeque<T>> max;
			if ( right.IsEmpty ) { 
				max = store;
			} else { 
				max = right;
			}
			IDeque<T> value = max.Value.DequeueLeft();
			if ( value.IsEmpty ) { 
				max = max.Remove( max.Key );
			} else { 
				max = max.Empty.Add( max.Key, value ).SetLeft( max.Left );
			}
			if ( right.IsEmpty ) { 
				store = max;
			} else { 
				store = store.SetRight( max );
			}
			if ( store.IsEmpty ) { 
				return this.Empty;
			} else { 
				return new PriorityQueue<P, T>( store, myCount - 1 );
			}
		}
		public System.Collections.Generic.KeyValuePair<P, T> Peek( P priority ) { 
			IBinarySearchTree<P, IDeque<T>> find = myStore.Find( priority );
			if ( 0 != this.Comparer.Compare( find.Key, priority ) ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			} else { 
				return new System.Collections.Generic.KeyValuePair<P,T>( find.Key, find.Value.PeekLeft() );
			}
		}
		public IPriorityQueue<P, T> Dequeue( P priority ) { 
			IBinarySearchTree<P, IDeque<T>> find = myStore.Find( priority );
			if ( 0 != this.Comparer.Compare( find.Key, priority ) ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			} else { 
				IDeque<T> list = find.Value.DequeueLeft();
				if ( list.IsEmpty ) { 
					find = find.Remove( priority );
				} else { 
					find = myStore.Empty.Add( 
						priority, 
						list 
					).SetLeft( 
						find.Left 
					).SetRight( 
						find.Right 
					);
				}
				if ( find.IsEmpty ) { 
					return this.Empty;
				} else { 
					return new PriorityQueue<P, T>( find, myCount - 1 );
				}
			}
		}
		public System.Boolean ContainsPriority( P priority ) { 
			IBinarySearchTree<P, IDeque<T>> find = myStore.Find( priority );
			return ( 0 == this.Comparer.Compare( find.Key, priority ) );
		}
		public System.Boolean Contains( System.Collections.Generic.KeyValuePair<P, T> item ) { 
			return this.Contains( item.Key, item.Value );
		}
		public System.Boolean Contains( P priority, T item ) { 
			IBinarySearchTree<P, IDeque<T>> find = myStore.Find( priority );
			if ( 0 != this.Comparer.Compare( find.Key, priority ) ) { 
				return false;
			} else { 
				System.Collections.Generic.IEqualityComparer<T> comparer = System.Collections.Generic.EqualityComparer<T>.Default;
				IDeque<T> list = find.Value;
				while ( !list.IsEmpty ) { 
					if ( comparer.Equals( list.PeekLeft(), item ) ) { 
						return true;
					}
					list = list.DequeueLeft();
				}
				return false;
			}
		}

		public System.Boolean ContainsItem( T item ) { 
			return this.ContainsItem( item, System.Collections.Generic.EqualityComparer<T>.Default );
		}
		public System.Boolean ContainsItem( T item, System.Collections.Generic.IEqualityComparer<T> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			foreach ( IBinarySearchTree<P, IDeque<T>> tree in BinarySearchTreeBase<P, IDeque<T>>.InOrder( myStore ) ) { 
				if ( tree.Value.Contains( item, comparer ) ) { 
					return true;
				}
			}
			return false;
		}
		public IPriorityQueue<P, T> DecreasePriority( T item ) { 
			return this.DecreasePriority( item, System.Collections.Generic.EqualityComparer<T>.Default );
		}
		public IPriorityQueue<P, T> DecreasePriority( T item, System.Collections.Generic.IEqualityComparer<T> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			} else if ( comparer.Equals( this.PeekMin().Value, item ) ) { 
				return this;
			}

			System.Boolean isFound = false;
			IBinarySearchTree<P, IDeque<T>> store = myStore.Empty;
			IDeque<T> d;
			IDeque<T> hold;
			T peek;
			foreach ( IBinarySearchTree<P, IDeque<T>> tree in BinarySearchTreeBase<P, IDeque<T>>.InOrder( myStore ) ) { 
				d = tree.Value;
				if ( !isFound && d.Contains( item, comparer ) ) { 
					isFound = true;
					if ( !comparer.Equals( d.PeekLeft(), item ) ) { 
						hold = d.Empty;
						while ( !d.IsEmpty ) { 
							peek = d.PeekLeft();
							if ( comparer.Equals( peek, item ) ) { 
								peek = hold.PeekRight();
								hold = hold.DequeueRight().EnqueueRight( d.PeekLeft() ).EnqueueRight( peek );
							} else { 
								hold = hold.EnqueueRight( peek );
							}
							d = d.DequeueLeft();
						}
						d = hold;
					} else { 
						IBinarySearchTree<P, IDeque<T>> a = store.Max;
						peek = a.Value.PeekRight();
						store = store.Remove( a.Key ).Add( a.Key, a.Value.DequeueRight().EnqueueRight( d.PeekLeft() ).EnqueueRight( peek ) );
						d = d.DequeueLeft();
					}
				}
				store = store.Add( tree.Key, d );
			}
			if ( !isFound ) { 
				return this;
			} else { 
				return new PriorityQueue<P, T>( store, myCount );
			}
		}
		public IPriorityQueue<P, T> IncreasePriority( T item ) { 
			return this.IncreasePriority( item, System.Collections.Generic.EqualityComparer<T>.Default );
		}
		public IPriorityQueue<P, T> IncreasePriority( T item, System.Collections.Generic.IEqualityComparer<T> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			} else if ( comparer.Equals( myStore.Max.Value.PeekRight(), item ) ) { 
				return this;
			}

			System.Boolean isFound = false;
			IBinarySearchTree<P, IDeque<T>> store = myStore.Empty;
			IDeque<T> d;
			IDeque<T> hold;
			T peek;
			foreach ( IBinarySearchTree<P, IDeque<T>> tree in BinarySearchTreeBase<P, IDeque<T>>.ReverseOrder( myStore ) ) { 
				d = tree.Value;
				if ( !isFound && d.Contains( item, comparer ) ) { 
					isFound = true;
					if ( !comparer.Equals( d.PeekRight(), item ) ) { 
						hold = d.Empty;
						while ( !d.IsEmpty ) { 
							peek = d.PeekRight();
							if ( comparer.Equals( peek, item ) ) { 
								peek = hold.PeekLeft();
								hold = hold.DequeueLeft().EnqueueLeft( d.PeekLeft() ).EnqueueLeft( peek );
							} else { 
								hold = hold.EnqueueLeft( peek );
							}
							d = d.DequeueLeft();
						}
						d = hold;
					} else { 
						IBinarySearchTree<P, IDeque<T>> a = store.Min;
						peek = a.Value.PeekLeft();
						store = store.Remove( a.Key ).Add( a.Key, a.Value.DequeueLeft().EnqueueLeft( d.PeekRight() ).EnqueueLeft( peek ) );
						d = d.DequeueRight();
					}
				}
				store = store.Add( tree.Key, d );
			}
			if ( !isFound ) { 
				return this;
			} else { 
				throw new System.NotImplementedException();
			}
		}
		public IPriorityQueue<P, T> UpdatePriority( T item, P priority ) { 
			return this.UpdatePriority( item, priority, System.Collections.Generic.EqualityComparer<T>.Default );
		}
		public IPriorityQueue<P, T> UpdatePriority( T item, P priority, System.Collections.Generic.IEqualityComparer<T> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			System.Boolean isFound = false;
			IBinarySearchTree<P, IDeque<T>> store = myStore.Empty;
			IDeque<T> d;
			IDeque<T> hold;
			T peek;
			foreach ( IBinarySearchTree<P, IDeque<T>> tree in BinarySearchTreeBase<P, IDeque<T>>.LevelOrder( myStore ) ) {
				d = tree.Value;
				if ( !isFound && d.Contains( item, comparer ) ) { 
					if ( 0 == this.Comparer.Compare( priority, tree.Key ) ) { 
						return this;
					} else { 
						isFound = true;
						hold = d.Empty;
						while ( !d.IsEmpty ) { 
							peek = d.PeekLeft();
							if ( !comparer.Equals( peek, item ) ) { 
								hold = hold.EnqueueRight( peek );
							}
							d = d.DequeueLeft();
						}
						d = hold;
					}
				}
				store = store.Add( tree.Key, d );
			}
			System.Int32 count = myCount;
			if ( isFound ) { 
				count = count - 1;
			}
			return new PriorityQueue<P, T>( store, count ).Add( priority, item );
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { 
			return this.GetEnumerator();
		}
		public System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<P, T>> GetEnumerator() { 
			IDeque<T> list;
			foreach ( IBinarySearchTree<P, IDeque<T>> node in BinarySearchTreeBase<P, IDeque<T>>.InOrder( myStore ) ) { 
				list = node.Value;
				while ( !list.IsEmpty ) { 
					yield return new System.Collections.Generic.KeyValuePair<P, T>( node.Key, list.PeekLeft() );
					list = list.DequeueLeft();
				}
			}
		}
		#endregion methods


		#region static methods
		public static IPriorityQueue<P, T> GetEmpty() { 
			return EmptyPriorityQueue.Get();
		}
		public static IPriorityQueue<P, T> GetEmpty( System.Collections.Generic.IComparer<P> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			return EmptyPriorityQueue.Get( comparer );
		}
		#endregion static methods

	}

}