namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class SplayTree<K, T> : BinarySearchTreeBase<K, T> { 

		#region internal classes
		new private sealed class EmptyTree : BinarySearchTreeBase<K, T>.EmptyTree { 
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary<IKeyLogic<K, T>, IBinarySearchTree<K, T>> theEmpty;
			private readonly System.Int32 myHashCode;

			static EmptyTree() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary<IKeyLogic<K, T>, IBinarySearchTree<K, T>>();
			}
			private EmptyTree( IKeyLogic<K, T> keyLogic ) : base( keyLogic ) { 
				myHashCode = theHashCode;
				unchecked { 
					myHashCode += keyLogic.GetHashCode();
				}
			}

			public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
				return new SplayTree<K, T>( this, key, item, this.KeyLogic, this );
			}
			public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( other.IsEmpty ) { 
					return this;
				} else { 
					return new SplayTree<K, T>( 
						other.Left, 
						other, 
						other.Right 
					);
				}
			}

			public sealed override System.Boolean IsInBalance( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				if ( 
					other.IsEmpty 
					|| other.IsLeaf 
					|| other.IsPerfect 
					|| ( other.Count <= 2 ) 
				) { 
					return true;
				}

				System.Int32 f = other.BalanceFactor;
				return ( ( -1 <= f ) && ( f <= 1 ) );
			}
			public sealed override IBinarySearchTree<K, T> BalanceTree( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}

				while ( !other.IsInBalance( other ) ) { 
					other = this.BalanceVertex( other );
				}
				return other;
			}
			private IBinarySearchTree<K, T> BalanceVertex( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( 
					other.IsEmpty 
					|| other.IsLeaf 
					|| other.IsPerfect 
					|| ( other.Count <= 2 ) 
				) { 
					return other;
				}

				IBinarySearchTree<K, T> probe;
				if ( other.IsRightHeavy ) { 
					probe = other.Right;
					if ( probe.IsLeftHeavy ) { 
						other = other.RotateLeftRight();
					} else { 
						other = other.RotateLeft();
					}
				} else if ( other.IsLeftHeavy ) { 
					probe = other.Left;
					if ( probe.IsRightHeavy ) { 
						other = other.RotateRightLeft();
					} else { 
						other = other.RotateRight();
					}
				}

				return other;
			}

			public static IBinarySearchTree<K, T> Get() { 
				return Get( KeyLogic<K, T>.Default );
			}
			public static IBinarySearchTree<K, T> Get( System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				}
				return Get( KeyLogic<K, T>.Get( keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> keyGenerator ) { 
				if ( null == keyGenerator ) { 
					throw new System.ArgumentNullException( "keyGenerator" );
				}
				return Get( KeyLogic<K, T>.Get( keyGenerator ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> keyGenerator, System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				} else if ( null == keyGenerator ) { 
					throw new System.ArgumentNullException( "keyGenerator" );
				}
				return Get( KeyLogic<K, T>.Get( keyGenerator, keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( IKeyLogic<K, T> keyLogic ) { 
				if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				}
				IBinarySearchTree<K, T> output = null;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( keyLogic ) ) { 
						output = theEmpty[ keyLogic ];
					} else { 
						output = new EmptyTree( keyLogic );
						theEmpty.Add( keyLogic, output );
					}
				}
				return output;
			}

			public sealed override System.Int32 GetHashCode() { 
				return myHashCode;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;
		private static BinarySearchTreeCtor<K, T> SplayCtor;

		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static SplayTree() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
			SplayCtor = delegate( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) { 
				return new SplayTree<K, T>( left, root, right );
			};
		}

		private SplayTree( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) : base( left, root, right ) { 
			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		private SplayTree( IBinarySearchTree<K, T> left, K key, T value, IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> right ) : base( left, key, value, keyLogic, right ) { 
			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public sealed override IBinarySearchTree<K, T> Empty { 
			get { 
				return GetEmpty( this.KeyLogic );
			}
		}
		public sealed override IBinarySearchTree<K, T> this[ K key ] { 
			get { 
				IBinarySearchTree<K, T> probe = this.Find( key );
				if ( 0 == probe.KeyLogic.Comparer.Compare( probe.Key, key ) ) { 
					return probe;
				} else { 
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}
		}
		#endregion properties


		#region methods
		public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
			IBinarySearchTree<K, T> current = this.Find( key );
			IBinarySearchTree<K, T> left;
			IBinarySearchTree<K, T> right;

			System.Collections.Generic.IComparer<K> comparer = current.KeyLogic.Comparer;
			System.Int32 c = comparer.Compare( current.Key, key );
			if ( c < 0 ) { 
				left = current.SetRight( current.Empty );
				right = current.Right;
				current = new SplayTree<K, T>( 
					left, 
					key, item, this.KeyLogic, 
					right 
				);
			} else if ( 0 < c ) { 
				left = current.Left;
				right = current.SetLeft( current.Empty );
				current = new SplayTree<K, T>( 
					left, 
					key, item, this.KeyLogic, 
					right 
				);
			} else {
				throw new DuplicateKeyException( "key" );
			}

			return current;
		}
		protected sealed override IBinarySearchTree<K, T> ReconstituteTree( IStack<IBinarySearchTree<K, T>> branch, IStack<System.Boolean> isLeft ) { 
			if ( null == branch ) { 
				throw new System.ArgumentNullException( "branch" );
			} else if ( null == isLeft ) { 
				throw new System.ArgumentNullException( "isLeft" );
			} else if ( branch.Count <= isLeft.Count ) { 
				throw new System.ArgumentOutOfRangeException( "isLeft", "isLeft must contain more elements than branch parameter." );
			}

			IBinarySearchTree<K, T> current;
			IBinarySearchTree<K, T> parent;
			while ( 1 < branch.Count ) { 
				current = branch.Peek();
				branch = branch.Pop();
				parent = branch.Peek();
				branch = branch.Pop();
				if ( isLeft.Peek() ) { 
					parent = new SplayTree<K, T>( 
						current, 
						parent, 
						parent.Right 
					).RotateRight();
				} else { 
					parent = new SplayTree<K, T>( 
						parent.Left, 
						parent, 
						current 
					).RotateLeft();
				}
				branch = branch.Push( parent );
				isLeft = isLeft.Pop();
			}

			return branch.Peek();
		}

		public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			} else { 
				return this.Merge( other, DefaultOperator, DefaultOperator );
			}
		}

		public sealed override IBinarySearchTree<K, T> RotateLeft() { 
			if ( this.Right.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> root = this.Right;
				IBinarySearchTree<K, T> leftRight = root.Left;
				IBinarySearchTree<K, T> right = root.Right;
				return new SplayTree<K, T>( 
					new SplayTree<K, T>( 
						this.Left, 
						this, 
						leftRight 
					), 
					root, 
					right 
				);
			}
		}
		public sealed override IBinarySearchTree<K, T> RotateRight() { 
			if ( this.Left.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> root = this.Left;
				IBinarySearchTree<K, T> rightLeft = root.Right;
				IBinarySearchTree<K, T> left = root.Left;
				root = new SplayTree<K, T>( 
					left, 
					root, 
					new SplayTree<K, T>( 
						rightLeft, 
						this, 
						this.Right 
					) 
				);
				return root;
			}
		}
		public sealed override IBinarySearchTree<K, T> RotateLeftRight() { 
			if ( this.Right.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> right = this.Right.RotateRight();
				return new SplayTree<K, T>( 
					this.Left, 
					this, 
					right 
				).RotateLeft();
			}
		}
		public sealed override IBinarySearchTree<K, T> RotateRightLeft() { 
			if ( this.Left.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> left = this.Left.RotateLeft();
				return new SplayTree<K, T>( 
					left, 
					this, 
					this.Right 
				).RotateRight();
			}
		}

		public sealed override IBinarySearchTree<K, T> SetLeft( IBinarySearchTree<K, T> left ) { 
			return this.SetLeft( left, SplayCtor );
		}
		public sealed override IBinarySearchTree<K, T> SetRight( IBinarySearchTree<K, T> right ) { 
			return this.SetRight( right, SplayCtor );
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static IBinarySearchTree<K, T> GetEmpty() { 
			return EmptyTree.Get();
		}
		public static IBinarySearchTree<K, T> GetEmpty( System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}
			return EmptyTree.Get( keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator ) { 
			if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			} else { 
				return EmptyTree.Get( generator );
			}
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}
			return EmptyTree.Get( generator, keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IKeyLogic<K, T> keyLogic ) { 
			if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			} else { 
				return EmptyTree.Get( keyLogic );
			}
		}
		#endregion static methods

	}

}