namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public abstract class GraphBase<V, W> { 

		#region internal classes
		protected abstract class EmptyGraph { 
			private readonly IComparer<W> myWeightComparer;
			private readonly System.Collections.Generic.IEqualityComparer<V> myVertexComparer;
			private readonly ISet<V> myVertexes;

			private EmptyGraph() : base() { 
				myWeightComparer = null;
				myVertexComparer = null;
			}
			protected EmptyGraph( IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) : this() { 
				if ( null == vertexComparer ) { 
					throw new System.ArgumentNullException( "vertexComparer" );
				} else if ( null == weightComparer ) { 
					throw new System.ArgumentNullException( "weightComparer" );
				}
				myWeightComparer = weightComparer;
				myVertexComparer = vertexComparer;
				myVertexes = Set<V>.GetEmpty( myVertexComparer );
			}

			public System.Boolean IsEmpty { 
				get { 
					return true;
				}
			}
			public System.Boolean IsConnected { 
				get { 
					return false;
				}
			}
			public IComparer<W> WeightComparer { 
				get { 
					return myWeightComparer;
				}
			}
			public System.Collections.Generic.IEqualityComparer<V> VertexComparer { 
				get { 
					return myVertexComparer;
				}
			}
			public ISet<V> Vertexes { 
				get { 
					return myVertexes;
				}
			}
			public abstract ISet<IEdge<V, W>> Edges { 
				get;
			}
			public IEdge<V, W> this[ V a, V b ] { 
				get { 
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}

			public System.Boolean ContainsEdge( V a, V b ) { 
				return false;
			}
			public System.Boolean TryGetEdge( V a, V b, out IEdge<V, W> edge ) { 
				edge = null;
				return false;
			}
			public System.Boolean IsSymmetric( V a, V b ) { 
				return false;
			}
			public System.Boolean IsSymmetric( IEdge<V, W> edge ) { 
				if ( null == edge ) { 
					throw new System.ArgumentNullException( "edge" );
				}
				return false;
			}

			public ISet<IEdge<V, W>> GetAdjacentTo( V vertex ) { 
				return this.Edges;
			}

			public System.Int32 GetHashCode( IEdge<V, W> obj ) { 
				if ( null == (System.Object)obj ) { 
					return 0;
				} else { 
					return obj.GetHashCode();
				}
			}
			public virtual System.Boolean Equals( IEdge<V, W> x, IEdge<V, W> y ) { 
				if ( ( null == (System.Object)x ) && ( null == (System.Object)y ) ) { 
					return true;
				} else if ( ( null == (System.Object)x ) || ( null == (System.Object)y ) ) {
					return false;
				} else if ( System.Object.ReferenceEquals( x, y ) ) { 
					return true;
				} else { 
					return ( 
						myWeightComparer.Equals( x.Weight, y.Weight ) 
						&& x.Vertexes.Equals( y.Vertexes ) 
					);
				}
			}
		}
		#endregion internal classes


		#region fields
		private readonly ISet<V> myVertexes;
		private readonly ISet<IEdge<V, W>> myEdges;
		private readonly IHashTable<V, ISet<IEdge<V, W>>> myOutBound;
		private readonly IComparer<W> myWeightComparer;
		private readonly System.Collections.Generic.IEqualityComparer<V> myVertexComparer;
		#endregion fields


		#region .ctor
		private GraphBase() : base() { 
		}
		protected GraphBase( ISet<V> vertexes, ISet<IEdge<V, W>> edges, IHashTable<V, ISet<IEdge<V, W>>> outBound, IComparer<W> weightComparer, System.Collections.Generic.IEqualityComparer<V> vertexComparer ) : this() { 
			if ( null == vertexComparer ) { 
				throw new System.ArgumentNullException( "vertexComparer" );
			} else if ( null == weightComparer ) { 
				throw new System.ArgumentNullException( "weightComparer" );
			} else if ( null == outBound ) { 
				throw new System.ArgumentNullException( "outBound" );
			} else if ( null == edges ) { 
				throw new System.ArgumentNullException( "edges" );
			} else if ( ( null == vertexes ) || ( vertexes.IsEmpty ) ) { 
				throw new System.ArgumentNullException( "vertexes" );
			}

			myVertexes = vertexes;
			myEdges = edges;
			myOutBound = outBound;
			myWeightComparer = weightComparer;
			myVertexComparer = vertexComparer;
		}
		#endregion .ctor


		#region properties
		public System.Boolean IsEmpty { 
			get { 
				return false;
			}
		}
		public abstract System.Boolean IsConnected { 
			get;
		}
		public IComparer<W> WeightComparer { 
			get { 
				return myWeightComparer;
			}
		}
		public System.Collections.Generic.IEqualityComparer<V> VertexComparer { 
			get { 
				return myVertexComparer;
			}
		}
		public ISet<V> Vertexes { 
			get { 
				return myVertexes;
			}
		}
		public ISet<IEdge<V, W>> Edges { 
			get { 
				return myEdges;
			}
		}
		protected IHashTable<V, ISet<IEdge<V, W>>> OutBound { 
			get { 
				return myOutBound;
			}
		}
		#endregion properties


		#region methods
		public virtual System.Boolean ContainsEdge( V a, V b ) { 
			IEdge<V, W> foo;
			return this.TryGetEdge( a, b, out foo );
		}
		public abstract System.Boolean TryGetEdge( V a, V b, out IEdge<V, W> edge );
		public virtual System.Boolean IsSymmetric( V a, V b ) { 
			return this.ContainsEdge( a, b );
		}
		public virtual System.Boolean IsSymmetric( IEdge<V, W> edge ) { 
			return this.Edges.Contains( edge );
		}
		#endregion methods

	}

}