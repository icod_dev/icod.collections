namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	internal enum GraphCyclicity : System.Int32 { 

		Untested = 0, 
		False = 1, 
		True = 2, 

	}

}