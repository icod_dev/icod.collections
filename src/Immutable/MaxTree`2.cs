namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class MaxTree<K, T> : BinarySearchTreeBase<K, T> { 

		#region internal classes
		new private sealed class EmptyTree : BinarySearchTreeBase<K, T>.EmptyTree { 
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary<IKeyLogic<K, T>, System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>> theEmpty;
			private readonly System.Int32 myHashCode;
			private readonly IBinarySearchTree<K, T> myLeft;

			static EmptyTree() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary<IKeyLogic<K, T>, System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>>();
			}
			private EmptyTree( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> left ) : base( keyLogic ) { 
				if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				}

				myLeft = left;
				myHashCode = theHashCode;
				unchecked { 
					myHashCode += left.GetHashCode();
					myHashCode += keyLogic.GetHashCode();
				}
			}

			public sealed override IBinarySearchTree<K, T> Left { 
				get { 
					return myLeft;
				}
			}

			public sealed override System.Boolean IsInBalance( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				if ( 
					other.IsEmpty 
					|| other.IsLeaf 
					|| ( other.Count <= 2 ) 
				) { 
					return true;
				}

				IBinarySearchTree<K, T> probe;
				probe = other.Right;
				if ( !probe.IsEmpty ) { 
					return false;
				}
				probe = other.Left;
				return probe.IsInBalance( probe );
			}
			public sealed override IBinarySearchTree<K, T> BalanceTree( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( this.IsInBalance( other ) ) { 
					return other;
				} else { 
					IBinarySearchTree<K, T> max = other.Max;
					K k = max.Key;
					return GetEmpty( max.Empty ).Add( 
						k, max.Value 
					).Merge( 
						other.BalanceTree( other.Remove( k ) ) 
					);
				}
			}

			public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
				return new MaxTree<K, T>( myLeft, key, item, this.KeyLogic, this );
			}
			public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( other.IsEmpty ) { 
					return Get( this.KeyLogic, other );
				} else { 
					IBinarySearchTree<K, T> max = other.Max;
					K k = max.Key;
					return new MaxTree<K, T>( 
						this, 
						k, max.Value, this.KeyLogic, 
						myLeft.Merge( other.BalanceTree( other.Remove( k ) ) ) 
					);
				}
			}

			public sealed override System.Int32 GetHashCode() { 
				return myHashCode;
			}

			public static IBinarySearchTree<K, T> Get() { 
				return Get( KeyLogic<K, T>.Default );
			}
			public static IBinarySearchTree<K, T> Get( System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				}
				return Get( KeyLogic<K, T>.Get( keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( IBinarySearchTree<K, T> left ) { 
				if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				} else if ( null == left.KeyLogic ) { 
					throw new System.ArgumentException( null, "left" );
				} else { 
					return Get( left.KeyLogic, left );
				}
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator ) { 
				if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}
				return Get( KeyLogic<K, T>.Get( generator ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, IBinarySearchTree<K, T> left ) { 
				if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return GetEmpty( KeyLogic<K, T>.Get( generator ), left );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				IKeyLogic<K, T> logic = KeyLogic<K, T>.Get( generator, comparer );
				return Get( logic, AvlTree<K, T>.GetEmpty( logic ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> comparer, IBinarySearchTree<K, T> left ) { 
				if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				} else if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return Get( KeyLogic<K, T>.Get( generator, comparer ), left );
			}
			public static IBinarySearchTree<K, T> Get( IKeyLogic<K, T> keyLogic ) { 
				if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				}

				return Get( keyLogic, AvlTree<K, T>.GetEmpty( keyLogic ) );
			}
			public static IBinarySearchTree<K, T> Get( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> left ) { 
				if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				} else if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				}

				IBinarySearchTree<K, T> empty;
				System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>> branch;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( keyLogic ) ) { 
						branch = theEmpty[ keyLogic ];
					} else { 
						branch = new System.Collections.Generic.Dictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>();
						theEmpty.Add( keyLogic, branch );
					}
				}
				lock ( branch ) { 
					if ( branch.ContainsKey( left ) ) { 
						empty = branch[ left ];
					} else { 
						empty = new EmptyTree( keyLogic, left );
						branch.Add( left, empty );
					}
				}
				return empty;
			}

		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;
		private static readonly BinarySearchTreeCtor<K, T> MaxCtor;

		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static MaxTree() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
			MaxCtor = delegate( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) {
				return new MaxTree<K, T>( left, root, right );
			};
		}

		private MaxTree( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) : base( left, root, right ) { 
			if ( !right.IsEmpty ) { 
				throw new System.ArgumentException( null, "right" );
			}

			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		private MaxTree( IBinarySearchTree<K, T> left, K key, T value, IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> right ) : base( left, key, value, keyLogic, right ) { 
			if ( !right.IsEmpty ) { 
				throw new System.ArgumentException( null, "right" );
			}

			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public sealed override IBinarySearchTree<K, T> Empty { 
			get { 
				return GetEmpty( this.KeyLogic );
			}
		}
		#endregion properties


		#region methods
		public sealed override IBinarySearchTree<K, T> Add( K key, T item ) {
			System.Int32 c = this.KeyLogic.Comparer.Compare( this.Key, key );
			if ( c < 0 ) { 
				return new MaxTree<K, T>( 
					this.Left.Add( this.Key, this.Value ), 
					key, item, this.KeyLogic, 
					this.Right 
				);
			} else if ( 0 < c ) { 
				return new MaxTree<K, T>( 
					this.Left.Add( key, item ), 
					this, 
					this.Right 
				);
			} else {
				throw new DuplicateKeyException( "other" );
			}
		}
		public sealed override IBinarySearchTree<K, T> Remove( K key ) { 
			System.Int32 c = this.KeyLogic.Comparer.Compare( this.Key, key );
			if ( c < 0 ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			} else if ( 0 < c ) { 
				return new MaxTree<K, T>( this.Left.Remove( key ), this, this.Right );
			} else { 
				IBinarySearchTree<K, T> max = this.Left.Max;
				K k = max.Key;
				return new MaxTree<K, T>( this.Left.Remove( k ), k, max.Value, this.KeyLogic, this.Right );
			}
		}
		public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			}

			IBinarySearchTree<K, T> empty = this.Empty;
			IBinarySearchTree<K, T> max = other.Max;
			K k = max.Key;
			System.Int32 c = this.KeyLogic.Comparer.Compare( this.Key, k );
			if ( c < 0 ) { 
				return empty.BalanceTree( 
					new MaxTree<K, T>( 
						other.BalanceTree( other.Remove( k ).Add( this.Key, this.Value ) ), 
						k, max.Value, this.KeyLogic, 
						this.Right 
					) 
				);
			} else if ( 0 < c ) { 
				return empty.BalanceTree( 
					new MaxTree<K, T>( 
						this.Left.Merge( other ), 
						this, 
						this.Right 
					) 
				);
			} else {
				throw new DuplicateKeyException( "other" );
			}
		}

		public sealed override IBinarySearchTree<K, T> SetLeft( IBinarySearchTree<K, T> left ) { 
			return this.SetLeft( left, MaxCtor );
		}
		public sealed override IBinarySearchTree<K, T> SetRight( IBinarySearchTree<K, T> right ) { 
			if ( null == right ) { 
				throw new System.ArgumentNullException( "right" );
			} else if ( !right.IsEmpty ) { 
				throw new System.NotSupportedException();
			} else if ( this.Right == right ) { 
				return this;
			} else { 
				return new MaxTree<K, T>( this.Left, this, right );
			}
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static IBinarySearchTree<K, T> GetEmpty() { 
			return EmptyTree.Get();
		}
		public static IBinarySearchTree<K, T> GetEmpty( System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}
			return EmptyTree.Get( keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator ) { 
			if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) {
				throw new System.ArgumentNullException( "keyComparer" );
			} else if ( null == generator ) {
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IBinarySearchTree<K, T> empty ) { 
			if ( null == empty ) { 
				throw new System.ArgumentNullException( "empty" );
			} else if ( !empty.IsEmpty ) { 
				throw new System.ArgumentException( null, "empty" );
			} else if ( null == empty.KeyLogic ) { 
				throw new System.ArgumentException( null, "empty" );
			}

			return EmptyTree.Get( empty );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, IBinarySearchTree<K, T> empty ) { 
			if ( null == empty ) { 
				throw new System.ArgumentNullException( "empty" );
			} else if ( !empty.IsEmpty ) {
				throw new System.ArgumentException( null, "empty" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, empty );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer, IBinarySearchTree<K, T> empty ) { 
			if ( null == empty ) { 
				throw new System.ArgumentNullException( "empty" );
			} else if ( !empty.IsEmpty ) {
				throw new System.ArgumentException( null, "empty" );
			} else if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, keyComparer, empty );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IKeyLogic<K, T> keyLogic ) { 
			if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			}

			return EmptyTree.Get( keyLogic );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> empty ) { 
			if ( null == empty ) { 
				throw new System.ArgumentNullException( "empty" );
			} else if ( !empty.IsEmpty ) { 
				throw new System.ArgumentException( null, "empty" );
			} else if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			}

			return EmptyTree.Get( keyLogic, empty );
		}
		#endregion static methods

	}

}