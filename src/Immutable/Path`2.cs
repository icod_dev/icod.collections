namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class Path<V, W> : IPath<V, W> { 

		#region internal classes
		[System.Serializable]
		[System.Diagnostics.DebuggerDisplay( "Empty" )]
		private sealed class EmptyPath : IPath<V, W> { 
			private static readonly System.Int32 theHashCode;
			private static readonly System.Collections.Generic.IDictionary< 
				System.Collections.Generic.IEqualityComparer<V>, 
				System.Collections.Generic.IDictionary< 
					Func<W, W, W>, IPath<V, W> 
				> 
			> theEmpty;
			private readonly Func<W, W, W> myAdd;
			private readonly System.Collections.Generic.IEqualityComparer<V> myComparer;
			private readonly System.Int32 myHashCode;

			static EmptyPath() {
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked {
					theHashCode += typeof( V ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( W ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary< 
					System.Collections.Generic.IEqualityComparer<V>, 
					System.Collections.Generic.IDictionary< 
						Func<W, W, W>, IPath<V, W> 
					>
				>();
			}
			private EmptyPath() : base() { 
				myHashCode = theHashCode;
			}
			public EmptyPath( Func<W, W, W> add, System.Collections.Generic.IEqualityComparer<V> comparer ) : this() { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				} else if ( null == add ) { 
					throw new System.ArgumentNullException( "add" );
				}
				myAdd = add;
				myComparer = comparer;
				unchecked { 
					myHashCode += myAdd.GetHashCode();
					myHashCode += myComparer.GetHashCode();
				}
			}

			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Empty {
				get {
					return this;
				}
			}
			public IPath<V, W> Empty {
				get {
					return this;
				}
			}
			public System.Boolean IsEmpty {
				get {
					return true;
				}
			}
			public W Weight {
				get {
					return default( W );
				}
			}
			public System.Int32 Count {
				get {
					return 0;
				}
			}
			public System.Boolean IsReadOnly {
				get {
					return true;
				}
			}
			public V Source { 
				get { 
					throw new System.InvalidOperationException();
				}
			}
			public V Destination { 
				get { 
					throw new System.InvalidOperationException();
				}
			}

			public System.Collections.Generic.KeyValuePair<V, W> Peek() {
				throw new System.InvalidOperationException();
			}
			public IPath<V, W> Enqueue( System.Collections.Generic.KeyValuePair<V, W> item ) {
				return this.Enqueue( item.Key, item.Value );
			}
			public IPath<V, W> Enqueue( V vertex, W weight ) { 
				return new Path<V, W>( Queue<V>.GetEmpty().Enqueue( vertex ), Stack<Pair<W>>.GetEmpty().Push( new Pair<W>( weight, weight ) ), myAdd, myComparer, vertex );
			}
			public IPath<V, W> Dequeue() {
				throw new System.InvalidOperationException();
			}
			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Reverse() {
				return this;
			}
			public IPath<V, W> Reverse() { 
				return this;
			}
			public System.Boolean Contains( System.Collections.Generic.KeyValuePair<V, W> item ) {
				return false;
			}
			public System.Boolean Contains( System.Collections.Generic.KeyValuePair<V, W> item, System.Collections.Generic.IEqualityComparer<System.Collections.Generic.KeyValuePair<V, W>> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				return false;
			}
			public System.Boolean Contains( V vertex, W weight ) {
				return false;
			}
			public System.Boolean Contains( V vertex ) { 
				return false;
			}
			public System.Boolean Contains( V vertex, System.Collections.Generic.IEqualityComparer<V> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				return false;
			}

			public System.Boolean Remove( System.Collections.Generic.KeyValuePair<V, W> item ) {
				throw new System.NotSupportedException();
			}
			System.Collections.Generic.KeyValuePair<V, W> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Peek() {
				return this.Peek();
			}
			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Enqueue( System.Collections.Generic.KeyValuePair<V, W> item ) {
				return this.Enqueue( item );
			}
			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Dequeue() {
				return this.Dequeue();
			}
			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Duplicate() {
				throw new System.NotSupportedException();
			}
			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Exchange() {
				throw new System.NotSupportedException();
			}
			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Rotate( System.Int32 shift ) {
				throw new System.NotSupportedException();
			}
			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Rotate( System.Int32 count, System.Int32 shift ) {
				throw new System.NotSupportedException();
			}
			IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Copy( System.Int32 count ) {
				throw new System.NotSupportedException();
			}
			public void CopyTo( System.Collections.Generic.KeyValuePair<V, W>[] array, System.Int32 arrayIndex ) {
				throw new System.NotSupportedException();
			}
			public void Add( System.Collections.Generic.KeyValuePair<V, W> item ) {
				throw new System.NotSupportedException();
			}
			public void Clear() {
				throw new System.NotSupportedException();
			}


			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
				yield break;
			}
			System.Collections.Generic.IEnumerator<V> System.Collections.Generic.IEnumerable<V>.GetEnumerator() {
				yield break;
			}
			System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.GetEnumerator() {
				yield break;
			}
			public System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<V, W>> GetEnumerator() {
				yield break;
			}

			public sealed override System.Int32 GetHashCode() {
				return myHashCode;
			}


			public static IPath<V, W> Get( Func<W, W, W> add ) { 
				if ( null == add ) { 
					throw new System.ArgumentNullException( "add" );
				}
				return Get( add, System.Collections.Generic.EqualityComparer<V>.Default );
			}
			public static IPath<V, W> Get( Func<W, W, W> add, System.Collections.Generic.IEqualityComparer<V> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				} else if ( null == add ) { 
					throw new System.ArgumentNullException( "add" );
				}
				IPath<V, W> output = null;
				System.Collections.Generic.IDictionary<Func<W, W, W>, IPath<V, W>> branch;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( comparer ) ) { 
						branch = theEmpty[ comparer ];
					} else { 
						branch = new System.Collections.Generic.Dictionary<Func<W, W, W>, IPath<V, W>>();
						theEmpty.Add( comparer, branch );
					}
				}
				lock ( branch ) { 
					if ( branch.ContainsKey( add ) ) { 
						output = branch[ add ];
					} else { 
						output = new EmptyPath( add, comparer );
						branch.Add( add, output );
					}
				}
				return output;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;

		private IPath<V, W> myEmpty;
		private readonly IQueue<V> myVertexes;
		private readonly IStack<Pair<W>> myWeights;
		private readonly Func<W, W, W> myAdd;
		private readonly System.Collections.Generic.IEqualityComparer<V> myComparer;
		private readonly V myDestination;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static Path() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( V ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( W ).AssemblyQualifiedName.GetHashCode();
			}
		}

		private Path() : base() { 
			myHashCode = theHashCode;
			myEmpty = null;
		}
		private Path( IQueue<V> vertexes, IStack<Pair<W>> weights, Func<W, W, W> add, System.Collections.Generic.IEqualityComparer<V> comparer, V destination ) : this() { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			} else if ( null == add ) { 
				throw new System.ArgumentNullException( "add" );
			} else if ( ( null == weights ) || ( weights.IsEmpty ) ) { 
				throw new System.ArgumentNullException( "weights" );
			} else if ( ( null == vertexes ) || ( vertexes.IsEmpty ) ) { 
				throw new System.ArgumentNullException( "vertexes" );
			} else if ( vertexes.Count != weights.Count ) { 
				throw new System.ArgumentOutOfRangeException( "weights", "weights parameter must contain exactly the same number of elements as vertexes parameter." );
			}

			myVertexes = vertexes;
			myWeights = weights;
			myAdd = add;
			myComparer = comparer;
			myDestination = destination;
			unchecked { 
				myHashCode += myVertexes.GetHashCode();
				myHashCode += myWeights.GetHashCode();
				myHashCode += myAdd.GetHashCode();
				myHashCode += myComparer.GetHashCode();
			}
		}
		#endregion .ctor


		#region properties
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Empty { 
			get { 
				return this.Empty;
			}
		}
		public IPath<V, W> Empty { 
			get { 
				if ( null == myEmpty ) { 
					System.Threading.Interlocked.CompareExchange<IPath<V, W>>( 
						ref myEmpty, 
						GetEmpty( myAdd, myComparer ), 
						null 
					);
				}
				return myEmpty;
			}
		}
		public System.Boolean IsEmpty { 
			get { 
				return false;
			}
		}
		public W Weight { 
			get { 
				return myWeights.Peek().First;
			}
		}
		public System.Int32 Count { 
			get { 
				return myVertexes.Count;
			}
		}
		public System.Boolean IsReadOnly { 
			get { 
				return true;
			}
		}
		public V Source { 
			get { 
				return myVertexes.Peek();
			}
		}
		public V Destination { 
			get { 
				return myDestination;
			}
		}
		#endregion properties


		#region methods
		public System.Collections.Generic.KeyValuePair<V, W> Peek() { 
			return new System.Collections.Generic.KeyValuePair<V,W>( 
				myVertexes.Peek(), 
				myWeights.Peek().Second 
			);
		}
		public IPath<V, W> Enqueue( System.Collections.Generic.KeyValuePair<V, W> item ) { 
			return this.Enqueue( item.Key, item.Value );
		}
		public IPath<V, W> Enqueue( V vertex, W weight ) { 
			Pair<W> top = myWeights.Peek();
			return new Path<V, W>( 
				myVertexes.Enqueue( vertex ), 
				myWeights.Push( new Pair<W>( myAdd( top.First, weight ), weight ) ), 
				myAdd, 
				myComparer, 
				vertex 
			);
		}
		public IPath<V, W> Dequeue() { 
			IQueue<V> vertexes = myVertexes.Dequeue();
			if ( vertexes.IsEmpty ) { 
				return this.Empty;
			} else { 
				return new Path<V, W>( vertexes, myWeights.Pop(), myAdd, myComparer, myDestination );
			}
		}
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Reverse() { 
			return this.Reverse();
		}
		public IPath<V, W> Reverse() { 
			throw new System.NotImplementedException();
		}
		public System.Boolean Contains( System.Collections.Generic.KeyValuePair<V, W> item ) { 
			V v = item.Key;
			W w = item.Value;
			IQueue<V> vertexes = myVertexes;
			IStack<Pair<W>> weights = myWeights;
			System.Collections.Generic.IEqualityComparer<W> wComparer = System.Collections.Generic.EqualityComparer<W>.Default;
			while ( !vertexes.IsEmpty ) { 
				if ( myComparer.Equals( vertexes.Peek(), v ) && wComparer.Equals( weights.Peek().Second, w ) ) { 
					return true;
				}
				vertexes = vertexes.Dequeue();
				weights = weights.Pop();
			}
			return false;
		}
		public System.Boolean Contains( System.Collections.Generic.KeyValuePair<V, W> item, System.Collections.Generic.IEqualityComparer<System.Collections.Generic.KeyValuePair<V, W>> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			foreach ( System.Collections.Generic.KeyValuePair<V, W> kvp in this ) { 
				if ( comparer.Equals( kvp, item ) ) { 
					return true;
				}
			}
			return false;
		}
		public System.Boolean Contains( V vertex, W weight ) { 
			return this.Contains( new System.Collections.Generic.KeyValuePair<V, W>( vertex, weight ) );
		}
		public System.Boolean Contains( V vertex ) { 
			return this.Contains( vertex, myComparer );
		}
		public System.Boolean Contains( V vertex, System.Collections.Generic.IEqualityComparer<V> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			foreach ( V v in myVertexes ) { 
				if ( comparer.Equals( v, vertex ) ) { 
					return true;
				}
			}
			return false;
		}


		public System.Boolean Remove( System.Collections.Generic.KeyValuePair<V, W> item ) { 
			throw new System.NotSupportedException();
		}
		System.Collections.Generic.KeyValuePair<V, W> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Peek() {
			return this.Peek();
		}
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Enqueue( System.Collections.Generic.KeyValuePair<V, W> item ) {
			return this.Enqueue( item );
		}
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Dequeue() {
			return this.Dequeue();
		}
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Duplicate() {
			throw new System.NotSupportedException();
		}
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Exchange() {
			throw new System.NotSupportedException();
		}
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Rotate( System.Int32 shift ) {
			throw new System.NotSupportedException();
		}
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Rotate( System.Int32 count, System.Int32 shift ) {
			throw new System.NotSupportedException();
		}
		IQueue<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.Copy( System.Int32 count ) {
			throw new System.NotSupportedException();
		}
		public void CopyTo( System.Collections.Generic.KeyValuePair<V, W>[] array, System.Int32 arrayIndex ) {
			throw new System.NotSupportedException();
		}
		public void Add( System.Collections.Generic.KeyValuePair<V, W> item ) {
			throw new System.NotSupportedException();
		}
		public void Clear() {
			throw new System.NotSupportedException();
		}


		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { 
			return this.GetEnumerator();
		}
		System.Collections.Generic.IEnumerator<V> System.Collections.Generic.IEnumerable<V>.GetEnumerator() { 
			return myVertexes.GetEnumerator();
		}
		System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<V, W>> IQueue<System.Collections.Generic.KeyValuePair<V, W>>.GetEnumerator() { 
			return this.GetEnumerator();
		}
		public System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<V, W>> GetEnumerator() { 
			IQueue<V> vertexes = myVertexes;
			IStack<Pair<W>> weights = myWeights;
			while ( !vertexes.IsEmpty ) { 
				yield return new System.Collections.Generic.KeyValuePair<V, W>( vertexes.Peek(), weights.Peek().Second );
				vertexes = vertexes.Dequeue();
				weights = weights.Pop();
			}
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static IPath<V, W> GetEmpty( Func<W, W, W> add, System.Collections.Generic.IEqualityComparer<V> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			} else if ( null == add ) {
				throw new System.ArgumentNullException( "add" );
			}
			return EmptyPath.Get( add, comparer );
		}
		public static IPath<V, W> Create( Func<W, W, W> add, System.Collections.Generic.IEqualityComparer<V> comparer, V vertex ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			} else if ( null == add ) { 
				throw new System.ArgumentNullException( "add" );
			}
			return EmptyPath.Get( add, comparer ).Enqueue( vertex, default( W ) );
		}
		#endregion static methods

	}

}