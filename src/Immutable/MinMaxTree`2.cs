namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class MinMaxTree<K, T> : BinarySearchTreeBase<K, T> { 

		#region internal classes
		new sealed private class EmptyTree : BinarySearchTreeBase<K, T>.EmptyTree { 
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary< 
				IKeyLogic<K, T>, 
				System.Collections.Generic.IDictionary< 
					IBinarySearchTree<K, T>, 
					System.Collections.Generic.IDictionary< 
						IBinarySearchTree<K, T>, 
						IBinarySearchTree<K, T> 
					>
				>
			> theEmpty;
			private readonly System.Int32 myHashCode = 0;
			private readonly IBinarySearchTree<K, T> myLeft;
			private readonly IBinarySearchTree<K, T> myRight;

			static EmptyTree() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary< 
					IKeyLogic<K, T>, 
					System.Collections.Generic.IDictionary< 
						IBinarySearchTree<K, T>, 
						System.Collections.Generic.IDictionary< 
							IBinarySearchTree<K, T>, 
							IBinarySearchTree<K, T> 
						>
					>
				>();
			}
			private EmptyTree( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> right ) : base( keyLogic ) { 
				if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				} else if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				}

				myRight = right;
				myLeft = left;

				myHashCode = theHashCode;
				unchecked { 
					myHashCode += keyLogic.GetHashCode();
					myHashCode += myRight.GetHashCode();
					myHashCode += myLeft.GetHashCode();
				}
			}

			public sealed override IBinarySearchTree<K, T> Left { 
				get { 
					return myLeft;
				}
			}
			public sealed override IBinarySearchTree<K, T> Right { 
				get { 
					return myRight;
				}
			}

			public sealed override System.Boolean IsInBalance( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				if ( 
					other.IsEmpty 
					|| other.IsLeaf 
					|| ( other.Count <= 2 ) 
				) { 
					return true;
				}

				IBinarySearchTree<K, T> left = other.Left;
				IBinarySearchTree<K, T> right = other.Right;
				if ( 
					( 
						( ( left.Height << 1 ) < right.Height ) 
						|| ( ( right.Height << 1 ) < left.Height ) 
					) 
					|| ( !left.IsInBalance( left ) || !right.IsInBalance( right ) ) 
				) { 
					return  false;
				} else { 
					return true;
				}
			}
			public sealed override IBinarySearchTree<K, T> BalanceTree( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				if ( 
					other.IsEmpty 
					|| other.IsLeaf 
					|| other.IsPerfect 
					|| ( other.Count <= 2 ) 
					|| this.IsInBalance( other ) 
				) { 
					return other;
				}

				IBinarySearchTree<K, T> left = other.Left;
				IBinarySearchTree<K, T> right = other.Right;
				if ( !left.IsInBalance( left ) || !right.IsInBalance( right ) ) { 
					return this.BalanceTree( new MinMaxTree<K, T>( 
						left.BalanceTree( left ), 
						other, 
						right.BalanceTree( right ) 
					) );
				}

				System.Int32 error = ComputeError( other );
				while ( 0 != error ) { 
					other = Migrate( other, error );
					error = ComputeError( other );
				}

				return other;
			}
			private static System.Int32 ComputeError( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}

				IBinarySearchTree<K, T> left = other.Left;
				IBinarySearchTree<K, T> right = other.Right;
				System.Int32 lh = left.Height;
				System.Int32 l = lh << 1;
				System.Int32 rh = right.Height;
				System.Int32 r = rh << 1;
				System.Int32 e = 0;
				if ( l < rh ) { 
					e = -( rh - lh );
				} else if ( r < lh ) { 
					e = ( lh - rh );
				}

				return ( e >> 1 );
			}
			private static IBinarySearchTree<K, T> Migrate( IBinarySearchTree<K, T> other, System.Int32 error ) { 
				if ( ( null == other ) || ( other.IsEmpty ) ) { 
					throw new System.ArgumentNullException( "other" );
				}

				IBinarySearchTree<K, T> left;
				Pair<K, T> mid;
				IBinarySearchTree<K, T> right;
				Triplet<IBinarySearchTree<K, T>, Pair<K, T>, IBinarySearchTree<K, T>> trip;
				if ( error < 0 ) { 
					trip = MigrateLeftToRight( other, error );
				} else if ( 0 < error ) { 
					trip = MigrateRightToLeft( other, error );
				} else { 
					return other;
				}
				left = trip.First;
				mid = trip.Second;
				right = trip.Third;
				return new MinMaxTree<K, T>( left, mid.First, mid.Second, other.KeyLogic, right );
			}
			private static Triplet<IBinarySearchTree<K, T>, Pair<K, T>, IBinarySearchTree<K, T>> MigrateLeftToRight( IBinarySearchTree<K, T> other, System.Int32 error ) { 
				if ( 0 < error ) { 
					throw new System.ArgumentOutOfRangeException( "error" );
				} else if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}

				IBinarySearchTree<K, T> left = other.Left;
				IBinarySearchTree<K, T> right = other.Right;
				Pair<K, T> mid = new Pair<K,T>( other.Key, other.Value );
				if ( 0 == error ) { 
					return new Triplet<IBinarySearchTree<K, T>, Pair<K, T>, IBinarySearchTree<K, T>>( 
						left, 
						mid, 
						right 
					);
				}

				IBinarySearchTree<K, T> current = left;
				IBinarySearchTree<K, T> snip;
				IBinarySearchTree<K, T> parent;
				IStack<IBinarySearchTree<K, T>> branch = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( current );
				error = -error;
				while ( error < current.Height ) { 
					current = current.Right;
					branch = branch.Push( current );
				}
				if ( current.Height < error ) { 
					branch = branch.Pop();
					current = branch.Peek();
				}
				snip = current.SetLeft( current.Empty );
				IBinarySearchTree<K, T> m = snip.Max;
				K mk = m.Key;
				snip = snip.Remove( mk ).Add( mid.First, mid.Second );
				mid = new Pair<K,T>( mk, m.Value );
				branch = branch.Pop().Push( current.Left );
				while ( 1 < branch.Count ) { 
					current = branch.Peek();
					branch = branch.Pop();
					parent = branch.Peek().SetRight( current.BalanceTree( current ) );
					branch = branch.Pop().Push( parent.BalanceTree( parent ) );
				}
				left = branch.Peek();

				current = right;
				branch = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( current );
				while ( !current.IsEmpty ) { 
					current = current.Left;
					branch = branch.Push( current );
				}
				branch = branch.Pop().Push( snip );
				while ( 1 < branch.Count ) { 
					current = branch.Peek();
					branch = branch.Pop();
					parent = branch.Peek().SetLeft( current.BalanceTree( current ) );
					branch = branch.Pop().Push( parent.BalanceTree( parent ) );
				}
				right = branch.Peek();

				return new Triplet<IBinarySearchTree<K, T>, Pair<K, T>, IBinarySearchTree<K, T>>( 
					left, 
					mid, 
					right 
				);
			}
			private static Triplet<IBinarySearchTree<K, T>, Pair<K, T>, IBinarySearchTree<K, T>> MigrateRightToLeft( IBinarySearchTree<K, T> other, System.Int32 error ) { 
				if ( error < 0 ) { 
					throw new System.ArgumentOutOfRangeException( "error" );
				} else if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}

				IBinarySearchTree<K, T> left = other.Left;
				IBinarySearchTree<K, T> right = other.Right;
				Pair<K, T> mid = new Pair<K, T>( other.Key, other.Value );
				if ( 0 == error ) { 
					return new Triplet<IBinarySearchTree<K,T>,Pair<K,T>,IBinarySearchTree<K,T>>( 
						left, 
						mid, 
						right 
					);
				}

				IBinarySearchTree<K, T> current = right;
				IBinarySearchTree<K, T> snip;
				IBinarySearchTree<K, T> parent;
				IStack<IBinarySearchTree<K, T>> branch = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( current );
				while ( error < current.Height ) { 
					current = current.Left;
					branch = branch.Push( current );
				}
				if ( current.Height < error ) { 
					branch = branch.Pop();
					current = branch.Peek();
				}
				snip = current.SetRight( current.Empty );
				IBinarySearchTree<K, T> m = snip.Min;
				K mk = m.Key;
				snip = snip.Remove( mk ).Add( mid.First, mid.Second );
				mid = new Pair<K, T>( mk, m.Value );
				branch = branch.Pop().Push( current.Right );
				while ( 1 < branch.Count ) { 
					current = branch.Peek();
					branch = branch.Pop();
					parent = branch.Peek().SetRight( current.BalanceTree( current ) );
					branch = branch.Pop().Push( parent.BalanceTree( parent ) );
				}
				right = branch.Peek();

				current = left;
				branch = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( current );
				while ( !current.IsEmpty ) { 
					current = current.Right;
					branch = branch.Push( current );
				}
				branch = branch.Pop().Push( snip );
				while ( 1 < branch.Count ) { 
					current = branch.Peek();
					branch = branch.Pop();
					parent = branch.Peek().SetRight( current.BalanceTree( current ) );
					branch = branch.Push( parent.BalanceTree( parent ) );
				}
				left = branch.Peek();

				return new Triplet<IBinarySearchTree<K,T>,Pair<K,T>,IBinarySearchTree<K,T>>( 
					left, 
					mid, 
					right 
				);
			}

			public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
				return new MinMaxTree<K, T>( myLeft, key, item, this.KeyLogic, myRight );
			}
			public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}

				IBinarySearchTree<K, T> left = other.Left;
				if ( left.IsEmpty ) { 
					left = myLeft;
				} else { 
					left = myLeft.Merge( left );
				}
				IBinarySearchTree<K, T>  right = other.Right;
				if ( right.IsEmpty ) { 
					right = myRight;
				} else { 
					right = myRight.Merge( right );
				}

				return new MinMaxTree<K, T>( 
					left, 
					other.Key, other.Value, this.KeyLogic, 
					right 
				);
			}


			public sealed override System.Int32 GetHashCode() { 
				return myHashCode;
			}


			public static IBinarySearchTree<K, T> Get() { 
				return Get( KeyLogic<K, T>.Default );
			}
			public static IBinarySearchTree<K, T> Get( System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				}
				return Get( KeyLogic<K, T>.Get( keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator ) { 
				if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return Get( KeyLogic<K, T>.Get( generator ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return Get( KeyLogic<K, T>.Get( generator, keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( IKeyLogic<K, T> keyLogic ) { 
				if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				}

				return Get( keyLogic, MinTree<K, T>.GetEmpty( keyLogic ), MaxTree<K, T>.GetEmpty( keyLogic ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> right ) { 
				if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				} else if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return Get( KeyLogic<K, T>.Get( generator ), left, right );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer, IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> right ) { 
				if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				} else if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				} else if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return Get( KeyLogic<K, T>.Get( generator, keyComparer ), left, right );
			}
			public static IBinarySearchTree<K, T> Get( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> right ) { 
				if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				} else if ( null == left ) { 
					throw new System.ArgumentNullException( "left" );
				} else if ( !left.IsEmpty ) { 
					throw new System.ArgumentException( null, "left" );
				} else if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				}

				IBinarySearchTree<K, T> empty;
				System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>> minMax;
				System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>> max;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( keyLogic ) ) { 
						minMax = theEmpty[ keyLogic ];
					} else { 
						minMax = new System.Collections.Generic.Dictionary<IBinarySearchTree<K, T>, System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>>();
						theEmpty.Add( keyLogic, minMax );
					}
				}
				lock ( minMax ) { 
					if ( minMax.ContainsKey( left ) ) { 
						max = minMax[ left ];
					} else { 
						max = new System.Collections.Generic.Dictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>();
						minMax.Add( left, max );
					}
				}
				lock ( max ) { 
					if ( max.ContainsKey( right ) ) { 
						empty = max[ right ];
					} else { 
						empty = new EmptyTree( keyLogic, left, right );
						max.Add( right, empty );
					}
				}
				return empty;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;
		private static readonly BinarySearchTreeCtor<K, T> MinMaxCtor;

		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static MinMaxTree() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
			MinMaxCtor = delegate( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) { 
				return new MinMaxTree<K, T>( left, root, right );
			};
		}

		private MinMaxTree( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) : base( left, root, right ) { 
			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		private MinMaxTree( IBinarySearchTree<K, T> left, K key, T value, IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> right ) : base( left, key, value, keyLogic, right ) { 
			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public sealed override IBinarySearchTree<K, T> Empty { 
			get { 
				return GetEmpty( this.KeyLogic, this.Left.Empty, this.Right.Empty );
			}
		}
		public sealed override IBinarySearchTree<K, T> Min { 
			get { 
				if ( this.Left.IsEmpty ) { 
					return this;
				} else { 
					return this.Left.Min;
				}
			}
		}
		public sealed override IBinarySearchTree<K, T> Max { 
			get { 
				if ( this.Right.IsEmpty ) { 
					return this;
				} else { 
					return this.Right.Max;
				}
			}
		}
		#endregion properties


		#region methods
		public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
			System.Int32 c = this.KeyLogic.Comparer.Compare( this.Key, key );
			IBinarySearchTree<K, T> output;
			if ( c < 0 ) { 
				output = new MinMaxTree<K, T>( 
					this.Left, 
					this, 
					this.Right.BalanceTree( this.Right.Add( key, item ) ) 
				);
			} else if ( 0 < c ) { 
				output = new MinMaxTree<K, T>( 
					this.Left.BalanceTree( this.Left.Add( key, item ) ), 
					this, 
					this.Right 
				);
			} else {
				throw new DuplicateKeyException( "key" );
			}

			return this.BalanceTree( output );
		}
		public sealed override IBinarySearchTree<K, T> Remove( K key ) { 
			IBinarySearchTree<K, T> output;
			System.Int32 c = this.KeyLogic.Comparer.Compare( this.Key, key );
			if ( c < 0 ) { 
				output = new MinMaxTree<K, T>( 
					this.Left.Remove( key ), 
					this, 
					this.Right 
				);
			} else if ( 0 < c ) { 
				output = new MinMaxTree<K, T>( 
					this.Left, 
					this, 
					this.Right.Remove( key ) 
				);
			} else { 
				IBinarySearchTree<K, T> mid;
				K k;
				IBinarySearchTree<K, T> left = this.Left;
				IBinarySearchTree<K, T> right = this.Right;
				if ( this.Right.Height < this.Left.Height ) { 
					mid = left.Max;
					k = mid.Key;
					left = left.Remove( k );
				} else { 
					mid = right.Min;
					k = mid.Key;
					right = right.Remove( k );
				}
				output = new MinMaxTree<K, T>( 
					left, 
					k, mid.Value, this.KeyLogic, 
					right 
				);
			}
			return this.BalanceTree( output );
		}
		public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			}

			Pair<IBinarySearchTree<K, T>> split = Split( other, this.Key );
			IBinarySearchTree<K, T> left = split.First;
			if ( ( null != left ) && ( !left.IsEmpty ) ) { 
				left = this.Left.Merge( left );
			}
			IBinarySearchTree<K, T> right = split.Second;
			if ( ( null != right ) && ( !right.IsEmpty ) ) { 
				right = this.Right.Merge( right );
			}

			IBinarySearchTree<K, T> output = this;
			if ( ( this.Left != left ) || ( this.Right != right ) ) { 
				output = this.BalanceTree( new MinMaxTree<K, T>( 
					left.BalanceTree( left ), this, right.BalanceTree( right ) 
				) );
			}

			return output;
		}

		public sealed override IBinarySearchTree<K, T> SetLeft( IBinarySearchTree<K, T> left ) { 
			return this.SetLeft( left, MinMaxCtor );
		}
		public sealed override IBinarySearchTree<K, T> SetRight( IBinarySearchTree<K, T> right ) { 
			return this.SetRight( right, MinMaxCtor );
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static IBinarySearchTree<K, T> GetEmpty() { 
			return EmptyTree.Get();
		}
		public static IBinarySearchTree<K, T> GetEmpty( System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}
			return EmptyTree.Get( keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator ) { 
			if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> right ) { 
			if ( null == right ) { 
				throw new System.ArgumentNullException( "right" );
			} else if ( !right.IsEmpty ) { 
				throw new System.ArgumentException( null, "right" );
			} else if ( null == left ) { 
				throw new System.ArgumentNullException( "left" );
			} else if ( !left.IsEmpty ) { 
				throw new System.ArgumentException( null, "left" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, left, right );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer, IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> right ) { 
			if ( null == right ) { 
				throw new System.ArgumentNullException( "right" );
			} else if ( !right.IsEmpty ) { 
				throw new System.ArgumentException( null, "right" );
			} else if ( null == left ) { 
				throw new System.ArgumentNullException( "left" );
			} else if ( !left.IsEmpty ) { 
				throw new System.ArgumentException( null, "left" );
			} else if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, keyComparer, left, right );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IKeyLogic<K, T> keyLogic ) { 
			if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			}

			return EmptyTree.Get( keyLogic );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> right ) { 
			if ( null == right ) { 
				throw new System.ArgumentNullException( "right" );
			} else if ( !right.IsEmpty ) { 
				throw new System.ArgumentException( null, "right" );
			} else if ( null == left ) { 
				throw new System.ArgumentNullException( "left" );
			} else if ( !left.IsEmpty ) { 
				throw new System.ArgumentException( null, "left" );
			} else if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			}

			return EmptyTree.Get( keyLogic, left, right );
		}
		#endregion static methods

	}

}