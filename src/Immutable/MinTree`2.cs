namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class MinTree<K, T> : BinarySearchTreeBase<K, T> { 

		#region internal classes
		new private sealed class EmptyTree : BinarySearchTreeBase<K, T>.EmptyTree { 
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary<IKeyLogic<K, T>, System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>> theEmpty;
			private readonly System.Int32 myHashCode;
			private readonly IBinarySearchTree<K, T> myRight;

			static EmptyTree() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary<IKeyLogic<K, T>, System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>>();
			}
			private EmptyTree( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> right ) : base( keyLogic ) { 
				if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				}

				myRight = right;
				myHashCode = theHashCode;
				unchecked { 
					myHashCode += keyLogic.GetHashCode();
					myHashCode += myRight.GetHashCode();
				}
			}

			public sealed override IBinarySearchTree<K, T> Right { 
				get { 
					return myRight;
				}
			}

			public sealed override System.Boolean IsInBalance( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				if ( 
					other.IsEmpty 
					|| other.IsLeaf 
					|| ( other.Count <= 2 ) 
				) { 
					return true;
				}

				IBinarySearchTree<K, T> probe;
				probe = other.Left;
				if ( !probe.IsEmpty ) { 
					return false;
				}
				probe = other.Right;
				return probe.IsInBalance( probe );
			}
			public sealed override IBinarySearchTree<K, T> BalanceTree( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( this.IsInBalance( other ) ) { 
					return other;
				} else { 
					IBinarySearchTree<K, T> min = other.Min;
					K k = min.Key;
					return GetEmpty( min.Empty ).Add( 
						k, min.Value 
					).Merge( 
						other.BalanceTree( other.Remove( k ) ) 
					);
				}
			}

			public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
				return new MinTree<K, T>( this, key, item, this.KeyLogic, myRight );
			}
			public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( other.IsEmpty ) { 
					return Get( this.KeyLogic, other );
				} else { 
					IBinarySearchTree<K, T> min = other.Min;
					K k = min.Key;
					return new MinTree<K, T>( 
						this, 
						k, min.Value, this.KeyLogic, 
						myRight.Merge( other.BalanceTree( other.Remove( k ) ) ) 
					);
				}
			}

			public sealed override System.Int32 GetHashCode() { 
				return myHashCode;
			}

			public static IBinarySearchTree<K, T> Get() { 
				return Get( KeyLogic<K, T>.Default );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator ) { 
				if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}
				return Get( KeyLogic<K, T>.Get( generator ) );
			}
			public static IBinarySearchTree<K, T> Get( System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				}
				return Get( KeyLogic<K, T>.Get( keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( IBinarySearchTree<K, T> right ) { 
				if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				} else if ( null == right.KeyLogic ) { 
					throw new System.ArgumentException( null, "right" );
				}
				return Get( right.KeyLogic, right );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, IBinarySearchTree<K, T> right ) { 
				if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return GetEmpty( KeyLogic<K, T>.Get( generator ), right );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return Get( KeyLogic<K, T>.Get( generator, keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer, IBinarySearchTree<K, T> right ) { 
				if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				} else if ( null == keyComparer ) {
					throw new System.ArgumentNullException( "keyComparer" );
				} else if ( null == generator ) { 
					throw new System.ArgumentNullException( "generator" );
				}

				return Get( KeyLogic<K, T>.Get( generator, keyComparer ), right );
			}
			public static IBinarySearchTree<K, T> Get( IKeyLogic<K, T> keyLogic ) { 
				if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				}

				return Get( keyLogic, AvlTree<K, T>.GetEmpty( keyLogic ) );
			}
			public static IBinarySearchTree<K, T> Get( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> right ) { 
				if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				} else if ( null == right ) { 
					throw new System.ArgumentNullException( "right" );
				} else if ( !right.IsEmpty ) { 
					throw new System.ArgumentException( null, "right" );
				}

				IBinarySearchTree<K, T> empty;
				System.Collections.Generic.IDictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>> branch;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( keyLogic ) ) { 
						branch = theEmpty[ keyLogic ];
					} else { 
						branch = new System.Collections.Generic.Dictionary<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>>();
						theEmpty.Add( keyLogic, branch );
					}
				}
				lock ( branch ) { 
					if ( branch.ContainsKey( right ) ) { 
						empty = branch[ right ];
					} else { 
						empty = new EmptyTree( keyLogic, right );
						branch.Add( right, empty );
					}
				}
				return empty;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;
		private static readonly BinarySearchTreeCtor<K, T> MinCtor;

		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static MinTree() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
			MinCtor = delegate( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) { 
				return new MinTree<K, T>( left, root, right );
			};
		}

		private MinTree( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) : base( left, root, right ) { 
			if ( !left.IsEmpty ) { 
				throw new System.ArgumentException( null, "left" );
			}

			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		private MinTree( IBinarySearchTree<K, T> left, K key, T value, IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> right ) : base( left, key, value, keyLogic, right ) { 
			if ( !left.IsEmpty ) { 
				throw new System.ArgumentException( null, "left" );
			}

			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public sealed override IBinarySearchTree<K, T> Empty { 
			get { 
				return GetEmpty( this.KeyLogic, this.Right.Empty );
			}
		}
		#endregion properties


		#region methods
		public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
			System.Int32 c = this.KeyLogic.Comparer.Compare( this.Key, key );
			if ( c < 0 ) { 
				return new MinTree<K, T>( 
					this.Left, 
					this, 
					this.Right.Add( key, item ) 
				);
			} else if ( 0 < c ) { 
				return new MinTree<K, T>( 
					this.Left, 
					key, item, this.KeyLogic, 
					this.Right.Add( this.Key, this.Value ) 
				);
			} else {
				throw new DuplicateKeyException( "other" );
			}
		}
		public sealed override IBinarySearchTree<K, T> Remove( K key ) { 
			System.Int32 c = this.KeyLogic.Comparer.Compare( this.Key, key );
			if ( c < 0 ) { 
				return new MinTree<K, T>( this.Left, this, this.Right.Remove( key ) );
			} else if ( 0 < c ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			} else { 
				IBinarySearchTree<K, T> min = this.Right.Min;
				K k = min.Key;
				return new MinTree<K, T>( this.Left, k, min.Value, this.KeyLogic, this.Right.Remove( k ) );
			}
		}
		public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			}

			IBinarySearchTree<K, T> empty = this.Empty;
			IBinarySearchTree<K, T> min = other.Min;
			K k = min.Key;
			System.Int32 c = this.KeyLogic.Comparer.Compare( this.Key, k );
			if ( c < 0 ) { 
				return empty.BalanceTree( 
					new MinTree<K, T>( 
						this.Left, 
						this, 
						this.Right.Merge( other ) 
					) 
				);
			} else if ( 0 < c ) { 
				return empty.BalanceTree( 
					new MinTree<K, T>( 
						this.Left, 
						k, min.Value, this.KeyLogic, 
						this.Right.Merge( other.Remove( k ).Add( this.Key, this.Value ) ) 
					) 
				);
			} else {
				throw new DuplicateKeyException( "other" );
			}
		}

		public sealed override IBinarySearchTree<K, T> SetLeft( IBinarySearchTree<K, T> left ) { 
			if ( null == left ) { 
				throw new System.ArgumentNullException( "left" );
			} else if ( !left.IsEmpty ) { 
				throw new System.NotSupportedException();
			} else if ( this.Left == left ) { 
				return this;
			} else { 
				return this.SetLeft( left, MinCtor );
			}
		}
		public sealed override IBinarySearchTree<K, T> SetRight( IBinarySearchTree<K, T> right ) { 
			return this.SetRight( right, MinCtor );
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static IBinarySearchTree<K, T> GetEmpty() { 
			return EmptyTree.Get();
		}
		public static IBinarySearchTree<K, T> GetEmpty( System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}
			return EmptyTree.Get( keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator ) { 
			if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IBinarySearchTree<K, T> empty ) { 
			if ( null == empty ) { 
				throw new System.ArgumentNullException( "empty" );
			} else if ( !empty.IsEmpty ) { 
				throw new System.ArgumentException( null, "empty" );
			} else if ( null == empty.KeyLogic ) { 
				throw new System.ArgumentException( null, "empty" );
			}

			return EmptyTree.Get( empty );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, IBinarySearchTree<K, T> empty ) { 
			if ( null == empty ) { 
				throw new System.ArgumentNullException( "empty" );
			} else if ( !empty.IsEmpty ) { 
				throw new System.ArgumentException( null, "empty" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, empty );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer, IBinarySearchTree<K, T> empty ) { 
			if ( null == empty ) { 
				throw new System.ArgumentNullException( "empty" );
			} else if ( !empty.IsEmpty ) { 
				throw new System.ArgumentException( null, "empty" );
			} else if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}

			return EmptyTree.Get( generator, keyComparer, empty );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IKeyLogic<K, T> keyLogic ) { 
			if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			}

			return EmptyTree.Get( keyLogic );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> empty ) { 
			if ( null == empty ) { 
				throw new System.ArgumentNullException( "empty" );
			} else if ( !empty.IsEmpty ) { 
				throw new System.ArgumentException( null, "empty" );
			} else if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			}

			return EmptyTree.Get( keyLogic, empty );
		}
		#endregion static methods

	}

}