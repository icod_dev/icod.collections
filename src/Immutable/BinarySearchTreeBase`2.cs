namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public abstract class BinarySearchTreeBase<K, T> : IBinarySearchTree<K, T> { 

		#region internal classes
		[System.Serializable]
		[System.Diagnostics.DebuggerDisplay( "Empty" )]
		protected abstract class EmptyTree : IBinarySearchTree<K, T> { 
			private readonly IKeyLogic<K, T> myKeyLogic;
			private static readonly System.Collections.Generic.ICollection<T> theValues;
			private static readonly System.Collections.Generic.ICollection<K> theKeys;

			static EmptyTree() { 
				theValues = new System.Collections.Generic.List<T>( 1 ).AsReadOnly();
				theKeys = new System.Collections.Generic.List<K>( 1 ).AsReadOnly();
			}
			protected EmptyTree( IKeyLogic<K, T> keyLogic ) : base() { 
				if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				}
				myKeyLogic = keyLogic;
			}

			ITree<T> ITree<T>.Empty { 
				get { 
					return this;
				}
			}
			ISearchTree<K, T> ISearchTree<K, T>.Empty { 
				get { 
					return this;
				}
			}
			public IBinarySearchTree<K, T> Empty { 
				get { 
					return this;
				}
			}
			public System.Int32 Count { 
				get { 
					return 0;
				}
			}
			public System.Int32 Height { 
				get { 
					return 0;
				}
			}
			public System.Int32 Length { 
				get { 
					return 0;
				}
			}
			public System.Boolean IsEmpty { 
				get { 
					return true;
				}
			}
			public System.Boolean IsLeaf { 
				get { 
					return false;
				}
			}
			public System.Int32 BalanceFactor { 
				get { 
					return 0;
				}
			}
			public System.Int32 SlackFactor { 
				get { 
					return 0;
				}
			}
			public System.Boolean IsLeftHeavy { 
				get { 
					return false;
				}
			}
			public System.Boolean IsRightHeavy { 
				get { 
					return false;
				}
			}
			public System.Boolean IsPerfect { 
				get { 
					return true;
				}
			}
			public System.Boolean IsFull { 
				get { 
					return true;
				}
			}
			public T Value { 
				get { 
					throw new System.InvalidOperationException();
				}
			}
			public System.Collections.Generic.ICollection<T> Values { 
				get { 
					return theValues;
				}
			}
			System.Collections.Generic.IEnumerable<ITree<T>> ITree<T>.Branches { 
				get { 
					yield break;
				}
			}
			System.Collections.Generic.IEnumerable<ISearchTree<K, T>> ISearchTree<K, T>.Branches { 
				get { 
					yield break;
				}
			}
			public System.Collections.Generic.IEnumerable<IBinarySearchTree<K, T>> Branches { 
				get { 
					yield break;
				}
			}
			public K Key { 
				get { 
					throw new System.InvalidOperationException();
				}
			}
			ISearchTree<K, T> ISearchTree<K, T>.this[ K key ] { 
				get { 
					return this[ key ];
				}
			}
			public IBinarySearchTree<K, T> this[ K key ] { 
				get { 
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}
			ISearchTree<K, T> ISearchTree<K, T>.Min { 
				get { 
					throw new System.InvalidOperationException();
				}
			}
			ISearchTree<K, T> ISearchTree<K, T>.Max { 
				get { 
					throw new System.InvalidOperationException();
				}
			}
			public IBinarySearchTree<K, T> Min { 
				get { 
					throw new System.InvalidOperationException();
				}
			}
			public IBinarySearchTree<K, T> Max { 
				get { 
					throw new System.InvalidOperationException();
				}
			}

			public IKeyLogic<K, T> KeyLogic { 
				get { 
					return myKeyLogic;
				}
			}
			public System.Collections.Generic.ICollection<K> Keys { 
				get { 
					return theKeys;
				}
			}
			IBinarySearchTree<K, T> IBinary<IBinarySearchTree<K, T>>.Left { 
				get { 
					return this.Left;
				}
			}
			public virtual IBinarySearchTree<K, T> Left { 
				get { 
					return this;
				}
			}
			IBinarySearchTree<K, T> IBinary<IBinarySearchTree<K, T>>.Right { 
				get { 
					return this.Right;
				}
			}
			public virtual IBinarySearchTree<K, T> Right { 
				get { 
					return this;
				}
			}

			public abstract System.Boolean IsInBalance( IBinarySearchTree<K, T> other );
			public virtual IBinarySearchTree<K, T> SetLeft( IBinarySearchTree<K, T> left ) { 
				throw new System.InvalidOperationException();
			}
			public virtual IBinarySearchTree<K, T> SetRight( IBinarySearchTree<K, T> right ) { 
				throw new System.InvalidOperationException();
			}

			public IBinarySearchTree<K, T> RotateLeft() { 
				return this;
			}
			public IBinarySearchTree<K, T> RotateRight() { 
				return this;
			}
			public IBinarySearchTree<K, T> RotateLeftRight() { 
				return this;
			}
			public IBinarySearchTree<K, T> RotateRightLeft() { 
				return this;
			}

			public System.Boolean ContainsKey( K key ) { 
				return false;
			}
			ISearchTree<K, T> ISearchTree<K, T>.Add( T item ) { 
				return this.Add( item );
			}
			ISearchTree<K, T> ISearchTree<K, T>.Add( K key, T item ) { 
				return this.Add( key, item );
			}
			public IBinarySearchTree<K, T> Add( T item ) { 
				return this.Add( this.KeyLogic.Generator( item ), item );
			}
			public abstract IBinarySearchTree<K, T> Add( K key, T item );

			ISearchTree<K, T> ISearchTree<K, T>.Remove( K key ) { 
				return this.Remove( key );
			}
			ISearchTree<K, T> ISearchTree<K, T>.RemoveItem( T item ) { 
				return this.RemoveItem( item );
			}
			public IBinarySearchTree<K, T> Remove( K key ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			}
			public IBinarySearchTree<K, T> RemoveItem( T item ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			}

			ISearchTree<K, T> ISearchTree<K, T>.Find( K key ) { 
				return this;
			}
			ISearchTree<K, T> ISearchTree<K, T>.FindItem( T item ) { 
				return this;
			}
			public IBinarySearchTree<K, T> Find( K key ) { 
				return this;
			}
			public IBinarySearchTree<K, T> FindItem( T item ) { 
				return this;
			}

			public abstract IBinarySearchTree<K, T> BalanceTree( IBinarySearchTree<K, T> other );
			public abstract IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other );

			public System.Boolean IsReadOnly { 
				get { 
					return true;
				}
			}
			void System.Collections.Generic.ICollection<ITree<T>>.Add( ITree<T> item ) {
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<ITree<T>>.Clear() {
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.ICollection<ITree<T>>.Contains( ITree<T> item ) {
				foreach ( ITree<T> tree in InOrder( this ) ) {
					if ( tree.Equals( item ) ) {
						return true;
					}
				}
				return false;
			}
			public void CopyTo( ITree<T>[] array, System.Int32 arrayIndex ) {
				if ( arrayIndex < 0 ) {
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) {
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) {
					throw new System.ArgumentException( null, "array" );
				}
				foreach ( ITree<T> tree in InOrder( this ) ) {
					array[ arrayIndex++ ] = tree;
				}
			}
			System.Boolean System.Collections.Generic.ICollection<ITree<T>>.Remove( ITree<T> item ) {
				throw new System.NotSupportedException();
			}

			void System.Collections.Generic.ICollection<ISearchTree<K, T>>.Add( ISearchTree<K, T> item ) {
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<ISearchTree<K, T>>.Clear() {
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.ICollection<ISearchTree<K, T>>.Contains( ISearchTree<K, T> item ) {
				foreach ( ISearchTree<K, T> tree in InOrder( this ) ) {
					if ( tree.Equals( item ) ) {
						return true;
					}
				}
				return false;
			}
			public void CopyTo( ISearchTree<K, T>[] array, System.Int32 arrayIndex ) {
				if ( arrayIndex < 0 ) {
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) {
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) {
					throw new System.ArgumentException( null, "array" );
				}
				foreach ( ISearchTree<K, T> tree in InOrder( this ) ) {
					array[ arrayIndex++ ] = tree;
				}
			}
			System.Boolean System.Collections.Generic.ICollection<ISearchTree<K, T>>.Remove( ISearchTree<K, T> item ) {
				throw new System.NotSupportedException();
			}

			void System.Collections.Generic.ICollection<IBinarySearchTree<K, T>>.Add( IBinarySearchTree<K, T> item ) {
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<IBinarySearchTree<K, T>>.Clear() {
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.ICollection<IBinarySearchTree<K, T>>.Contains( IBinarySearchTree<K, T> item ) {
				foreach ( IBinarySearchTree<K, T> tree in InOrder( this ) ) {
					if ( tree.Equals( item ) ) {
						return true;
					}
				}
				return false;
			}
			public void CopyTo( IBinarySearchTree<K, T>[] array, System.Int32 arrayIndex ) {
				if ( arrayIndex < 0 ) {
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) {
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) {
					throw new System.ArgumentException( null, "array" );
				}
				foreach ( IBinarySearchTree<K, T> tree in InOrder( this ) ) {
					array[ arrayIndex++ ] = tree;
				}
			}
			System.Boolean System.Collections.Generic.ICollection<IBinarySearchTree<K, T>>.Remove( IBinarySearchTree<K, T> item ) {
				throw new System.NotSupportedException();
			}

			void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Add( System.Collections.Generic.KeyValuePair<K, T> item ) {
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Clear() {
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Contains( System.Collections.Generic.KeyValuePair<K, T> item ) {
				foreach ( IBinarySearchTree<K, T> tree in InOrder( this ) ) {
					if ( tree.Equals( item ) ) {
						return true;
					}
				}
				return false;
			}
			public void CopyTo( System.Collections.Generic.KeyValuePair<K, T>[] array, System.Int32 arrayIndex ) { 
				if ( arrayIndex < 0 ) { 
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) { 
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) { 
					throw new System.ArgumentException( null, "array" );
				}
				foreach ( ISearchTree<K, T> tree in InOrder( this ) ) { 
					array[ arrayIndex++ ] = new System.Collections.Generic.KeyValuePair<K,T>( tree.Key, tree.Value );
				}
			}
			System.Boolean System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Remove( System.Collections.Generic.KeyValuePair<K, T> item ) {
				throw new System.NotSupportedException();
			}

			void System.Collections.Generic.ICollection<T>.Add( T item ) {
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<T>.Clear() {
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.ICollection<T>.Contains( T item ) {
				return this.Values.Contains( item );
			}
			public void CopyTo( T[] array, System.Int32 arrayIndex ) {
				if ( arrayIndex < 0 ) {
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) {
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) {
					throw new System.ArgumentException( null, "array" );
				}
				this.Values.CopyTo( array, arrayIndex );
			}
			System.Boolean System.Collections.Generic.ICollection<T>.Remove( T item ) {
				throw new System.NotSupportedException();
			}

			T System.Collections.Generic.IDictionary<K, T>.this[ K key ] {
				get {
					return this[ key ].Value;
				}
				set {
					throw new System.NotSupportedException();
				}
			}
			void System.Collections.Generic.IDictionary<K, T>.Add( K key, T item ) { 
				throw new System.NotSupportedException();
			}
			System.Boolean System.Collections.Generic.IDictionary<K, T>.Remove( K key ) { 
				throw new System.NotSupportedException();
			}
			public System.Boolean TryGetValue( K key, out T value ) {  
				value = default( T );
				return false;
			}

			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
				yield break;
			}
			System.Collections.Generic.IEnumerator<ITree<T>> ITree<T>.GetEnumerator() {
				yield break;
			}
			System.Collections.Generic.IEnumerator<ISearchTree<K, T>> ISearchTree<K, T>.GetEnumerator() {
				yield break;
			}
			System.Collections.Generic.IEnumerator<ITree<T>> System.Collections.Generic.IEnumerable<ITree<T>>.GetEnumerator() {
				yield break;
			}
			System.Collections.Generic.IEnumerator<ISearchTree<K, T>> System.Collections.Generic.IEnumerable<ISearchTree<K, T>>.GetEnumerator() { 
				yield break;
			}
			System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator() { 
				yield break;
			}
			System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<K, T>> System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<K, T>>.GetEnumerator() { 
				yield break;
			}
			public System.Collections.Generic.IEnumerator<IBinarySearchTree<K, T>> GetEnumerator() { 
				yield break;
			}
		}
		#endregion internal classes


		#region fields
		public static readonly Func<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>> DefaultOperator;

		private readonly T myValue;
		private readonly K myKey;
		private readonly IKeyLogic<K, T> myKeyLogic;
		private readonly IBinarySearchTree<K, T> myLeft;
		private readonly IBinarySearchTree<K, T> myRight;
		private readonly System.Int32 myBalanceFactor;
		private readonly System.Int32 mySlackFactor;
		private readonly System.Int32 myHeight;
		private readonly System.Int32 myCount;
		private readonly System.Int32 myLength;
		private readonly System.Boolean myIsPerfect;
		private readonly System.Boolean myIsFull;
		private System.Collections.Generic.ICollection<K> myKeys;
		private System.Collections.Generic.ICollection<T> myValues;
		#endregion fields


		#region .ctor
		static BinarySearchTreeBase() { 
			DefaultOperator = delegate( IBinarySearchTree<K, T> other ) { 
				return other;
			};
		}

		private BinarySearchTreeBase() : base() { 
			myKeys = null;
			myValues = null;
		}
		private BinarySearchTreeBase( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> right ) : this() { 
			if ( null == left ) { 
				throw new System.ArgumentNullException( "left" );
			} else if ( null == right ) { 
				throw new System.ArgumentNullException( "right" );
			}

			myLeft = left;
			myRight = right;

			System.Int32 lh = left.Height;
			System.Int32 rh = right.Height;
			myBalanceFactor = rh - lh;
			myHeight = 1 + System.Math.Max( lh, rh );
			myLength = 1 + System.Math.Min( left.Length, right.Length );
			mySlackFactor = myHeight - myLength;
			myCount = 1 + left.Count + right.Count;
			myIsPerfect = ( myCount == ( (System.Int32)System.Math.Pow( 2.0, myHeight ) - 1 ) );

			if ( myLeft.IsEmpty ^ myRight.IsEmpty ) { 
				myIsFull = false;
			} else { 
				myIsFull = myLeft.IsFull && myRight.IsFull;
			}
		}
		protected BinarySearchTreeBase( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) : this( left, right ) { 
			if ( ( null == root ) || root.IsEmpty ) { 
				throw new System.ArgumentNullException( "root" );
			} else if ( null == root.KeyLogic ) {
				throw new DuplicateKeyException( "root" );
			}

			myKeyLogic = root.KeyLogic;
			myKey = root.Key;
			myValue = root.Value;
		}
		protected BinarySearchTreeBase( IBinarySearchTree<K, T> left, K key, T value, IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> right ) : this( left, right ) { 
			if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			}

			myKeyLogic = keyLogic;
			myKey = key;
			myValue = value;
		}
		#endregion .ctor


		#region properties
		ITree<T> ITree<T>.Empty { 
			get { 
				return this.Empty;
			}
		}
		ISearchTree<K, T> ISearchTree<K, T>.Empty { 
			get { 
				return this.Empty;
			}
		}
		public abstract IBinarySearchTree<K, T> Empty { 
			get;
		}
		public System.Int32 Count { 
			get { 
				return myCount;
			}
		}
		public System.Int32 Height { 
			get { 
				return myHeight;
			}
		}
		public System.Int32 Length { 
			get { 
				return myLength;
			}
		}
		public System.Boolean IsEmpty { 
			get { 
				return false;
			}
		}
		public System.Int32 BalanceFactor { 
			get { 
				return myBalanceFactor;
			}
		}
		public System.Int32 SlackFactor { 
			get { 
				return mySlackFactor;
			}
		}
		public System.Boolean IsRightHeavy { 
			get { 
				return ( this.Left.Height < this.Right.Height );
			}
		}
		public System.Boolean IsLeftHeavy { 
			get { 
				return ( this.Right.Height < this.Left.Height );
			}
		}
		public System.Boolean IsPerfect { 
			get { 
				return myIsPerfect;
			}
		}
		public System.Boolean IsLeaf { 
			get { 
				return ( this.Left.IsEmpty && this.Right.IsEmpty );
			}
		}
		public System.Boolean IsFull { 
			get { 
				return myIsFull;
			}
		}
		public virtual T Value { 
			get { 
				return myValue;
			}
		}
		public K Key { 
			get { 
				return myKey;
			}
		}
		public IKeyLogic<K,T> KeyLogic { 
			get { 
				return myKeyLogic;
			}
		}

		public System.Collections.Generic.ICollection<T> Values { 
			get { 
				if ( null == myValues ) { 
					System.Collections.Generic.List<T> values = new System.Collections.Generic.List<T>( this.Count );
					foreach ( ITree<T> tree in InOrder( this ) ) { 
						values.Add( tree.Value );
					}
					if ( null == myValues ) { 
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<T>>( 
							ref myValues, 
							values.AsReadOnly(), 
							null 
						);
					}
				}
				return myValues;
			}
		}
		public System.Collections.Generic.ICollection<K> Keys { 
			get { 
				if ( null == myKeys ) { 
					System.Collections.Generic.List<K> keys = new System.Collections.Generic.List<K>( this.Count );
					foreach ( ISearchTree<K, T> tree in InOrder( this ) ) { 
						keys.Add( tree.Key );
					}
					if ( null == myKeys ) { 
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<K>>( 
							ref myKeys, 
							keys.AsReadOnly(), 
							null 
						);
					}
				}
				return myKeys;
			}
		}

		T System.Collections.Generic.IDictionary<K, T>.this[ K key ] { 
			get { 
				return this[ key ].Value;
			}
			set { 
				throw new System.NotSupportedException();
			}
		}
		ISearchTree<K, T> ISearchTree<K, T>.this[ K key ] { 
			get { 
				return this[ key ];
			}
		}
		public virtual IBinarySearchTree<K, T> this[ K key ] { 
			get { 
				IBinarySearchTree<K, T> current = this;
				System.Collections.Generic.IComparer<K> comparer;
				System.Int32 c;

				do { 
					comparer = current.KeyLogic.Comparer;
					c = comparer.Compare( current.Key, key );
					if ( c < 0 ) { 
						current = current.Right;
					} else if ( 0 < c ) { 
						current = current.Left;
					}
				} while ( ( !current.IsEmpty ) && ( c != 0 ) );

				if ( 0 == c ) { 
					return current;
				} else { 
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}
		}
		ISearchTree<K, T> ISearchTree<K, T>.Min { 
			get { 
				return this.Min;
			}
		}
		ISearchTree<K, T> ISearchTree<K, T>.Max { 
			get { 
				return this.Max;
			}
		}
		public virtual IBinarySearchTree<K, T> Min { 
			get { 
				IBinarySearchTree<K, T> current = this;
				IBinarySearchTree<K, T> probe = current.Left;

				while ( !probe.IsEmpty ) { 
					current = probe;
					probe = probe.Left;
				}

				return current;
			}
		}
		public virtual IBinarySearchTree<K, T> Max { 
			get { 
				IBinarySearchTree<K, T> current = this;
				IBinarySearchTree<K, T> probe = current.Right;

				while ( !probe.IsEmpty ) { 
					current = probe;
					probe = probe.Right;
				}

				return current;
			}
		}

		System.Collections.Generic.IEnumerable<ITree<T>> ITree<T>.Branches { 
			get { 
				foreach ( IBinarySearchTree<K, T> tree in this.Branches ) { 
					yield return tree;
				}
			}
		}
		System.Collections.Generic.IEnumerable<ISearchTree<K, T>> ISearchTree<K, T>.Branches { 
			get { 
				foreach ( IBinarySearchTree<K, T> tree in this.Branches ) { 
					yield return tree;
				}
			}
		}
		public System.Collections.Generic.IEnumerable<IBinarySearchTree<K, T>> Branches { 
			get { 
				yield return this.Left;
				yield return this.Right;
			}
		}

		IBinarySearchTree<K, T> IBinary<IBinarySearchTree<K, T>>.Left { 
			get { 
				return this.Left;
			}
		}
		public IBinarySearchTree<K, T> Left { 
			get { 
				return myLeft;
			}
		}
		IBinarySearchTree<K, T> IBinary<IBinarySearchTree<K, T>>.Right { 
			get { 
				return this.Right;
			}
		}
		public IBinarySearchTree<K, T> Right { 
			get { 
				return myRight;
			}
		}

		public System.Boolean IsReadOnly { 
			get { 
				return true;
			}
		}
		#endregion properties


		#region methods
		protected System.Int32 ComputeHashCode() { 
			System.Int32 hashCode = 0;
			unchecked { 
				hashCode += this.Left.GetHashCode();
				hashCode += this.Right.GetHashCode();
				if ( null != this.Key ) { 
					hashCode += this.Key.GetHashCode();
				}
			}
			return hashCode;
		}
		public virtual System.Boolean ContainsKey( K key ) { 
			IBinarySearchTree<K, T> probe = this.Find( key );
			if ( probe.IsEmpty ) { 
				return false;
			} else { 
				return ( 0 == probe.KeyLogic.Comparer.Compare( key, probe.Key ) );
			}
		}

		ISearchTree<K, T> ISearchTree<K, T>.Add( T item ) { 
			return this.Add( item );
		}
		ISearchTree<K, T> ISearchTree<K, T>.Add( K key, T item ) { 
			return this.Add( key, item );
		}
		public IBinarySearchTree<K, T> Add( T item ) { 
			return this.Add( this.KeyLogic.Generator( item ), item );
		}
		public abstract IBinarySearchTree<K, T> Add( K key, T item );

		public virtual System.Boolean IsInBalance( IBinarySearchTree<K, T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( 
				other.IsEmpty 
				|| other.IsLeaf 
				|| other.IsPerfect 
			) { 
				return true;
			}
			return this.Empty.IsInBalance( other );
		}
		public virtual IBinarySearchTree<K, T> BalanceTree( IBinarySearchTree<K, T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( 
				other.IsEmpty 
				|| other.IsLeaf 
				|| other.IsPerfect 
				|| this.IsInBalance( other ) 
			) { 
				return other;
			} else { 
				return this.Empty.BalanceTree( other );
			}
		}

		ISearchTree<K, T> ISearchTree<K, T>.Remove( K key ) { 
			return this.Remove( key );
		}
		ISearchTree<K, T> ISearchTree<K, T>.RemoveItem( T item ) { 
			return this.RemoveItem( item );
		}
		public virtual IBinarySearchTree<K, T> RemoveItem( T item ) { 
			return this.Remove( this.KeyLogic.Generator( item ) );
		}
		public virtual IBinarySearchTree<K, T> Remove( K key ) { 
			IBinarySearchTree<K, T> current = this.Find( key );
			System.Collections.Generic.IComparer<K> comparer = current.KeyLogic.Comparer;
			System.Int32 c = comparer.Compare( key, current.Key );
			if ( 0 != c ) { 
				throw new System.Collections.Generic.KeyNotFoundException();
			}

			IBinarySearchTree<K, T> left = current.Left;
			IBinarySearchTree<K, T> right = current.Right;
			if ( left.IsEmpty ) { 
				return right;
			} else if ( right.IsEmpty ) { 
				return left;
			} else { 
				IBinarySearchTree<K, T> probe;
				if ( left.Count <= right.Count ) { 
					probe = left.Right;
					while ( !probe.IsEmpty ) { 
						left = left.RotateLeft();
						probe = left.Right;
					}
					current = left.SetRight( right );
				} else { 
					probe = right.Left;
					while ( !probe.IsEmpty ) { 
						right = right.RotateLeft();
						probe = right.Left;
					}
					current = right.SetLeft( left );
				}
				return this.BalanceTree( current );
			}
		}

		ISearchTree<K, T> ISearchTree<K, T>.FindItem( T item ) { 
			return this.FindItem( item );
		}
		ISearchTree<K, T> ISearchTree<K, T>.Find( K key ) { 
			return this.Find( key );
		}
		public IBinarySearchTree<K, T> FindItem( T item ) { 
			return this.Find( this.KeyLogic.Generator( item ) );
		}
		public virtual IBinarySearchTree<K, T> Find( K key ) { 
			IBinarySearchTree<K, T> current;
			IBinarySearchTree<K, T> probe = this;
			IStack<System.Boolean> isLeft = Stack<System.Boolean>.GetEmpty();

			System.Collections.Generic.IComparer<K> comparer;
			System.Int32 c;

			do { 
				current = probe;
				comparer = current.KeyLogic.Comparer;
				c = comparer.Compare( current.Key, key );
				if ( c < 0 ) { 
					probe = current.Right;
					isLeft = isLeft.Push( false );
				} else if ( 0 < c ) { 
					probe = current.Left;
					isLeft = isLeft.Push( true );
				}
			} while ( !probe.IsEmpty && ( 0 != c ) );
			if ( probe.IsEmpty ) { 
				isLeft = isLeft.Pop();
			}

			isLeft = isLeft.Reverse();
			current = this;
			System.Boolean goLeft;
			while ( !isLeft.IsEmpty ) { 
				goLeft = isLeft.Peek();
				isLeft = isLeft.Pop();
				if ( goLeft ) { 
					if ( !isLeft.IsEmpty && !isLeft.Peek() ) { 
						isLeft = isLeft.Pop();
						current = current.RotateRightLeft();
					} else { 
						current = current.RotateRight();
					}
				} else { 
					if ( !isLeft.IsEmpty && isLeft.Peek() ) { 
						isLeft = isLeft.Pop();
						current = current.RotateLeftRight();
					} else { 
						current = current.RotateLeft();
					}
				}
			}

			return current;
		}

		public abstract IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other );
		protected IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other, Func<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>> currentOp, Func<IBinarySearchTree<K, T>, IBinarySearchTree<K, T>> parentOp ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( null == currentOp ) { 
				throw new System.ArgumentNullException( "currentOp" );
			} else if ( null == parentOp ) {
				throw new System.ArgumentNullException( "parentOp" );
			} else if ( other.IsEmpty ) { 
				return parentOp( this );
			}

			IStack<System.Boolean> isLeft = Stack<System.Boolean>.GetEmpty();
			IStack<System.Boolean> isSplit = Stack<System.Boolean>.GetEmpty();
			IBinarySearchTree<K, T> current = null;
			IBinarySearchTree<K, T> parent = null;
			IStack<IBinarySearchTree<K, T>> branch = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( this );
			IStack<IBinarySearchTree<K, T>> input = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( other );

			K min;
			K max;
			K key;
			System.Int32 c;
			System.Collections.Generic.IComparer<K> comparer;
			Pair<IBinarySearchTree<K, T>> pair;
			do { 
				other = input.Peek();
				input = input.Pop();
				min = other.Min.Key;
				max = other.Max.Key;
				current = branch.Peek();
				do { 
					key = current.Key;
					comparer = current.KeyLogic.Comparer;
					c = comparer.Compare( key, min );
					if ( c < 0 ) { 
						current = current.Right;
						branch = branch.Push( current );
						isLeft = isLeft.Push( false );
						isSplit = isSplit.Push( false );
					} else if ( 0 < c ) { 
						c = comparer.Compare( key, max );
						if ( 0 < c ) { 
							current = current.Left;
							branch = branch.Push( current );
							isLeft = isLeft.Push( true );
							isSplit = isSplit.Push( false );
						} else if ( c < 0 ) { 
							pair = Split( other, key );
							input = input.Push( pair.Second );
							other = pair.First;
							max = other.Max.Key;
							current = current.Left;
							branch = branch.Push( current );
							isLeft = isLeft.Push( true );
							isSplit = isSplit.Push( true );
						} else {
							throw new DuplicateKeyException( "other" );
						}
					} else {
						throw new DuplicateKeyException( "other" );
					}
				} while ( ( !current.IsEmpty ) && ( 0 != c ) );

				branch = branch.Pop().Push( other );

				while ( ( 1 < branch.Count ) && ( !isSplit.Peek() ) ) { 
					current = branch.Peek();
					branch = branch.Pop();
					parent = branch.Peek();
					branch = branch.Pop();
					if ( isLeft.Peek() ) { 
						parent = parent.SetLeft( currentOp( current ) );
					} else { 
						parent = parent.SetRight( currentOp( current ) );
					}
					branch = branch.Push( parentOp( parent ) );
					isLeft = isLeft.Pop();
					isSplit = isSplit.Pop();
				}
			} while ( !input.IsEmpty );

			while ( 1 < branch.Count ) { 
				current = branch.Peek();
				branch = branch.Pop();
				parent = branch.Peek();
				branch = branch.Pop();
				if ( isLeft.Peek() ) { 
					parent = parent.SetLeft( currentOp( current ) );
				} else { 
					parent = parent.SetRight( currentOp( current ) );
				}
				branch = branch.Push( parentOp( parent ) );
				isLeft = isLeft.Pop();
				isSplit = isSplit.Pop();
			}

			return branch.Peek();
		}

		protected virtual IBinarySearchTree<K, T> ReconstituteTree( IStack<IBinarySearchTree<K, T>> branch, IStack<System.Boolean> isLeft ) { 
			if ( null == branch ) { 
				throw new System.ArgumentNullException( "branch" );
			} else if ( null == isLeft ) { 
				throw new System.ArgumentNullException( "isLeft" );
			} else if ( branch.Count <= isLeft.Count ) { 
				throw new System.ArgumentOutOfRangeException( "isLeft", "The isLeft parameter must contain exactly one fewer elements than the branch parameter." );
			}

			IBinarySearchTree<K, T> current;
			IBinarySearchTree<K, T> parent;
			IBinarySearchTree<K, T> empty = this.Empty;
			while ( 1 < branch.Count ) { 
				current = branch.Peek();
				branch = branch.Pop();
				parent = branch.Peek();
				branch = branch.Pop();
				if ( isLeft.Peek() ) { 
					parent = parent.SetLeft( current );
				} else { 
					parent = parent.SetRight( current );
				}
				isLeft = isLeft.Pop();
				branch = branch.Push( empty.BalanceTree( parent ) );
			}

			return branch.Peek();
		}

		public abstract IBinarySearchTree<K, T> SetLeft( IBinarySearchTree<K, T> left );
		protected IBinarySearchTree<K, T> SetLeft( IBinarySearchTree<K, T> left, BinarySearchTreeCtor<K, T> ctor ) { 
			if ( null == left ) { 
				throw new System.ArgumentNullException( "left" );
			} else if ( null == ctor ) { 
				throw new System.ArgumentNullException( "ctor" );
			} else if ( this.Left == left ) { 
				return this;
			} else if ( left.IsEmpty ) { 
				return ctor( left, this, this.Right );
			} else if ( this.KeyLogic.Comparer.Compare( this.Key, left.Max.Key ) <= 0 ) { 
				throw new System.ArgumentException( null, "left" );
			} else { 
				return ctor( left, this, this.Right );
			}
		}
		public abstract IBinarySearchTree<K, T> SetRight( IBinarySearchTree<K, T> right );
		protected IBinarySearchTree<K, T> SetRight( IBinarySearchTree<K, T> right, BinarySearchTreeCtor<K, T> ctor ) { 
			if ( null == right ) { 
				throw new System.ArgumentNullException( "right" );
			} else if ( null == ctor ) { 
				throw new System.ArgumentNullException( "ctor" );
			} else if ( this.Right == right ) { 
				return this;
			} else if ( right.IsEmpty ) { 
				return ctor( this.Left, this, right );
			} else if ( 0 <= this.KeyLogic.Comparer.Compare( this.Key, right.Min.Key ) ) { 
				throw new System.ArgumentException( null, "right" );
			} else { 
				return ctor( this.Left, this, right );
			}
		}

		public virtual IBinarySearchTree<K, T> RotateLeft() { 
			if ( this.Right.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> root = this.Right;
				IBinarySearchTree<K, T> left = this.SetRight( root.Left );
				root = root.SetLeft( left );
				return root;
			}
		}
		public virtual IBinarySearchTree<K, T> RotateRight() { 
			if ( this.Left.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> root = this.Left;
				IBinarySearchTree<K, T> right = this.SetLeft( root.Right );
				root = root.SetRight( right );
				return root;
			}
		}

		public virtual IBinarySearchTree<K, T> RotateLeftRight() { 
			if ( this.Right.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> right = this.Right;
				return this.SetRight( right.RotateLeft() ).RotateLeft();
			}
		}
		public virtual IBinarySearchTree<K, T> RotateRightLeft() { 
			if ( this.Left.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> left = this.Left;
				return this.SetLeft( left.RotateLeft() ).RotateRight();
			}
		}

		void System.Collections.Generic.ICollection<ITree<T>>.Add( ITree<T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<ITree<T>>.Clear() {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<ITree<T>>.Contains( ITree<T> item ) {
			foreach ( ITree<T> tree in InOrder( this ) ) {
				if ( tree.Equals( item ) ) {
					return true;
				}
			}
			return false;
		}
		public void CopyTo( ITree<T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( ITree<T> tree in InOrder( this ) ) {
				array[ arrayIndex++ ] = tree;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<ITree<T>>.Remove( ITree<T> item ) {
			throw new System.NotSupportedException();
		}

		void System.Collections.Generic.ICollection<ISearchTree<K, T>>.Add( ISearchTree<K, T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<ISearchTree<K, T>>.Clear() {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<ISearchTree<K, T>>.Contains( ISearchTree<K, T> item ) {
			foreach ( ISearchTree<K, T> tree in InOrder( this ) ) {
				if ( tree.Equals( item ) ) {
					return true;
				}
			}
			return false;
		}
		public void CopyTo( ISearchTree<K, T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( ISearchTree<K, T> tree in InOrder( this ) ) {
				array[ arrayIndex++ ] = tree;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<ISearchTree<K, T>>.Remove( ISearchTree<K, T> item ) {
			throw new System.NotSupportedException();
		}

		void System.Collections.Generic.ICollection<IBinarySearchTree<K, T>>.Add( IBinarySearchTree<K, T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<IBinarySearchTree<K, T>>.Clear() {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<IBinarySearchTree<K, T>>.Contains( IBinarySearchTree<K, T> item ) {
			foreach ( IBinarySearchTree<K, T> tree in InOrder( this ) ) {
				if ( tree.Equals( item ) ) {
					return true;
				}
			}
			return false;
		}
		public void CopyTo( IBinarySearchTree<K, T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( IBinarySearchTree<K, T> tree in InOrder( this ) ) {
				array[ arrayIndex++ ] = tree;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<IBinarySearchTree<K, T>>.Remove( IBinarySearchTree<K, T> item ) {
			throw new System.NotSupportedException();
		}

		void System.Collections.Generic.ICollection<T>.Add( T item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<T>.Clear() {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<T>.Contains( T item ) {
			return this.Values.Contains( item );
		}
		public void CopyTo( T[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			this.Values.CopyTo( array, arrayIndex );
		}
		System.Boolean System.Collections.Generic.ICollection<T>.Remove( T item ) {
			throw new System.NotSupportedException();
		}

		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Add( System.Collections.Generic.KeyValuePair<K, T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Clear() {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Contains( System.Collections.Generic.KeyValuePair<K, T> item ) {
			foreach ( IBinarySearchTree<K, T> tree in InOrder( this ) ) {
				if ( tree.Equals( item ) ) {
					return true;
				}
			}
			return false;
		}
		public void CopyTo( System.Collections.Generic.KeyValuePair<K, T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( ISearchTree<K, T> tree in InOrder( this ) ) {
				array[ arrayIndex++ ] = new System.Collections.Generic.KeyValuePair<K, T>( tree.Key, tree.Value );
			}
		}
		System.Boolean System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Remove( System.Collections.Generic.KeyValuePair<K, T> item ) {
			throw new System.NotSupportedException();
		}

		void System.Collections.Generic.IDictionary<K, T>.Add( K key, T item ) {
			throw new System.NotSupportedException();
		}
		System.Boolean System.Collections.Generic.IDictionary<K, T>.Remove( K key ) {
			throw new System.NotSupportedException();
		}
		public virtual System.Boolean TryGetValue( K key, out T value ) { 
			value = default( T );
			System.Boolean success = false;
			IBinarySearchTree<K, T> probe = this;
			System.Collections.Generic.IComparer<K> comparer;
			System.Int32 c;
			do { 
				comparer = probe.KeyLogic.Comparer;
				c = comparer.Compare( probe.Key, key );
				if ( c < 0 ) { 
					probe = probe.Right;
				} else if ( 0 < c ) { 
					probe = probe.Left;
				}
			} while ( ( !probe.IsEmpty ) && ( 0 != c ) );
			if ( 0 == c ) { 
				value = probe.Value;
				success = true;
			}
			return success;
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { 
			return this.GetEnumerator();
		}
		System.Collections.Generic.IEnumerator<ITree<T>> ITree<T>.GetEnumerator() { 
			foreach ( ITree<T> tree in this ) { 
				yield return tree;
			}
		}
		System.Collections.Generic.IEnumerator<ISearchTree<K, T>> ISearchTree<K, T>.GetEnumerator() { 
			foreach ( ISearchTree<K, T> tree in this ) { 
				yield return tree;
			}
		}
		System.Collections.Generic.IEnumerator<ITree<T>> System.Collections.Generic.IEnumerable<ITree<T>>.GetEnumerator() { 
			foreach ( ITree<T> item in this ) { 
				yield return item;
			}
		}
		System.Collections.Generic.IEnumerator<ISearchTree<K, T>> System.Collections.Generic.IEnumerable<ISearchTree<K, T>>.GetEnumerator() { 
			foreach ( ISearchTree<K, T> item in this ) { 
				yield return item;
			}
		}
		System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator() { 
			return this.Values.GetEnumerator();
		}
		System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<K, T>> System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<K, T>>.GetEnumerator() { 
			foreach ( ISearchTree<K, T> tree in InOrder( this ) ) { 
				yield return new System.Collections.Generic.KeyValuePair<K, T>( tree.Key, tree.Value );
			}
		}
		public System.Collections.Generic.IEnumerator<IBinarySearchTree<K, T>> GetEnumerator() {
			return InOrder( this ).GetEnumerator();
		}
		#endregion methods


		#region static methods
		protected static Pair<IBinarySearchTree<K, T>> Split( IBinarySearchTree<K, T> other, K key ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} if ( other.IsEmpty ) { 
				return new Pair<IBinarySearchTree<K,T>>( other, other );
			}

			IBinarySearchTree<K, T> current = other;
			IStack<IBinarySearchTree<K, T>> left = Stack<IBinarySearchTree<K, T>>.GetEmpty();
			IStack<IBinarySearchTree<K, T>> right = Stack<IBinarySearchTree<K, T>>.GetEmpty();
			System.Collections.Generic.IComparer<K> comparer;
			System.Int32 c;
			IBinarySearchTree<K, T> leftt;
			IBinarySearchTree<K, T> rightt;
			do { 
				leftt = current.Left;
				rightt = current.Right;
				comparer = current.KeyLogic.Comparer;
				c = comparer.Compare( key, current.Key );
				if ( c < 0 ) { 
					// xRR
					c = comparer.Compare( key, current.Min.Key );
					if ( c < 0 ) { 
						// RRR
						right = right.Push( current );
						current = current.Empty;
					} else if ( 0 < c ) { 
						// LRR
						right = right.Push( current.SetLeft( current.Empty ) );
						current = leftt;
					} else {
						throw new DuplicateKeyException( "key" );
					}
				} else if ( 0 < c ) { 
					// LLx
					c = comparer.Compare( key, current.Max.Key );
					if ( c < 0 ) { 
						//LLR
						left = left.Push( current.SetRight( current.Empty ) );
						current = rightt;
					} else if ( 0 < c ) { 
						// LLL
						left = left.Push( current );
						current = current.Empty;
					} else {
						throw new DuplicateKeyException( "key" );
					}
				} else {
					throw new DuplicateKeyException( "key" );
				}
			} while ( ( c != 0 ) && ( !current.IsEmpty ) );
			if ( 0 == c ) {
				throw new DuplicateKeyException( "key" );
			}

			IBinarySearchTree<K, T> parent;
			while ( 1 < left.Count ) { 
				current = left.Peek();
				left = left.Pop();
				parent = left.Peek();
				left = left.Pop();
				parent = parent.SetRight( current );
				left = left.Push( parent );
			}
			while ( 1 < right.Count ) { 
				current = right.Peek();
				right = right.Pop();
				parent = right.Peek();
				right = right.Pop();
				parent = parent.SetLeft( current );
				right = right.Push( parent );
			}

			return new Pair<IBinarySearchTree<K,T>>( left.Peek(), right.Peek() );
		}

		public static System.Collections.Generic.IEnumerable<IBinarySearchTree<K, T>> PreOrder( IBinarySearchTree<K, T> tree ) { 
			if ( null == tree ) { 
				throw new System.ArgumentNullException( "tree" );
			} else if ( tree.IsEmpty ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IStack<IBinarySearchTree<K, T>> stack = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( tree );
			IBinarySearchTree<K, T> current;
			IBinarySearchTree<K, T> left;
			IBinarySearchTree<K, T> right;
			while ( !stack.IsEmpty ) {
				current = stack.Peek();
				stack = stack.Pop();
				right = current.Right;
				if ( !right.IsEmpty ) {
					stack = stack.Push( right );
				}
				left = current.Left;
				if ( !left.IsEmpty ) {
					stack = stack.Push( left );
				}
				yield return current;
			}
		}
		public static System.Collections.Generic.IEnumerable<IBinarySearchTree<K, T>> InOrder( IBinarySearchTree<K, T> tree ) { 
			if ( null == tree ) { 
				throw new System.ArgumentNullException( "tree" );
			} else if ( tree.IsEmpty ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IStack<IBinarySearchTree<K, T>> stack = Stack<IBinarySearchTree<K, T>>.GetEmpty();
			for ( IBinarySearchTree<K, T> current = tree; ( false == current.IsEmpty ) || ( false == stack.IsEmpty ); current = current.Right ) { 
				while ( false == current.IsEmpty ) {
					stack = stack.Push( current );
					current = current.Left;
				}
				current = stack.Peek();
				stack = stack.Pop();
				yield return current;
			}
		}
		public static System.Collections.Generic.IEnumerable<IBinarySearchTree<K, T>> ReverseOrder( IBinarySearchTree<K, T> tree ) { 
			if ( null == tree ) { 
				throw new System.ArgumentNullException( "tree" );
			} else if ( tree.IsEmpty ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IStack<IBinarySearchTree<K, T>> stack = Stack<IBinarySearchTree<K, T>>.GetEmpty();
			for ( IBinarySearchTree<K, T> current = tree; ( false == current.IsEmpty ) || ( false == stack.IsEmpty ); current = current.Left ) { 
				while ( false == current.IsEmpty ) { 
					stack = stack.Push( current );
					current = current.Right;
				}
				current = stack.Peek();
				stack = stack.Pop();
				yield return current;
			}
		}
		public static System.Collections.Generic.IEnumerable<IBinarySearchTree<K, T>> PostOrder( IBinarySearchTree<K, T> tree ) { 
			if ( null == tree ) { 
				throw new System.ArgumentNullException( "tree" );
			} else if ( tree.IsEmpty ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IStack<IBinarySearchTree<K, T>> stack = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( tree );
			IStack<IBinarySearchTree<K, T>> visited = Stack<IBinarySearchTree<K, T>>.GetEmpty();
			IBinarySearchTree<K, T> current;
			IBinarySearchTree<K, T> left;
			IBinarySearchTree<K, T> right;
			while ( !stack.IsEmpty ) {
				current = stack.Peek();
				if ( ( !visited.IsEmpty ) && ( visited.Peek() == current ) ) {
					stack = stack.Pop();
					visited = visited.Pop();
					yield return current;
				} else if ( !current.IsLeaf ) {
					visited = visited.Push( current );
					right = current.Right;
					if ( !right.IsEmpty ) {
						stack = stack.Push( right );
					}
					left = current.Left;
					if ( !left.IsEmpty ) {
						stack = stack.Push( left );
					}
				} else {
					stack = stack.Pop();
					yield return current;
				}
			}
		}
		public static System.Collections.Generic.IEnumerable<IBinarySearchTree<K, T>> LevelOrder( IBinarySearchTree<K, T> tree ) { 
			if ( null == tree ) { 
				throw new System.ArgumentNullException( "tree" );
			} else if ( tree.IsEmpty ) { 
				yield break;
			} else if ( tree.IsLeaf ) { 
				yield return tree;
				yield break;
			}

			IQueue<IBinarySearchTree<K, T>> hold = Queue<IBinarySearchTree<K, T>>.GetEmpty().Enqueue( tree );
			IBinarySearchTree<K, T> current;
			IBinarySearchTree<K, T> left;
			IBinarySearchTree<K, T> right;
			while ( !hold.IsEmpty ) {
				current = hold.Peek();
				hold = hold.Dequeue();
				left = current.Left;
				if ( !left.IsEmpty ) {
					hold = hold.Enqueue( left );
				}
				right = current.Right;
				if ( !right.IsEmpty ) {
					hold = hold.Enqueue( right );
				}
				yield return current;
			}
		}
		#endregion static methods

	}

}