namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public abstract class GraphSolver<V, W, R> : GraphSolverBase<IGraph<V, W>, R> { 

		private readonly IGraph<V, W> myGraph;

		protected GraphSolver( IGraph<V, W> graph ) : base() { 
			if ( null == graph ) { 
				throw new System.ArgumentNullException( "graph" );
			}
			myGraph = graph;
		}

		public IGraph<V, W> Graph { 
			get { 
				return myGraph;
			}
		}

	}

}