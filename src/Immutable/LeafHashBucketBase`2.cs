namespace Icod.Collections.Immutable {

	[System.Serializable]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public abstract class LeafHashBucketBase<K, T> : IHashTable<K, T> { 

		#region fields
		private readonly IHashLogic<K, T> myHashLogic;
		private readonly System.Int32 myKeyHash;
		#endregion fields


		#region .ctor
		protected LeafHashBucketBase( IHashLogic<K, T> hashLogic, System.Int32 keyHash ) : base() {
			if ( null == hashLogic ) {
				throw new System.ArgumentNullException( "hashLogic" );
			}

			myHashLogic = hashLogic;
			myKeyHash = keyHash;
		}
		#endregion .ctor


		#region properties
		public IHashLogic<K, T> HashLogic {
			get {
				return myHashLogic;
			}
		}
		public IHashTable<K, T> Empty {
			get {
				return HashTable<K, T>.GetEmpty( myHashLogic );
			}
		}
		public abstract System.Int32 Count {
			get;
		}
		public System.Int32 Length {
			get {
				return 1;
			}
		}
		public System.Int32 Occupancy {
			get {
				return 1;
			}
		}
		public System.Double LoadFactor {
			get {
				return 1;
			}
		}
		public virtual System.Boolean IsEmpty {
			get {
				return ( 0 == this.Count );
			}
		}
		T System.Collections.Generic.IDictionary<K, T>.this[ K key ] {
			get {
				return this[ key ];
			}
			set {
				throw new System.NotSupportedException();
			}
		}
		public abstract T this[ K key ] {
			get;
		}
		public System.Int32 KeyHash {
			get {
				return myKeyHash;
			}
		}
		public abstract System.Collections.Generic.ICollection<K> Keys {
			get;
		}
		public abstract System.Collections.Generic.ICollection<T> Values {
			get;
		}
		public System.Boolean IsReadOnly {
			get {
				return true;
			}
		}
		#endregion properties


		#region methods
		public abstract System.Boolean ContainsKey( K key );
		public abstract IHashTable<K, T> Add( K key, T item );
		public abstract IHashTable<K, T> Remove( K key );
		public System.Boolean ContainsValue( T item ) {
			System.Collections.Generic.IEqualityComparer<T> comparer = myHashLogic.ItemComparer;
			foreach ( System.Collections.Generic.KeyValuePair<K, T> kvp in this ) {
				if ( comparer.Equals( kvp.Value, item ) ) {
					return true;
				}
			}
			return false;
		}
		public HashPath<K, T> Probe( K key ) {
			return new HashPath<K, T>( Stack<IHashTable<K, T>>.GetEmpty().Push( this ), Stack<System.Int32>.GetEmpty() );
		}
		public IHashTable<K, T> GetBucket( System.Int32 index ) {
			if ( 0 != index ) {
				throw new System.IndexOutOfRangeException();
			} else {
				return this;
			}
		}
		public IHashTable<K, T> SetBucket( System.Int32 index, IHashTable<K, T> bucket ) {
			if ( 0 != index ) {
				throw new System.IndexOutOfRangeException();
			} else {
				return bucket;
			}
		}

		void System.Collections.Generic.IDictionary<K, T>.Add( K key, T value ) {
			throw new System.NotImplementedException();
		}
		System.Boolean System.Collections.Generic.IDictionary<K, T>.Remove( K key ) {
			throw new System.NotSupportedException();
		}
		public System.Boolean TryGetValue( K key, out T value ) {
			value = default( T );
			System.Boolean success = false;
			IHashTable<K, T> probe = this.Probe( key ).Bucket.Peek();
			if ( probe.ContainsKey( key ) ) {
				value = probe[ key ];
				success = true;
			}
			return success;
		}
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Add( System.Collections.Generic.KeyValuePair<K, T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Clear() {
			throw new System.NotSupportedException();
		}
		public System.Boolean Contains( System.Collections.Generic.KeyValuePair<K, T> item ) {
			return this.Probe( item.Key ).Bucket.Peek().ContainsValue( item.Value );
		}
		public void CopyTo( System.Collections.Generic.KeyValuePair<K, T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( System.Collections.Generic.KeyValuePair<K, T> kvp in this ) {
				array[ arrayIndex++ ] = kvp;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Remove( System.Collections.Generic.KeyValuePair<K, T> item ) {
			throw new System.NotImplementedException();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
		public abstract System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<K, T>> GetEnumerator();

		public sealed override System.Boolean Equals( System.Object obj ) {
			IHashTable<K, T> other = ( obj as IHashTable<K, T> );
			if ( null == other ) {
				return false;
			} else {
				return this.Equals( other );
			}
		}
		public System.Boolean Equals( IHashTable<K, T> other ) {
			return HashHelper.Equals<K, T>( this, other );
		}
		#endregion methods


		#region static methods
		public static System.Boolean operator ==( LeafHashBucketBase<K, T> a, LeafHashBucketBase<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator ==( LeafHashBucketBase<K, T> a, IHashTable<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator ==( IHashTable<K, T> a, LeafHashBucketBase<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator !=( LeafHashBucketBase<K, T> a, LeafHashBucketBase<K, T> b ) {
			return !( a == b );
		}
		public static System.Boolean operator !=( LeafHashBucketBase<K, T> a, IHashTable<K, T> b ) {
			return !( a == b );
		}
		public static System.Boolean operator !=( IHashTable<K, T> a, LeafHashBucketBase<K, T> b ) {
			return !( a == b );
		}
		#endregion static methods

	}

}