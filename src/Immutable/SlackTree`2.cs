namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class SlackTree<K, T> : AvlTreeBase<K, T> { 

		#region internal classes
		new private class EmptyTree : AvlTreeBase<K, T>.EmptyTree { 
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary<IKeyLogic<K, T>, IBinarySearchTree<K, T>> theEmpty;
			private readonly System.Int32 myHashCode;

			static EmptyTree() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
					theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary<IKeyLogic<K, T>, IBinarySearchTree<K, T>>();
			}
			private EmptyTree( IKeyLogic<K, T> keyLogic ) : base( keyLogic ) { 
				myHashCode = theHashCode;
				unchecked { 
					myHashCode += keyLogic.GetHashCode();
				}
			}

			public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
				return new SlackTree<K, T>( this, key, item, this.KeyLogic, this );
			}
			public sealed override IBinarySearchTree<K, T> Merge( IBinarySearchTree<K, T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				} else if ( other.IsEmpty ) { 
					return this;
				} else { 
					return this.BalanceTree( new SlackTree<K, T>( 
						other.Left, 
						other, 
						other.Right 
					) );
				}
			}

			public static IBinarySearchTree<K, T> Get() { 
				return Get( KeyLogic<K, T>.Default );
			}
			public static IBinarySearchTree<K, T> Get( System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				}
				return Get( KeyLogic<K, T>.Get( keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> keyGenerator ) { 
				if ( null == keyGenerator ) { 
					throw new System.ArgumentNullException( "keyGenerator" );
				}
				return Get( KeyLogic<K, T>.Get( keyGenerator ) );
			}
			public static IBinarySearchTree<K, T> Get( KeyGenerator<T, K> keyGenerator, System.Collections.Generic.IComparer<K> keyComparer ) { 
				if ( null == keyComparer ) { 
					throw new System.ArgumentNullException( "keyComparer" );
				} else if ( null == keyGenerator ) { 
					throw new System.ArgumentNullException( "keyGenerator" );
				}
				return Get( KeyLogic<K, T>.Get( keyGenerator, keyComparer ) );
			}
			public static IBinarySearchTree<K, T> Get( IKeyLogic<K, T> keyLogic ) { 
				if ( null == keyLogic ) { 
					throw new System.ArgumentNullException( "keyLogic" );
				}
				IBinarySearchTree<K, T> output = null;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( keyLogic ) ) { 
						output = theEmpty[ keyLogic ];
					} else { 
						output = new EmptyTree( keyLogic );
						theEmpty.Add( keyLogic, output );
					}
				}
				return output;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;
		private static BinarySearchTreeCtor<K, T> SlackCtor;

		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static SlackTree() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
			SlackCtor = delegate( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) { 
				return new SlackTree<K, T>( left, root, right );
			};
		}

		private SlackTree( IBinarySearchTree<K, T> left, IBinarySearchTree<K, T> root, IBinarySearchTree<K, T> right ) : base( left, root, right ) { 
			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		private SlackTree( IBinarySearchTree<K, T> left, K key, T value, IKeyLogic<K, T> keyLogic, IBinarySearchTree<K, T> right ) : base( left, key, value, keyLogic, right ) { 
			myHashCode = theHashCode;
			unchecked { 
				myHashCode += this.ComputeHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public sealed override IBinarySearchTree<K, T> Empty { 
			get { 
				return GetEmpty( this.KeyLogic );
			}
		}
		#endregion properties


		#region methods
		public sealed override IBinarySearchTree<K, T> Add( K key, T item ) { 
			IBinarySearchTree<K, T> empty = this.Empty;
			IBinarySearchTree<K, T> current = this;
			IStack<IBinarySearchTree<K, T>> branch = Stack<IBinarySearchTree<K, T>>.GetEmpty().Push( current );
			IStack<System.Boolean> isLeft = Stack<System.Boolean>.GetEmpty();
			System.Int32 c;
			System.Collections.Generic.IComparer<K> comparer = this.KeyLogic.Comparer;
			IBinarySearchTree<K, T> probe;

			do { 
				c = comparer.Compare( current.Key, key );
				if ( c < 0 ) { 
					probe = current.Right;
					if ( 
						current.IsRightHeavy 
						&& ( probe.SlackFactor <= 0 ) 
						&& !( probe.IsEmpty || probe.IsLeaf ) 
					) { 
						current = current.RotateLeftRight();
						branch = branch.Pop();
					} else { 
						current = probe;
						isLeft = isLeft.Push( false );
					}
				} else if ( 0 < c ) { 
					probe = current.Left;
					if ( 
						current.IsLeftHeavy 
						&& ( probe.SlackFactor <= 0 ) 
						&& !( probe.IsEmpty || probe.IsLeaf ) 
					) { 
						current = current.RotateRightLeft();
						branch = branch.Pop();
					} else { 
						current = probe;
						isLeft = isLeft.Push( true );
					}
				} else {
					throw new DuplicateKeyException( "key" );
				}
				branch = branch.Push( current );
			} while ( !current.IsEmpty );

			if ( current.IsEmpty ) { 
				branch = branch.Pop().Push( new SlackTree<K, T>( 
					empty, key, item, this.KeyLogic, empty 
				) );
			}

			return this.ReconstituteTree( branch, isLeft );
		}

		public sealed override IBinarySearchTree<K, T> RotateLeft() {
			if ( this.Right.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> root = this.Right;
				IBinarySearchTree<K, T> leftRight = root.Left;
				IBinarySearchTree<K, T> right = root.Right;
				return new SlackTree<K, T>( 
					new SlackTree<K, T>( 
						this.Left, 
						this, 
						leftRight 
					), 
					root, 
					right 
				);
			}
		}
		public sealed override IBinarySearchTree<K, T> RotateRight() { 
			if ( this.Left.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> root = this.Left;
				IBinarySearchTree<K, T> rightLeft = root.Right;
				IBinarySearchTree<K, T> left = root.Left;
				root = new SlackTree<K, T>( 
					left, 
					root, 
					new SlackTree<K, T>( 
						rightLeft, 
						this, 
						this.Right 
					) 
				);
				return root;
			}
		}
		public sealed override IBinarySearchTree<K, T> RotateLeftRight() { 
			if ( this.Right.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> right = this.Right.RotateRight();
				return new SlackTree<K, T>( 
					this.Left, 
					this, 
					right 
				).RotateLeft();
			}
		}
		public sealed override IBinarySearchTree<K, T> RotateRightLeft() { 
			if ( this.Left.IsEmpty ) { 
				return this;
			} else { 
				IBinarySearchTree<K, T> left = this.Left.RotateLeft();
				return new SlackTree<K, T>( 
					left, 
					this, 
					this.Right 
				).RotateRight();
			}
		}

		public sealed override IBinarySearchTree<K, T> SetLeft( IBinarySearchTree<K, T> left ) { 
			return this.SetLeft( left, SlackCtor );
		}
		public sealed override IBinarySearchTree<K, T> SetRight( IBinarySearchTree<K, T> right ) { 
			return this.SetRight( right, SlackCtor );
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static IBinarySearchTree<K, T> GetEmpty() { 
			return EmptyTree.Get();
		}
		public static IBinarySearchTree<K, T> GetEmpty( System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}
			return EmptyTree.Get( keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator ) { 
			if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			} else { 
				return EmptyTree.Get( generator );
			}
		}
		public static IBinarySearchTree<K, T> GetEmpty( KeyGenerator<T, K> generator, System.Collections.Generic.IComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			} else if ( null == generator ) { 
				throw new System.ArgumentNullException( "generator" );
			}
			return EmptyTree.Get( generator, keyComparer );
		}
		public static IBinarySearchTree<K, T> GetEmpty( IKeyLogic<K, T> keyLogic ) {
			if ( null == keyLogic ) { 
				throw new System.ArgumentNullException( "keyLogic" );
			} else { 
				return EmptyTree.Get( keyLogic );
			}
		}
		#endregion static methods

	}

}