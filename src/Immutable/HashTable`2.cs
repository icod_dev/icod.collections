namespace Icod.Collections.Immutable {

	[System.Serializable]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class HashTable<K, T> : IHashTable<K, T> { 

		#region fields
		private static readonly System.Int32 theHashCode;

		private readonly IHashLogic<K, T> myHashLogic;
		private readonly IHashTable<K, T>[] myStore;
		private readonly System.Int32 myCount;
		private readonly System.Int32 myOccupancy;
		private readonly System.Int32 myHashCode;
		private System.Collections.Generic.ICollection<K> myKeys;
		private System.Collections.Generic.ICollection<T> myValues;
		#endregion fields


		#region .ctor
		static HashTable() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
		}

		private HashTable() : base() { 
			myHashCode = theHashCode;
			myKeys = null;
			myValues = null;
		}
		private HashTable( IHashLogic<K, T> hashLogic ) : this() {
			if ( null == hashLogic ) {
				throw new System.ArgumentNullException( "hashLogic" );
			}

			myHashLogic = hashLogic;
			unchecked { 
				myHashCode += myHashLogic.GetHashCode();
			}
		}
		internal HashTable( IHashLogic<K, T> hashLogic, System.Int32 capacity ) : this( hashLogic ) {
			if ( ( capacity < PrimeHelper.MinPrime ) || ( PrimeHelper.MaxPrime < capacity ) ) { 
				throw new System.ArgumentOutOfRangeException( "capacity" );
			}

			myStore = new IHashTable<K,T>[ capacity ];
			myHashLogic = hashLogic;
			myCount = 0;
			myOccupancy = 0;
			unchecked { 
				myHashCode += myStore.GetHashCode();
			}
		}
		private HashTable( IHashLogic<K, T> hashLogic, IHashTable<K, T>[] store, System.Int32 count, System.Int32 occupancy ) : this( hashLogic ) { 
			if ( ( null == store ) || ( 0 == store.Length ) ) { 
				throw new System.ArgumentNullException( "store" );
			} else if ( store.Length < PrimeHelper.MinPrime ) { 
				throw new System.ArgumentException( null, "store" );
			} else if ( count < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "count" );
			} else if ( occupancy < 0 ) { 
				throw new System.ArgumentOutOfRangeException( "occupancy" );
			}

			myOccupancy = occupancy;
			myCount = count;
			myStore = store;
			unchecked { 
				myHashCode += myCount;
				myHashCode += myOccupancy;
				myHashCode += myStore.GetHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public IHashLogic<K, T> HashLogic { 
			get { 
				return myHashLogic;
			}
		}
		public IHashTable<K, T> Empty { 
			get { 
				return GetEmpty( this.HashLogic );
			}
		}
		public System.Int32 Count { 
			get { 
				return myCount;
			}
		}
		public System.Int32 Length { 
			get { 
				return myStore.Length;
			}
		}
		public System.Int32 Occupancy { 
			get { 
				return myOccupancy;
			}
		}
		public System.Double LoadFactor { 
			get { 
				return ( (System.Double)this.Occupancy / (System.Double)this.Length );
			}
		}
		public System.Boolean IsEmpty { 
			get { 
				return ( 0 == myCount );
			}
		}
		public T this[ K key ] { 
			get { 
				throw new System.NotImplementedException();
			}
		}
		public System.Collections.Generic.ICollection<K> Keys { 
			get { 
				if ( null == myKeys ) { 
					System.Collections.Generic.List<K> output = new System.Collections.Generic.List<K>();
					foreach ( System.Collections.Generic.KeyValuePair<K, T> kvp in this ) { 
						output.Add( kvp.Key );
					}
					if ( null == myKeys ) { 
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<K>>( 
							ref myKeys,
							output.AsReadOnly(), 
							null 
						);
					}
				}
				return myKeys;
			}
		}
		public System.Collections.Generic.ICollection<T> Values { 
			get {
				if ( null == myValues ) {
					System.Collections.Generic.List<T> output = new System.Collections.Generic.List<T>();
					foreach ( System.Collections.Generic.KeyValuePair<K, T> kvp in this ) {
						output.Add( kvp.Value );
					}
					if ( null == myValues ) { 
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<T>>(
							ref myValues,
							output.AsReadOnly(),
							null
						);
					}
				}
				return myValues;
			}
		}
		public System.Boolean IsReadOnly { 
			get { 
				return true;
			}
		}
		#endregion properties


		#region methods
		public System.Boolean ContainsKey( K key ) { 
			return this.Probe( key ).Bucket.Peek().ContainsKey( key );
		}
		public System.Boolean ContainsValue( T item ) { 
			System.Collections.Generic.IEqualityComparer<T> comparer = myHashLogic.ItemComparer;
			foreach ( System.Collections.Generic.KeyValuePair<K, T> kvp in this ) { 
				if ( comparer.Equals( kvp.Value, item ) ) { 
					return true;
				}
			}
			return false;
		}
		public HashPath<K, T> Probe( K key ) { 
			System.Int32 code = HashHelper.GetHash<K>( this.HashLogic.KeyComparer, key );
			System.Int32 i;
			IHashTable<K, T> current = this;
			IStack<IHashTable<K, T>> bucket = Stack<IHashTable<K, T>>.GetEmpty().Push( current );
			IStack<System.Int32> index = Stack<System.Int32>.GetEmpty();
			do { 
				i = code % current.Length;
				index = index.Push( i );
				current = current.GetBucket( i );
				bucket = bucket.Push( current );
			} while ( 1 < current.Length );

			return new HashPath<K,T>( bucket, index );
		}
		public IHashTable<K, T> GetBucket( System.Int32 index ) { 
			if ( ( index < 0 ) || ( myStore.Length <= index ) ) { 
				throw new System.IndexOutOfRangeException();
			} else { 
				return ( myStore[ index ] ?? this.Empty );
			}
		}
		public IHashTable<K, T> SetBucket( System.Int32 index, IHashTable<K, T> bucket ) { 
			if ( null == bucket ) { 
				throw new System.ArgumentNullException( "bucket" );
			} else if ( ( index < 0 ) || ( myStore.Length <= index ) ) { 
				throw new System.IndexOutOfRangeException();
			}

			IHashTable<K, T> old = this.GetBucket( index );
			if ( bucket == old ) { 
				return this;
			}
			System.Int32 occupancy = myOccupancy;
			if ( old.IsEmpty && !bucket.IsEmpty ) { 
				occupancy += 1;
			} else if ( !old.IsEmpty && bucket.IsEmpty ) { 
				occupancy -= 1;
			}
			IHashTable<K, T>[] store = new IHashTable<K,T>[ myStore.Length ];
			System.Array.Copy( myStore, 0, store, 0, myStore.Length );
			store[ index ] = bucket;
			return new HashTable<K, T>( this.HashLogic, store, ( myCount - old.Count ) + bucket.Count, occupancy );
		}
		public IHashTable<K, T> Add( K key, T item ) { 
			IHashLogic<K, T> logic = this.HashLogic;
			System.Int32 code = HashHelper.GetHash<K>( logic.KeyComparer, key );
			HashPath<K, T> probe = this.Probe( key );
			IStack<IHashTable<K, T>> bucket = probe.Bucket;
			IStack<System.Int32> index = probe.Index;
			IHashTable<K, T> current = bucket.Peek();

			if ( current.IsEmpty ) { 
				current = new LeafHashBucket<K, T>( logic, code, key, item );
			} else { 
				current = current.Add( key, item );
			}
			bucket = bucket.Pop().Push( current );

			return HashHelper.Unwind<K, T>( bucket, index );
		}
		public IHashTable<K, T> Remove( K key ) { 
			HashPath<K, T> probe = this.Probe( key );
			IStack<IHashTable<K, T>> bucket = probe.Bucket;
			IStack<System.Int32> index = probe.Index;
			IHashTable<K, T> current = bucket.Peek().Remove( key );
			bucket = bucket.Pop().Push( current );

			return HashHelper.Unwind<K, T>( bucket, index );
		}

		T System.Collections.Generic.IDictionary<K, T>.this[ K key ] {
			get {
				return this[ key ];
			}
			set {
				throw new System.NotSupportedException();
			}
		}
		void System.Collections.Generic.IDictionary<K, T>.Add( K key, T value ) {
			throw new System.NotImplementedException();
		}
		System.Boolean System.Collections.Generic.IDictionary<K, T>.Remove( K key ) {
			throw new System.NotSupportedException();
		}
		public System.Boolean TryGetValue( K key, out T value ) {
			value = default( T );
			System.Boolean success = false;
			IHashTable<K, T> probe = this.Probe( key ).Bucket.Peek();
			if ( probe.ContainsKey( key ) ) {
				value = probe[ key ];
				success = true;
			}
			return success;
		}
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Add( System.Collections.Generic.KeyValuePair<K, T> item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Clear() {
			throw new System.NotSupportedException();
		}
		public System.Boolean Contains( System.Collections.Generic.KeyValuePair<K, T> item ) {
			return this.Probe( item.Key ).Bucket.Peek().ContainsValue( item.Value );
		}
		public void CopyTo( System.Collections.Generic.KeyValuePair<K, T>[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( System.Collections.Generic.KeyValuePair<K, T> kvp in this ) {
				array[ arrayIndex++ ] = kvp;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<K, T>>.Remove( System.Collections.Generic.KeyValuePair<K, T> item ) {
			throw new System.NotImplementedException();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
		public System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<K, T>> GetEnumerator() { 
			IQueue<IHashTable<K, T>> bucket = Queue<IHashTable<K, T>>.GetEmpty().Enqueue( this );
			IHashTable<K, T> current;
			IHashTable<K, T> probe;
			while ( !bucket.IsEmpty ) { 
				current = bucket.Peek();
				bucket = bucket.Dequeue();
				if ( 0 < current.Count ) { 
					if ( current is LeafHashBucketBase<K, T> ) { 
						foreach ( System.Collections.Generic.KeyValuePair<K, T> pair in current ) { 
							yield return pair;
						}
					} else { 
						for ( System.Int32 i = 0; i < current.Length; i++ ) { 
							probe = current.GetBucket( i );
							if ( 0 < probe.Count ) { 
								bucket = bucket.Enqueue( current.GetBucket( i ) );
							}
						}
					}
				}
			}
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		public sealed override System.Boolean Equals( System.Object obj ) { 
			IHashTable<K, T> other = ( obj as IHashTable<K, T> );
			if ( null == other ) { 
				return false;
			} else { 
				return this.Equals( other );
			}
		}
		public System.Boolean Equals( IHashTable<K, T> other ) { 
			return HashHelper.Equals<K, T>( this, other );
		}
		#endregion methods


		#region static methods
		public static IHashTable<K, T> GetEmpty() {
			return EmptyHashBucket<K, T>.Get();
		}
		public static IHashTable<K, T> GetEmpty( System.Collections.Generic.IEqualityComparer<K> keyComparer ) { 
			if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}

			return EmptyHashBucket<K, T>.Get( keyComparer );
		}
		public static IHashTable<K, T> GetEmpty( System.Collections.Generic.IEqualityComparer<K> keyComparer, System.Collections.Generic.IEqualityComparer<T> itemComparer ) { 
			if ( null == itemComparer ) { 
				throw new System.ArgumentNullException( "itemComparer" );
			} else if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}

			return EmptyHashBucket<K, T>.Get( keyComparer, itemComparer );
		}
		public static IHashTable<K, T> GetEmpty( IHashLogic<K, T> hashLogic ) { 
			if ( null == hashLogic ) { 
				throw new System.ArgumentNullException( "hashLogic" );
			}

			return EmptyHashBucket<K, T>.Get( hashLogic );
		}

		public static IHashTable<K, T> ToSize( IHashTable<K, T> hashTable ) { 
			if ( null == hashTable ) { 
				throw new System.ArgumentNullException( "hashTable" );
			}
			return HashHelper.ToSize<K, T>( hashTable );
		}
		public static IHashTable<K, T> Resize( IHashTable<K, T> hashTable, System.Int32 size ) { 
			if ( ( size < PrimeHelper.MinPrime ) || ( PrimeHelper.MaxPrime < size ) ) { 
				throw new System.ArgumentOutOfRangeException( "size" );
			} else if ( ( null == hashTable ) || hashTable.IsEmpty ) { 
				throw new System.ArgumentNullException( "hashTable" );
			}
			return HashHelper.Resize<K, T>( hashTable, size );
		}
		public static IHashTable<K, T> Rehash( IHashTable<K, T> hashTable ) { 
			if ( null == hashTable ) { 
				throw new System.ArgumentNullException( "hashTable" );
			} else if ( hashTable.IsEmpty ) { 
				return hashTable.Empty;
			}
			return HashHelper.Rehash<K, T>( hashTable );
		}

		public static System.Boolean operator ==( HashTable<K, T> a, HashTable<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator ==( HashTable<K, T> a, IHashTable<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator ==( IHashTable<K, T> a, HashTable<K, T> b ) {
			return HashHelper.Equals<K, T>( a, b );
		}
		public static System.Boolean operator !=( HashTable<K, T> a, HashTable<K, T> b ) {
			return !( a == b );
		}
		public static System.Boolean operator !=( HashTable<K, T> a, IHashTable<K, T> b ) {
			return !( a == b );
		}
		public static System.Boolean operator !=( IHashTable<K, T> a, HashTable<K, T> b ) {
			return !( a == b );
		}
		#endregion static methods

	}

}