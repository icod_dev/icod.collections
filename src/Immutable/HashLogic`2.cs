namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public struct HashLogic<K, T> : IHashLogic<K, T>, System.IEquatable<IHashLogic<K, T>> { 

		#region fields
		private static readonly System.Int32 theHashCode;
		private static System.Collections.Generic.IDictionary< 
			System.Collections.Generic.IEqualityComparer<K>, 
			System.Collections.Generic.IDictionary< 
				System.Collections.Generic.IEqualityComparer<T>, 
				IHashLogic<K, T> 
			> 
		> theLogic;
		private static IHashLogic<K, T> theDefault;

		private readonly System.Collections.Generic.IEqualityComparer<K> myKeyComparer;
		private readonly System.Collections.Generic.IEqualityComparer<T> myItemComparer;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static HashLogic() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}

			theDefault = null;

			theLogic = new System.Collections.Generic.Dictionary< 
				System.Collections.Generic.IEqualityComparer<K>, 
				System.Collections.Generic.IDictionary< 
					System.Collections.Generic.IEqualityComparer<T>, 
					IHashLogic<K, T> 
				> 
			>();
		}

		private HashLogic( System.Collections.Generic.IEqualityComparer<K> keyComparer, System.Collections.Generic.IEqualityComparer<T> itemComparer ) { 
			if ( null == itemComparer ) { 
				throw new System.ArgumentNullException( "itemComparer" );
			} else if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}

			myHashCode = theHashCode;
			myKeyComparer = keyComparer;
			myItemComparer = itemComparer;
			unchecked { 
				myHashCode += myKeyComparer.GetHashCode();
				myHashCode += myItemComparer.GetHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public static IHashLogic<K, T> Default { 
			get { 
				if ( null == theDefault ) { 
					System.Threading.Interlocked.CompareExchange<IHashLogic<K, T>>( 
						ref theDefault, 
						Get(), 
						null 
					);
				}
				return theDefault;
			}
		}

		public System.Collections.Generic.IEqualityComparer<K> KeyComparer { 
			get { 
				return myKeyComparer;
			}
		}
		public System.Collections.Generic.IEqualityComparer<T> ItemComparer { 
			get { 
				return myItemComparer;
			}
		}
		#endregion properties


		#region methods
		public override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		public override System.Boolean Equals( System.Object obj ) { 
			if ( null == obj ) { 
				return false;
			} else { 
				IHashLogic<K, T> foo = ( obj as IHashLogic<K, T> );
				if ( null == foo ) { 
					return false;
				} else { 
					return this.Equals( foo );
				}
			}
		}
		public System.Boolean Equals( IHashLogic<K, T> other ) { 
			if ( null == (System.Object)other ) { 
				return false;
			} else { 
				return ( 
					this.ItemComparer.Equals( other.ItemComparer ) 
					&& this.KeyComparer.Equals( other.KeyComparer ) 
				);
			}
		}
		#endregion methods


		#region static methods
		public static IHashLogic<K, T> Get() { 
			return Get( Comparer<K>.Default, Comparer<T>.Default );
		}
		public static IHashLogic<K, T> Get( System.Collections.Generic.IEqualityComparer<K> keyComparer, System.Collections.Generic.IEqualityComparer<T> itemComparer ) { 
			if ( null == itemComparer ) { 
				throw new System.ArgumentNullException( "itemComparer" );
			} else if ( null == keyComparer ) { 
				throw new System.ArgumentNullException( "keyComparer" );
			}
			IHashLogic<K, T> output = null;
			System.Collections.Generic.IDictionary<System.Collections.Generic.IEqualityComparer<T>, IHashLogic<K, T>> item;
			lock ( theLogic ) { 
				if ( theLogic.ContainsKey( keyComparer ) ) { 
					item = theLogic[ keyComparer ];
				} else { 
					item = new System.Collections.Generic.Dictionary<System.Collections.Generic.IEqualityComparer<T>, IHashLogic<K, T>>();
					theLogic.Add( keyComparer, item );
				}
			}
			lock ( item ) { 
				if ( item.ContainsKey( itemComparer ) ) { 
					output = item[ itemComparer ];
				} else { 
					output = new HashLogic<K,T>( keyComparer, itemComparer );
					item.Add( itemComparer, output );
				}
			}
			return output;
		}

		public static System.Boolean operator ==( HashLogic<K, T> a, HashLogic<K, T> b ) { 
			return a.Equals( b );
		}
		public static System.Boolean operator ==( HashLogic<K, T> a, IHashLogic<K, T> b ) { 
			if ( null == (System.Object)b ) { 
				return false;
			} else { 
				return a.Equals( b );
			}
		}
		public static System.Boolean operator ==( IHashLogic<K, T> a, HashLogic<K, T> b ) {
			if ( null == (System.Object)a ) { 
				return false;
			} else {
				return b.Equals( a );
			}
		}
		public static System.Boolean operator !=( HashLogic<K, T> a, HashLogic<K, T> b ) { 
			return !( a == b );
		}
		public static System.Boolean operator !=( HashLogic<K, T> a, IHashLogic<K, T> b ) {
			return !( a == b );
		}
		public static System.Boolean operator !=( IHashLogic<K, T> a, HashLogic<K, T> b ) {
			return !( a == b );
		}
		#endregion static methods

	}

}