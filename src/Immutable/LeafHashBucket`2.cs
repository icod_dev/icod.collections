namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class LeafHashBucket<K, T> : LeafHashBucketBase<K, T> { 

		#region fields
		private static readonly System.Int32 theHashCode;
		private readonly System.Int32 myHashCode;
		private readonly K myKey;
		private readonly T myValue;
		private System.Collections.Generic.ICollection<K> myKeys;
		private System.Collections.Generic.ICollection<T> myValues;
		#endregion fields


		#region .ctor
		static LeafHashBucket() {
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked {
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
		}
		private LeafHashBucket( IHashLogic<K, T> hashLogic, System.Int32 keyHash ) : base( hashLogic, keyHash ) {
			myHashCode = theHashCode;
			unchecked {
				myHashCode += hashLogic.GetHashCode();
				myHashCode += keyHash;
			}
		}
		internal LeafHashBucket( IHashLogic<K, T> hashLogic, System.Int32 keyHash, K key, T value ) : this( hashLogic, keyHash ) {
			myValue = value;
			myKey = key;
		}
		#endregion .ctor


		#region properties
		public sealed override System.Int32 Count {
			get {
				return 1;
			}
		}
		public sealed override System.Boolean IsEmpty {
			get {
				return false;
			}
		}
		public sealed override T this[ K key ] {
			get {
				if ( this.ContainsKey( key ) ) {
					return myValue;
				} else {
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}
		}
		public sealed override System.Collections.Generic.ICollection<K> Keys {
			get {
				if ( null == myKeys ) {
					System.Collections.Generic.List<K> keys = new System.Collections.Generic.List<K>( 1 );
					keys.Add( myKey );
					if ( null == myKeys ) {
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<K>>(
							ref myKeys,
							keys.AsReadOnly(),
							null
						);
					}
				}
				return myKeys;
			}
		}
		public sealed override System.Collections.Generic.ICollection<T> Values {
			get {
				if ( null == myValues ) {
					System.Collections.Generic.List<T> values = new System.Collections.Generic.List<T>( 1 );
					values.Add( myValue );
					if ( null == myValues ) {
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<T>>(
							ref myValues,
							values.AsReadOnly(),
							null
						);
					}
				}
				return myValues;
			}
		}
		#endregion properties


		#region methods
		public sealed override System.Boolean ContainsKey( K key ) {
			return this.HashLogic.KeyComparer.Equals( myKey, key );
		}
		public sealed override IHashTable<K, T> Add( K key, T item ) {
			IHashLogic<K, T> logic = this.HashLogic;
			System.Collections.Generic.IEqualityComparer<K> comparer = logic.KeyComparer;
			System.Int32 tCode = this.KeyHash;
			System.Int32 oCode = HashHelper.GetHash<K>( comparer, key );
			if ( tCode == oCode ) {
				if ( comparer.Equals( myKey, key ) ) {
					throw new DuplicateKeyException( "key" );
				} else {
					if ( comparer is System.Collections.Generic.IComparer<K> ) {
						return new ComparableHashBucket<K, T>(
							logic,
							tCode,
							new Pair<K, T>[ 2 ] { 
									new Pair<K, T>( myKey, myValue ), 
									new Pair<K, T>( key, item ) 
								}
						);
					} else {
						return new ListHashBucket<K, T>(
							logic,
							tCode,
							new Pair<K, T>[ 2 ] { 
									new Pair<K, T>( myKey, myValue ), 
									new Pair<K, T>( key, item ) 
								}
						);
					}
				}
			} else {
				return HashHelper.HashUp<K, T>( logic, PrimeHelper.MinPrime, this, new LeafHashBucket<K, T>( logic, oCode, key, item ) );
			}
		}
		public sealed override IHashTable<K, T> Remove( K key ) {
			if ( this.HashLogic.KeyComparer.Equals( myKey, key ) ) {
				return this.Empty;
			} else {
				throw new System.Collections.Generic.KeyNotFoundException();
			}
		}

		public sealed override System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<K, T>> GetEnumerator() {
			yield return new System.Collections.Generic.KeyValuePair<K, T>( myKey, myValue );
		}

		public sealed override System.Int32 GetHashCode() {
			return myHashCode;
		}
		#endregion methods

	}

}