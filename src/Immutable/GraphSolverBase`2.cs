namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public abstract class GraphSolverBase<G, R> : IGraphSolver<G, R> { 

		#region internal classes
		private sealed class AsyncSolution : Icod.AsyncResult<R> { 
			public AsyncSolution( System.AsyncCallback callback, G graph ) : base( callback, graph ) { 
			}

			public void SetSynchronousCopmpletion( System.Boolean synchronous ) { 
				base.SetCompletedSynchronously( synchronous );
			}
			public void Complete( R result, System.Boolean completedSynchronously, System.Exception completionException ) { 
				base.SetCompletion( result, completedSynchronously, completionException );
			}
			public void Complete( System.Boolean completedSynchronously, System.Exception completionException ) { 
				base.SetCompletion( completedSynchronously, completionException );
			}
			public R End() { 
				return base.EndInvoke();
			}
		}
		#endregion internal classes


		#region .ctor
		protected GraphSolverBase() : base() { 
		}
		#endregion .ctor


		#region methods
		public System.IAsyncResult BeginSolve( System.AsyncCallback callback, G graph ) { 
			AsyncSolution output = new AsyncSolution( callback, graph );
			System.Threading.ThreadPool.QueueUserWorkItem( this.SolveHelper, output );
			if ( output.IsCompleted ) { 
				output.SetSynchronousCopmpletion( true );
			}
			return output;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage( 
			"Microsoft.Design", 
			"CA1031:DoNotCatchGeneralExceptionTypes", 
			Justification = "The exception *will* be rethrown when End* is invoked." 
		)]
		private void SolveHelper( System.Object asyncResult ) { 
			AsyncSolution input = (AsyncSolution)asyncResult;
			try { 
				input.Complete( this.Solve(), false, null );
			} catch ( System.Exception e ) { 
				input.Complete( false, e );
			}
		}

		public abstract R Solve();

		public R EndSolve( System.IAsyncResult result ) { 
			AsyncSolution input = (AsyncSolution)result;
			return input.End();
		}
		#endregion methods

	}

}