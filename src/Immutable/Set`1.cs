namespace Icod.Collections.Immutable { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class Set<T> : ISet<T> { 

		#region internal classes
		[System.Serializable]
		[System.Diagnostics.DebuggerDisplay( "Empty" )]
		private sealed class EmptySet : ISet<T> { 
			private static readonly System.Int32 theHashCode;
			private static System.Collections.Generic.IDictionary<System.Collections.Generic.IEqualityComparer<T>, ISet<T>> theEmpty;
			private readonly System.Int32 myHashCode;
			private readonly System.Collections.Generic.IEqualityComparer<T> myComparer;

			static EmptySet() { 
				theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
				unchecked { 
					theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
				}
				theEmpty = new System.Collections.Generic.Dictionary<System.Collections.Generic.IEqualityComparer<T>, ISet<T>>();
			}
			private EmptySet() : base() { 
				myHashCode = theHashCode;
			}
			public EmptySet( System.Collections.Generic.IEqualityComparer<T> comparer ) : this() { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				myComparer = comparer;
				unchecked { 
					myHashCode += myComparer.GetHashCode();
				}
			}

			public ISet<T> Empty { 
				get { 
					return this;
				}
			}
			public System.Int32 Count { 
				get { 
					return 0;
				}
			}
			public System.Boolean IsEmpty { 
				get { 
					return true;
				}
			}
			public System.Collections.Generic.IEqualityComparer<T> Comparer { 
				get { 
					return myComparer;
				}
			}
			public System.Boolean IsReadOnly { 
				get { 
					return true;
				}
			}

			void System.Collections.Generic.ICollection<T>.Add( T item ) {
				throw new System.NotSupportedException();
			}
			void System.Collections.Generic.ICollection<T>.Clear() {
				throw new System.NotSupportedException();
			}
			public void CopyTo( T[] array, System.Int32 arrayIndex ) {
				if ( arrayIndex < 0 ) {
					throw new System.ArgumentOutOfRangeException( "arrayIndex" );
				} else if ( null == array ) {
					throw new System.ArgumentNullException( "array" );
				} else if ( ( array.Length - arrayIndex ) < this.Count ) {
					throw new System.ArgumentException( null, "array" );
				}
				foreach ( T t in this ) {
					array[ arrayIndex++ ] = t;
				}
			}
			System.Boolean System.Collections.Generic.ICollection<T>.Remove( T item ) {
				throw new System.NotImplementedException();
			}

			public System.Boolean Contains( T item ) { 
				return false;
			}
			public ISet<T> Add( T item ) { 
				return new Set<T>( HashTable<T, System.Int32>.GetEmpty( myComparer ).Add( item, 0 ) );
			}
			public ISet<T> Add( System.Collections.Generic.IEnumerable<T> collection ) { 
				if ( null == collection ) { 
					throw new System.ArgumentNullException( "collection" );
				}
				IHashTable<T, System.Int32> store = HashTable<T, System.Int32>.GetEmpty( myComparer );
				foreach ( T t in collection ) { 
					if ( !store.ContainsKey( t ) ) { 
						store = store.Add( t, 0 );
					}
				}
				if ( store.IsEmpty ) { 
					return this;
				} else { 
					return new Set<T>( store );
				}
			}
			public ISet<T> Remove( T item ) { 
				return this;
			}
			public ISet<T> Union( ISet<T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				return other;
			}
			public ISet<T> Intersection( ISet<T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				return this;
			}
			public ISet<T> SymmetricDifference( ISet<T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				return other;
			}
			public ISet<T> Add( ISet<T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				return other;
			}
			public ISet<T> Subtract( ISet<T> other ) { 
				if ( null == other ) { 
					throw new System.ArgumentNullException( "other" );
				}
				return this;
			}
			public ISet<ISet<T>> Partition( System.Predicate<T> predicate ) { 
				if ( null == predicate ) { 
					throw new System.ArgumentNullException( "predicate" );
				}
				return Set<ISet<T>>.GetEmpty();
			}
			public ISet<ISet<T>> Partition( Func<T, T, System.Boolean> predicate ) { 
				if ( null == predicate ) { 
					throw new System.ArgumentNullException( "predicate" );
				}
				return Set<ISet<T>>.GetEmpty();
			}

			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { 
				return this.GetEnumerator();
			}
			public System.Collections.Generic.IEnumerator<T> GetEnumerator() { 
				yield break;
			}

			public sealed override System.Int32 GetHashCode() {
				return myHashCode;
			}
			public sealed override System.Boolean Equals( System.Object obj ) { 
				return this.Equals( obj as ISet<T> );
			}
			public System.Boolean Equals( ISet<T> other ) { 
				if ( null == other ) { 
					return false;
				} else { 
					return ( 
						System.Object.ReferenceEquals( this, other ) 
						|| ( myComparer == other.Comparer ) 
					);
				}
			}

			public static System.Boolean operator ==( EmptySet x, ISet<T> y ) { 
				if ( ( null == (System.Object)x ) && ( null == (System.Object)y ) ) { 
					return true;
				} else if ( ( null == (System.Object)x ) || ( null == (System.Object)y ) ) {
					return false;
				} else { 
					return x.Equals( y );
				}
			}
			public static System.Boolean operator ==( ISet<T> x, EmptySet y ) { 
				return ( y == x );
			}
			public static System.Boolean operator !=( EmptySet x, ISet<T> y ) { 
				return !( x == y );
			}
			public static System.Boolean operator !=( ISet<T> x, EmptySet y ) { 
				return !( y == x );
			}

			public static ISet<T> Get() { 
				return Get( Comparer<T>.Default );
			}
			public static ISet<T> Get( System.Collections.Generic.IEqualityComparer<T> comparer ) { 
				if ( null == comparer ) { 
					throw new System.ArgumentNullException( "comparer" );
				}
				ISet<T> output = null;
				lock ( theEmpty ) { 
					if ( theEmpty.ContainsKey( comparer ) ) { 
						output = theEmpty[ comparer ];
					} else { 
						output = new EmptySet( comparer );
						theEmpty.Add( comparer, output );
					}
				}
				return output;
			}
		}
		#endregion internal classes


		#region fields
		private static readonly System.Int32 theHashCode;
		private readonly IHashTable<T, System.Int32> myStore;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static Set() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
		}

		private Set() : base() { 
			myHashCode = theHashCode;
		}
		private Set( IHashTable<T, System.Int32> store ) : this() { 
			if ( null == store ) { 
				throw new System.ArgumentNullException( "store" );
			}
			myStore = store;
			unchecked { 
				myHashCode += myStore.GetHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public ISet<T> Empty { 
			get { 
				return GetEmpty( this.Comparer );
			}
		}
		public System.Int32 Count { 
			get { 
				return myStore.Count;
			}
		}
		public System.Boolean IsEmpty { 
			get { 
				return myStore.IsEmpty;
			}
		}
		public System.Collections.Generic.IEqualityComparer<T> Comparer { 
			get { 
				return myStore.HashLogic.KeyComparer;
			}
		}
		public System.Boolean IsReadOnly { 
			get { 
				return true;
			}
		}
		#endregion properties


		#region methods
		public System.Boolean Contains( T item ) { 
			return myStore.ContainsKey( item );
		}
		public ISet<T> Remove( T item ) { 
			if ( !myStore.ContainsKey( item ) ) { 
				return this;
			}
			IHashTable<T, System.Int32> store = myStore.Remove( item );
			if ( store.Count <= 0 ) { 
				return this.Empty;
			} else { 
				return new Set<T>( store );
			}
		}
		public ISet<T> Add( T item ) { 
			if ( myStore.ContainsKey( item ) ) { 
				return this;
			}
			return new Set<T>( myStore.Add( item, 0 ) );
		}
		public ISet<T> Add( System.Collections.Generic.IEnumerable<T> collection ) { 
			if ( null == collection ) { 
				throw new System.ArgumentNullException( "collection" );
			}
			IHashTable<T, System.Int32> store = myStore;
			foreach ( T t in collection ) { 
				if ( !store.ContainsKey( t ) ) { 
					store = store.Add( t, 0 );
				}
			}
			if ( myStore == store ) { 
				return this;
			} else { 
				return new Set<T>( store );
			}
		}
		public ISet<T> Union( ISet<T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			} else if ( this.Count < other.Count ) { 
				return other.Union( this );
			}

			IHashTable<T, System.Int32> store = myStore;
			foreach ( T t in other ) { 
				if ( !store.ContainsKey( t ) ) { 
					store = store.Add( t, 0 );
				}
			}
			if ( store != myStore ) { 
				return new Set<T>( store );
			} else { 
				return this;
			}
		}
		public ISet<T> Intersection( ISet<T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this.Empty;
			} else if ( other.Count < this.Count ) { 
				return other.Intersection( this );
			}

			IHashTable<T, System.Int32> store = HashTable<T, System.Int32>.GetEmpty( this.Comparer );
			foreach ( T t in this ) { 
				if ( other.Contains( t ) ) { 
					store = store.Add( t, 0 );
				}
			}
			if ( store != myStore ) { 
				if ( store.Count <= 0 ) { 
					return this.Empty;
				} else { 
					return new Set<T>( store );
				}
			} else { 
				return this;
			}
		}
		public ISet<T> SymmetricDifference( ISet<T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			}
			return this.Union( other ).Subtract( this.Intersection( other ) );
		}
		public ISet<T> Add( ISet<T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			}
			return this.Union( other );
		}
		public ISet<T> Subtract( ISet<T> other ) { 
			if ( null == other ) { 
				throw new System.ArgumentNullException( "other" );
			} else if ( other.IsEmpty ) { 
				return this;
			}

			IHashTable<T, System.Int32> store = myStore;
			ISet<T> intersection = this.Intersection( other );
			foreach ( T t in intersection ) { 
				store = store.Remove( t );
			}
			if ( myStore == store ) { 
				return this;
			} else { 
				if ( store.Count <= 0 ) { 
					return this.Empty;
				} else { 
					return new Set<T>( store );
				}
			}
		}
		public ISet<ISet<T>> Partition( System.Predicate<T> predicate ) { 
			if ( null == predicate ) { 
				throw new System.ArgumentNullException( "predicate" );
			}
			ISet<T> tSet = this.Empty;
			ISet<T> fSet = tSet;
			foreach ( T t in this ) { 
				if ( predicate( t ) ) { 
					tSet = tSet.Add( t );
				} else { 
					fSet = fSet.Add( t );
				}
			}
			return Set<ISet<T>>.GetEmpty().Add( tSet ).Add( fSet );
		}
		public ISet<ISet<T>> Partition( Func<T, T, System.Boolean> predicate ) { 
			if ( null == predicate ) { 
				throw new System.ArgumentNullException( "predicate" );
			}
			ISet<ISet<T>> output = Set<ISet<T>>.GetEmpty();

			ISet<T> current = null;
			ISet<T> input = this;
			System.Collections.Generic.IEqualityComparer<T> comparer = input.Comparer;
			T x = default( T );
			while ( !input.IsEmpty ) { 
				current = this.Empty;
				foreach( T t in input ) { 
					x = t;
					break;
				}
				current = current.Add( x );
				foreach ( T y in input ) { 
					if ( !comparer.Equals( x, y ) && predicate( x, y ) ) { 
						current = current.Add( y );
					}
				}
				output = output.Add( current );
				foreach ( T t in current ) { 
					input = input.Remove( t );
				}
			}

			return output;
		}

		void System.Collections.Generic.ICollection<T>.Add( T item ) {
			throw new System.NotSupportedException();
		}
		void System.Collections.Generic.ICollection<T>.Clear() {
			throw new System.NotSupportedException();
		}
		public void CopyTo( T[] array, System.Int32 arrayIndex ) {
			if ( arrayIndex < 0 ) {
				throw new System.ArgumentOutOfRangeException( "arrayIndex" );
			} else if ( null == array ) {
				throw new System.ArgumentNullException( "array" );
			} else if ( ( array.Length - arrayIndex ) < this.Count ) {
				throw new System.ArgumentException( null, "array" );
			}
			foreach ( T t in this ) { 
				array[ arrayIndex++ ] = t;
			}
		}
		System.Boolean System.Collections.Generic.ICollection<T>.Remove( T item ) {
			throw new System.NotImplementedException();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { 
			return this.GetEnumerator();
		}
		public System.Collections.Generic.IEnumerator<T> GetEnumerator() { 
			return myStore.Keys.GetEnumerator();
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		public sealed override System.Boolean Equals( System.Object obj ) { 
			return this.Equals( obj as ISet<T> );
		}
		public System.Boolean Equals( ISet<T> other ) { 
			if ( null == (System.Object)other ) { 
				return false;
			} else if ( System.Object.ReferenceEquals( this, other ) ) { 
				return true;
			} else if ( this.Count != other.Count ) { 
				return false;
			} else { 
				foreach ( T t in this ) { 
					if ( !other.Contains( t ) ) { 
						return false;
					}
				}
				return true;
			}
		}
		public static System.Boolean operator ==( Set<T> x, ISet<T> y ) { 
			if ( ( null == (System.Object)x ) && ( null == (System.Object)y ) ) { 
				return true;
			} else if ( ( null == (System.Object)x ) || ( null == (System.Object)y ) ) { 
				return false;
			} else { 
				return x.Equals( y );
			}
		}
		public static System.Boolean operator ==( ISet<T> x, Set<T> y ) { 
			return ( y == x );
		}
		public static System.Boolean operator !=( Set<T> x, ISet<T> y ) { 
			return !( x == y );
		}
		public static System.Boolean operator !=( ISet<T> x, Set<T> y ) { 
			return !( y == x );
		}
		#endregion methods


		#region static methods
		public static ISet<T> GetEmpty() { 
			return EmptySet.Get();
		}
		public static ISet<T> GetEmpty( System.Collections.Generic.IEqualityComparer<T> comparer ) { 
			if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			return EmptySet.Get( comparer );
		}
		#endregion static methods

	}

}