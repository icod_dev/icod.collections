namespace Icod.Collections.Immutable {

	[System.Serializable]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class ListHashBucket<K, T> : LeafHashBucketBase<K, T> { 

		#region fields
		private static readonly System.Int32 theHashCode;
		private readonly System.Int32 myHashCode;
		private readonly Pair<K, T>[] myStore;
		private System.Collections.Generic.ICollection<K> myKeys;
		private System.Collections.Generic.ICollection<T> myValues;
		#endregion fields


		#region .ctor
		static ListHashBucket() {
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked {
				theHashCode += typeof( K ).AssemblyQualifiedName.GetHashCode();
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
		}
		private ListHashBucket( IHashLogic<K, T> hashLogic, System.Int32 keyHash ) : base( hashLogic, keyHash ) {
			myHashCode = theHashCode;
			unchecked {
				myHashCode += hashLogic.GetHashCode();
				myHashCode += keyHash;
			}
		}
		internal ListHashBucket( IHashLogic<K, T> hashLogic, System.Int32 keyHash, Pair<K, T>[] store ) : this( hashLogic, keyHash ) {
			if ( ( null == store ) || ( store.Length <= 0 ) ) {
				throw new System.ArgumentNullException( "store" );
			} else if ( null == hashLogic ) {
				throw new System.ArgumentNullException( "hashLogic" );
			}

			myStore = store;
			unchecked {
				myHashCode += myStore.GetHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public sealed override System.Int32 Count {
			get {
				return myStore.Length;
			}
		}
		public sealed override T this[ K key ] { 
			get { 
				System.Int32 i = this.GetIndexOf( key );
				if ( ( 0 <= i ) || ( i < myStore.Length ) ) { 
					return myStore[ i ].Second;
				} else { 
					throw new System.Collections.Generic.KeyNotFoundException();
				}
			}
		}
		public sealed override System.Collections.Generic.ICollection<K> Keys {
			get {
				if ( null == myKeys ) {
					System.Collections.Generic.List<K> output = new System.Collections.Generic.List<K>();
					foreach ( Pair<K, T> pair in myStore ) {
						output.Add( pair.First );
					}
					if ( null == myKeys ) {
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<K>>(
							ref myKeys,
							output.AsReadOnly(),
							null
						);
					}
				}
				return myKeys;
			}
		}
		public sealed override System.Collections.Generic.ICollection<T> Values {
			get {
				if ( null == myValues ) {
					System.Collections.Generic.List<T> output = new System.Collections.Generic.List<T>();
					foreach ( Pair<K, T> pair in myStore ) {
						output.Add( pair.Second );
					}
					if ( null == myValues ) {
						System.Threading.Interlocked.CompareExchange<System.Collections.Generic.ICollection<T>>(
							ref myValues,
							output.AsReadOnly(),
							null
						);
					}
				}
				return myValues;
			}
		}
		#endregion properties


		#region methods
		private System.Int32 GetIndexOf( K key ) {
			System.Int32 code = HashHelper.GetHash<K>( this.HashLogic.KeyComparer, key );
			if ( code != this.KeyHash ) {
				return -1;
			}
			System.Collections.Generic.IEqualityComparer<K> comparer = this.HashLogic.KeyComparer;
			System.Int32 i = myStore.Length;
			while ( ( 0 <= --i ) && ( !comparer.Equals( myStore[ i ].First, key ) ) ) {
				;
			}
			return i;
		}
		public sealed override System.Boolean ContainsKey( K key ) {
			System.Int32 i = this.GetIndexOf( key );
			return ( ( 0 <= i ) && ( i < myStore.Length ) );
		}
		public sealed override IHashTable<K, T> Add( K key, T item ) {
			System.Int32 i = this.GetIndexOf( key );
			if ( ( 0 <= i ) && ( i < myStore.Length ) ) {
				throw new DuplicateKeyException();
			}

			IHashLogic<K, T> logic = this.HashLogic;
			System.Collections.Generic.IEqualityComparer<K> comparer = logic.KeyComparer;
			System.Int32 tCode = this.KeyHash;
			System.Int32 oCode = HashHelper.GetHash<K>( comparer, key );

			if ( tCode == oCode ) {
				System.Int32 oldLen = myStore.Length;
				Pair<K, T>[] store = new Pair<K, T>[ oldLen + 1 ];
				System.Array.Copy( myStore, 0, store, 0, oldLen );
				store[ oldLen ] = new Pair<K, T>( key, item );
				return new ListHashBucket<K, T>( logic, tCode, store );
			} else {
				return HashHelper.HashUp<K, T>( logic, PrimeHelper.MinPrime, this, new LeafHashBucket<K, T>( logic, oCode, key, item ) );
			}
		}
		public sealed override IHashTable<K, T> Remove( K key ) {
			System.Int32 storeLen = myStore.Length;
			System.Int32 i = this.GetIndexOf( key );
			if ( ( i < 0 ) || ( storeLen <= i ) ) {
				throw new System.Collections.Generic.KeyNotFoundException();
			}

			System.Int32 len = storeLen - 1;
			if ( 0 == len ) {
				return this.Empty;
			}
			Pair<K, T>[] store = new Pair<K, T>[ len ];
			if ( 0 < i ) {
				System.Array.Copy( myStore, 0, store, 0, i );
			}
			if ( i < len ) {
				System.Array.Copy( myStore, i + 1, store, i, len - i );
			}
			if ( 1 == len ) {
				return new LeafHashBucket<K, T>( this.HashLogic, this.KeyHash, store[ 0 ].First, store[ 0 ].Second );
			} else {
				return new ListHashBucket<K, T>( this.HashLogic, this.KeyHash, store );
			}
		}

		public sealed override System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<K, T>> GetEnumerator() {
			foreach ( Pair<K, T> pair in myStore ) {
				yield return new System.Collections.Generic.KeyValuePair<K, T>( pair.First, pair.Second );
			}
		}

		public sealed override System.Int32 GetHashCode() {
			return myHashCode;
		}
		#endregion methods

	}

}