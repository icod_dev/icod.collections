namespace Icod.Collections { 

	[System.Serializable]
	[System.Runtime.InteropServices.ComVisible( true )]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public class DuplicateKeyException : System.ArgumentException { 

		private const System.String theMessage = "An item with the same key has already been added.";

		protected DuplicateKeyException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context ) { 
		}
		public DuplicateKeyException() : base( theMessage ) { 
		}
		public DuplicateKeyException( System.String paramName ) : this( theMessage, paramName ) { 
		}
		public DuplicateKeyException( System.String message, System.String paramName ) : base( message, paramName ) { 
		}
		public DuplicateKeyException( System.String paramName, System.Exception innerException ) : this( theMessage, paramName, innerException ) { 
		}
		public DuplicateKeyException( System.String message, System.String paramName, System.Exception innerException ) : base( message, paramName, innerException ) { 
		}

	}

}