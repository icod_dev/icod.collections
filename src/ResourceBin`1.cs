namespace Icod.Collections { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	[System.Diagnostics.DebuggerDisplay( "Count = {Count}" )]
	public sealed class ResourceBin<T> : System.IDisposable { 

		#region fields
		private Queue<T> myStore;
		private readonly System.Int32 myCapacity;
		private System.Threading.Semaphore myConsumer;
		private System.Threading.Semaphore myProducer;
		private System.Int32 myIsDisposed = 0;
		#endregion fields


		#region .ctor
		public ResourceBin( System.Int32 capacity ) : base() { 
			if ( capacity <= 0 ) { 
				throw new System.ArgumentOutOfRangeException( "capacity", "capacity must be greater than zero." );
			}
			myCapacity = capacity;
			myStore = new Queue<T>();
			myConsumer = new System.Threading.Semaphore( 0, capacity );
			myProducer = new System.Threading.Semaphore( capacity, capacity );
		}
		~ResourceBin() { 
			this.Dispose( false );
		}
		#endregion .ctor


		#region properties
		public System.Int32 Count { 
			get { 
				if ( 0 != myIsDisposed ) { 
					throw new System.ObjectDisposedException( null );
				}
				try { 
					return myStore.Count;
				} catch ( System.NullReferenceException ) { 
					throw new System.ObjectDisposedException( null );
				}
			}
		}
		public System.Int32 Capacity { 
			get { 
				if ( 0 != myIsDisposed ) { 
					throw new System.ObjectDisposedException( null );
				}
				return myCapacity;
			}
		}
		#endregion properties


		#region methods
		public T Dequeue() { 
			if ( 0 != myIsDisposed ) { 
				throw new System.ObjectDisposedException( null );
			}
			try { 
				myConsumer.WaitOne();
				T output = myStore.Dequeue();
				myProducer.Release( 1 );
				return output;
			} catch ( System.NullReferenceException ) { 
				throw new System.ObjectDisposedException( null );
			} catch ( System.ObjectDisposedException ) { 
				throw new System.ObjectDisposedException( null );
			}
		}
		public void Enqueue( T item ) { 
			if ( 0 != myIsDisposed ) { 
				throw new System.ObjectDisposedException( null );
			}
			try { 
				myProducer.WaitOne();
				myStore.Enqueue( item );
				myConsumer.Release( 1 );
			} catch ( System.NullReferenceException ) { 
				throw new System.ObjectDisposedException( null );
			} catch ( System.ObjectDisposedException ) { 
				throw new System.ObjectDisposedException( null );
			}
		}

		public void Dispose() { 
			this.Dispose( true );
			System.GC.SuppressFinalize( this );
		}
		private void Dispose( System.Boolean disposing ) { 
			if ( disposing ) { 
				if ( 1 == System.Threading.Thread.VolatileRead( ref myIsDisposed ) ) { 
					return;
				}
				System.IDisposable handle;

				handle = myConsumer;
				if ( null != handle ) { 
					handle.Dispose();
					myConsumer = null;
				}
				handle = myProducer;
				if ( null != handle ) { 
					handle.Dispose();
					myProducer = null;
				}
				handle = null;

				myStore = null;
				System.Threading.Thread.VolatileWrite( ref myIsDisposed, 1 );
			}
		}
		#endregion methods

	}

}