namespace Icod.Collections { 

	[System.Serializable]
	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public sealed class Comparer<T> : IComparer<T> { 

		#region fields
		private static readonly System.Int32 theHashCode;
		private static Comparer<T> theDefault;
		private static System.Collections.Generic.IDictionary< 
			System.Collections.Generic.IComparer<T>, 
			System.Collections.Generic.IDictionary< 
				System.Collections.Generic.IEqualityComparer<T>, 
				Comparer<T> 
			>
		> theComparers;

		private readonly System.Collections.Generic.IComparer<T> myComparer;
		private readonly System.Collections.Generic.IEqualityComparer<T> myEqualityComparer;
		private readonly System.Int32 myHashCode;
		#endregion fields


		#region .ctor
		static Comparer() { 
			theHashCode = System.Reflection.Assembly.GetExecutingAssembly().GetType().AssemblyQualifiedName.GetHashCode();
			unchecked { 
				theHashCode += typeof( T ).AssemblyQualifiedName.GetHashCode();
			}
			theDefault = null;

			theComparers = new System.Collections.Generic.Dictionary< 
				System.Collections.Generic.IComparer<T>, 
				System.Collections.Generic.IDictionary< 
					System.Collections.Generic.IEqualityComparer<T>, 
					Comparer<T> 
				>
			>();
		}

		private Comparer() : base() { 
			myHashCode = theHashCode;
		}
		private Comparer( System.Collections.Generic.IComparer<T> comparer, System.Collections.Generic.IEqualityComparer<T> equalityComparer ) : this() { 
			if ( null == equalityComparer ) { 
				throw new System.ArgumentNullException( "equalityComparer" );
			} else if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}
			myEqualityComparer = equalityComparer;
			myComparer = comparer;
			unchecked { 
				myHashCode += myComparer.GetHashCode();
				myHashCode += myEqualityComparer.GetHashCode();
			}
		}
		#endregion .ctor


		#region properties
		public static Comparer<T> Default { 
			get { 
				if ( null == theDefault ) { 
					System.Threading.Interlocked.CompareExchange<Comparer<T>>( 
						ref theDefault, 
						Get(), 
						null 
					);
				}
				return theDefault;
			}
		}
		#endregion propreties


		#region methods
		public System.Int32 Compare( T x, T y ) { 
			return myComparer.Compare( x, y );
		}
		public System.Int32 GetHashCode( T obj ) { 
			return myEqualityComparer.GetHashCode( obj );
		}
		public System.Boolean Equals( T x, T y ) { 
			return myEqualityComparer.Equals( x, y );
		}

		public T Min( T x, T y ) { 
			if ( this.Compare( x, y ) <= 0 ) { 
				return x;
			} else { 
				return y;
			}
		}
		public T Max( T x, T y ) { 
			if ( this.Compare( x, y ) < 0 ) { 
				return y;
			} else { 
				return x;
			}
		}

		public sealed override System.Int32 GetHashCode() { 
			return myHashCode;
		}
		#endregion methods


		#region static methods
		public static Comparer<T> Get() { 
			return Get( System.Collections.Generic.Comparer<T>.Default, System.Collections.Generic.EqualityComparer<T>.Default );
		}
		public static Comparer<T> Get( System.Collections.Generic.IComparer<T> comparer, System.Collections.Generic.IEqualityComparer<T> equalityComparer ) { 
			if ( null == equalityComparer ) { 
				throw new System.ArgumentNullException( "equalityComparer" );
			} else if ( null == comparer ) { 
				throw new System.ArgumentNullException( "comparer" );
			}

			Comparer<T> output = null;
			System.Collections.Generic.IDictionary<System.Collections.Generic.IEqualityComparer<T>, Comparer<T>> equals = null;
			lock ( theComparers ) { 
				if ( theComparers.ContainsKey( comparer ) ) { 
					equals = theComparers[ comparer ];
				} else { 
					equals = new System.Collections.Generic.Dictionary<System.Collections.Generic.IEqualityComparer<T>, Comparer<T>>();
					theComparers.Add( comparer, equals );
				}
			}
			lock ( equals ) { 
				if ( equals.ContainsKey( equalityComparer ) ) { 
					output = equals[ equalityComparer ];
				} else { 
					output = new Comparer<T>( comparer, equalityComparer );
					equals.Add( equalityComparer, output );
				}
			}
			return output;
		}
		#endregion static methods

	}

}