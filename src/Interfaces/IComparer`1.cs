namespace Icod.Collections { 

	[Icod.LgplLicense]
	[Icod.Author( "Timothy J. ``Flytrap\'\' Bruce" )]
	[Icod.ReportBugsTo( "mailto:uniblab@hotmail.com" )]
	public interface IComparer<T> : System.Collections.Generic.IEqualityComparer<T>, System.Collections.Generic.IComparer<T> { 

		T Min( T x, T y );
		T Max( T x, T y );

	}

}