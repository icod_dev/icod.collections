﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "Icod.Collections" )]
[assembly: AssemblyDescription( "ICOD collections library" )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "PeopleSuck Industries" )]
[assembly: AssemblyProduct( "ICOD Collections library" )]
[assembly: AssemblyCopyright( "Copyright © PeopleSuck Industries 2011" )]
[assembly: AssemblyTrademark( "2011 PeopleSuck Industries" )]
[assembly: AssemblyCulture( "" )]
[assembly: Icod.Author( @"Timothy J. ``Flytrap'' Bruce" )]
[assembly: Icod.ReportBugsTo( "uniblab@hotmail.com" )]
[assembly: Icod.LgplLicense]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible( false )]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid( "10676e0f-473b-4bfa-a2ce-ae3edf7f6d16" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion( "1.0.0.1" )]
[assembly: AssemblyFileVersion( "1.0.0.1" )]
[assembly: AssemblyInformationalVersion( "1.0.0.1" )]

[assembly: AssemblyDelaySign( false )]